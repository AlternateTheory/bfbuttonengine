--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local S = Engine.Settings;

S.ButtonDefaults = {};
S.ButtonDefaults.Action = {};
S.ButtonDefaults.Action.Type 		= "empty";
S.ButtonDefaults.FlyoutDirection	= "UP";
S.ButtonDefaults.KeyBindText		= "";
S.ButtonDefaults.Enabled			= true;
S.ButtonDefaults.RespondToMouse		= true;
S.ButtonDefaults.Alpha				= 1.0;
S.ButtonDefaults.Locked				= false;
S.ButtonDefaults.FullLock			= false;
S.ButtonDefaults.Source				= false;
S.ButtonDefaults.AlwaysShowGrid		= true;
S.ButtonDefaults.ShowTooltip		= true;
S.ButtonDefaults.ShowKeyBindText	= true;
S.ButtonDefaults.ShowCounts			= true;
S.ButtonDefaults.ShowMacroName		= true;
S.ButtonDefaults.AllowKeybindMode	= true;

S.OriginalButtonGroupDefaults = {};
S.OriginalButtonDefaults = {};

S.ActionKeyFunction	= _G["IsShiftKeyDown"];

S.DefaultButtonFrameNameFormat = "BFButtonEngine_Button_%i";
S.DefaultButtonSeq = 1;

S.CursorOverlayFrameNameFormat = "BFButtonEngineCursorOverlay_%i";
S.CursorOverlaySeq = 1;

S.FlyoutDirectionUIFrameNameFormat = "BFButtonEngineFlyoutDirectionUI_%i";
S.FlyoutDirectionUISeq = 1;
