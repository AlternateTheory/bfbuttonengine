--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014
    
    Desc:	Util type functionality for the Button Engine

]]

--[[
    Line 75: function Util.SpellFullName(NameOrID)

    Line 147: function Util.LookupFlyoutIndex(FlyoutID)
    Line 155: function Util.LookupFlyoutTexture(FlyoutID)
    Line 170: function Util.LookupTalentSpellID(Name, Specialization)
    Line 190: function Util.LookupSpecializationSpellID(Name, Specialization)

    Line 336: function Util.GetItemIDFromHyperlink(link)
    Line 344: function Util.LookupItemInventorySlot(ItemID)
    Line 352: function Util.LookupItemBagSlot(ItemID)
    
    Line 368: function Util.CacheInventoryItems()
    Line 390: function Util.CacheBagItems()
    
    Line 409: function Util.LookupMountIndex(MountID)
    Line 425: function Util.LookupEquipmentSetIndex(EquipmentSetID)
    
    Line 439: function Util.GetLocaleString(Value)
    Line 447: function Util.GetDefaultButtonFrameName()
    Line 460: function Util.NoCombatAndValidButtonTest(ActionButton)
    
    Line 472: function Util.FindInTable(Table, Value, Start)
    Line 485: function Util.FindInTableWhere(Table, Delegate)
    Line 497: function Util.RemoveFromTable(Table, Value, Start)
    Line 510: function Util.ClearTable(Table)
    Line 520: function Util.FindInPairs(Table, Value)
    Line 533: function Util.TableAddUnsetKeys(Table, AddTable)
    Line 545: function Util.Count(Table)
    Line 560: function DeepCopy(orig)
]]


local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local Util = Engine.Util;

local S = Engine.Settings;
local C = Engine.Constants;



local L = Engine.Locales[GetLocale()] or {};
if (GetLocale() ~= "enUS") then
    setmetatable(L, Engine.Locales["enUS"]);
end


-- local Cache tables
local SpellIDToIndex;
local FlyoutIDToIndex;
local TalentSpellIDs;
local TalentNameSpellIDs;
local SpecializationSpellIDs;
local SpecializationNameSpellIDs;
local ItemBagSlot;
local ItemInventorySlot;
local GetMouseFoci = GetMouseFoci


function Util.GetMouseFocus()
    local frames = GetMouseFoci()
    for _, frame in ipairs(frames) do
        if frame:IsMouseMotionFocus() then
            return frame
        end
    end
end


function Util.SpellFullName(NameOrID)
    local spellInfo = C_Spell.GetSpellInfo(NameOrID)
    local Name = spellInfo and spellInfo.name
    local Rank = C_Spell.GetSpellSubtext(NameOrID)
    local spellName = Name
    if (Name and Rank) then
        spellName = Name .. "(" .. Rank .. ")"
    end
    return spellName
end


do
    local EventManager = CreateFrame("FRAME")
    local EventCallbacks = {}
    local RunOncePostCombatCallbacks = {}
    local RunOncePostCombatUniqueCallbacks = {}
    function EventManager:OnEvent(event, ...)
        for _, func in ipairs(EventCallbacks[event]) do
            func(...)
        end
    end
    EventManager:SetScript("OnEvent", EventManager.OnEvent)


    --[[------------------------------------------------
        RegisterEventCallback
    --------------------------------------------------]]
    function Util.RegisterEventCallback(event, func)
        if not EventCallbacks[event] then
            EventCallbacks[event] = {}
            EventManager:RegisterEvent(event)
        end

        tinsert(EventCallbacks[event], func)
    end


    --[[------------------------------------------------
        UnregisterEventCallback
    --------------------------------------------------]]
    function Util.UnregisterEventCallback(event, func)
        if EventCallbacks[event] then
            tDeleteItem(EventCallbacks[event], func)
            if #EventCallbacks[event] == 0 then
                EventManager:UnregisterEvent(event)
                EventCallbacks[event] = nil
            end
        end
    end


    local function ExecuteRunOncePostCombatCallbacks()
        for _, callback in ipairs(RunOncePostCombatCallbacks) do
            callback[1](unpack(callback, 2))
        end
        Util.UnregisterEventCallback("PLAYER_REGEN_ENABLED", ExecuteRunOncePostCombatCallbacks)
        RunOncePostCombatCallbacks = {}
        RunOncePostCombatUniqueCallbacks = {}
    end


    --[[------------------------------------------------
        RunOncePostCombat
        These callbacks will be called in registered order
        once combat ends
        After which the list of callbacks will be cleared
    --------------------------------------------------]]
    function Util.RunOncePostCombat(func, ...)
        if #RunOncePostCombatCallbacks == 0 then
            Util.RegisterEventCallback("PLAYER_REGEN_ENABLED", ExecuteRunOncePostCombatCallbacks)
        end
        tinsert(RunOncePostCombatCallbacks, {func, ...})
    end


    --[[------------------------------------------------
        RunOncePostCombatUnique
        Adds the functions to the registered list of callbacks
        but if already added via this function, the callback will
        not be added again to the list
    --------------------------------------------------]]
    function Util.RunOncePostCombatUnique(func, ...)
        if #RunOncePostCombatCallbacks == 0 then
            Util.RegisterEventCallback("PLAYER_REGEN_ENABLED", ExecuteRunOncePostCombatCallbacks)
        end
        if not RunOncePostCombatUniqueCallbacks[func] then
            tinsert(RunOncePostCombatCallbacks, {func, ...})
            RunOncePostCombatUniqueCallbacks[func] = true
        end
    end
end




--[[ FIXME
    Ideally a scan only happens once to populate the icon lookup
    If there is a miss (e.g. a continual miss due to an unlearnable flyout)
    a Question mark will be populated for the id
    To avoid permanently holding a question mark the flyout textures should be flushed when talents and learned/changed etc
]]
local FlyoutTextures = {}
function Util.GetFlyoutTexture(id)
    local iconID = FlyoutTextures[id]
    if iconID then
        return iconID
    end
    for slotIndex in Util.SpellBookSlotIterator() do
        local spellBookInfo = C_SpellBook.GetSpellBookItemInfo(slotIndex, Enum.SpellBookSpellBank.Player)
        if spellBookInfo.itemType == Enum.SpellBookItemType.Flyout then
            FlyoutTextures[spellBookInfo.actionID] = spellBookInfo.iconID
        end
    end
    iconID = FlyoutTextures[id]
    if iconID == nil then
        FlyoutTextures[id] = C.QUESTION_MARK
        iconID = C.QUESTION_MARK
    end
    return iconID
end


function Util.GetFlyoutSlotIndex(id)
    for slotIndex in Util.SpellBookSlotIterator() do
        local spellBookInfo = C_SpellBook.GetSpellBookItemInfo(slotIndex, Enum.SpellBookSpellBank.Player)
        if spellBookInfo.itemType == Enum.SpellBookItemType.Flyout and spellBookInfo.actionID == id then
            return slotIndex
        end
    end
    return nil
end


function Util.IsKnownProfessionSpell(spellID)
    for slotIndex in Util.ProfessionSlotIterator() do
        local spellBookItem = C_SpellBook.GetSpellBookItemInfo(slotIndex, Enum.SpellBookSpellBank.Player)
        if spellID == spellBookItem.spellID then
            return true
        end
    end
    return false
end

--[[------------------------------------------------
    WoW v7.0.3
        * Sometimes two talents can have the same name but be slightly different
          based on which specilization ergo the specialization should ideally be specified
        * e.g. Of Talent Name repeat
            - Priest MindBender, Discipline and Holy
--------------------------------------------------]]
function Util.LookupTalentSpellID(Name, Specialization)
    
    Name = string.gsub(Name or "", "%([^%(%)]*%)$", "");	-- Strip off end bracketing
    
    if (Specialization) then
        return TalentNameSpellIDs[Specialization][Name];
    end
    
    for i = 1, GetNumSpecializations() do
        local SpellID = TalentNameSpellIDs[i][Name];
        if (SpellID) then
            return SpellID;
        end
    end
    return nil;
    
end



function Util.LookupSpecializationSpellID(Name, Specialization)
    
    Name = string.gsub(Name or "", "%([^%(%)]*%)$", "");	-- Strip off end bracketing
    
    if (Specialization) then
        return SpecializationNameSpellIDs[Specialization][Name];
    end
    
    for i = 1, GetNumSpecializations() do
        local SpellID = SpecializationNameSpellIDs[i][Name];
        if (SpellID) then
            return SpellID;
        end
    end
    return nil;
    
end


--[[------------------------------------------------
    WoW v11
    For use in for loops to get the slot indexes
    for use with C_SpellBook and the Enum.SpellBookSpellBank.Player
--------------------------------------------------]]
function Util.SpellBookSlotIterator()
    local spellGroups =
    {
       C_SpellBook.GetSpellBookSkillLineInfo(Enum.SpellBookSkillLineIndex.Class),
       C_SpellBook.GetSpellBookSkillLineInfo(Enum.SpellBookSkillLineIndex.General)
    }
    if not spellGroups[1] then
        return function() return nil end
    end
    local numSpecializations = GetNumSpecializations(false, false)
    local numAvailableSkillLines = C_SpellBook.GetNumSpellBookSkillLines()
    local firstSpecIndex = Enum.SpellBookSkillLineIndex.MainSpec
    local maxSpecIndex = firstSpecIndex + numSpecializations
    maxSpecIndex = math.min(numAvailableSkillLines, maxSpecIndex)
    for skillLineIndex = firstSpecIndex, maxSpecIndex do
       local skillLineInfo = C_SpellBook.GetSpellBookSkillLineInfo(skillLineIndex)
       if skillLineInfo then
          tinsert(spellGroups, skillLineInfo)
       end
    end

    local i = 0
    local j = 1
    local itemIndexOffset = spellGroups[j].itemIndexOffset
    local numSpellBookItems = spellGroups[j].numSpellBookItems
    return function()
       i = i + 1
       while i > numSpellBookItems do
          j = j + 1
          if j > #spellGroups then
             return nil
          end
          itemIndexOffset = spellGroups[j].itemIndexOffset
          numSpellBookItems = spellGroups[j].numSpellBookItems
          i = 1
       end
       return i + itemIndexOffset
    end
 end


function Util.ProfessionSlotIterator()
    local prof1, prof2, arch, fish, cook = GetProfessions()
    local profs = {prof1 or 0, prof2 or 0, arch or 0, fish or 0, cook or 0}

    local i = 0
    local j = 1
    local _, _, _, _, numSpells, spellOffset = GetProfessionInfo(profs[j])
    return function()
        i = i + 1
        while numSpells == nil or i > numSpells do
            j = j + 1
            if j > #profs then
                return nil
            end
            local _, _, _, _, ns, so = GetProfessionInfo(profs[j])
            numSpells = ns
            spellOffset = so
            i = 1
        end
        return i + spellOffset
    end
end


--[[------------------------------------------------
    * Copied from Blizz code
--------------------------------------------------]]
function Util.GetItemIDFromHyperlink(link)

    return tonumber(link:match("|Hitem:(%d+)"));
    
end



function Util.LookupItemInventorySlot(ItemID)

    return ItemInventorySlot[ItemID];
    
end



function Util.LookupItemBagSlot(ItemID)
    local Result = ItemBagSlot[ItemID];
    if (Result) then
        return Result[1], Result[2];
    end
    return nil, nil;
end



--[[------------------------------------------------
    WoW v???
        * note that reverse order matches how Bliz
          Default operates if same item type is in
          multiple slots
--------------------------------------------------]]
function Util.CacheInventoryItems()

    ItemInventorySlot = {};
    local FishingToolSlot = 28; -- INVSLOT_LAST_EQUIPPED used to cover inventory, but now the fishing slot is the last slot - there does not appear to be a global constant for this
    for Slot = FishingToolSlot, INVSLOT_FIRST_EQUIPPED, -1 do
        local ItemID = GetInventoryItemID("player", Slot);
        if (ItemID) then
            ItemInventorySlot[ItemID] = Slot;
        end
    end

end
Util.CacheInventoryItems();



--[[------------------------------------------------
    WoW v???
        * note that reverse order matches how Bliz
          Default operates if same item type is in
          multiple slots
--------------------------------------------------]]
function Util.CacheBagItems()

    ItemBagSlot = {};

    for Bag = NUM_TOTAL_EQUIPPED_BAG_SLOTS, 0, -1 do
        local NumSlots = C_Container.GetContainerNumSlots(Bag);
        for Slot = C_Container.GetContainerNumSlots(Bag), 1, -1 do
            local ItemID = C_Container.GetContainerItemID(Bag, Slot);
            if (ItemID) then
                ItemBagSlot[ItemID] = {Bag, Slot};
            end
        end
    end

end
Util.CacheBagItems();



function Util.LookupMountIndex(MountID)

    local Num = C_MountJournal.GetNumMounts();
    if (MountID == C.SUMMON_RANDOM_FAVORITE_MOUNT_ID) then
        return 0;
    end
    for i = 1, Num do
        if (select(12, C_MountJournal.GetDisplayedMountInfo(i)) == MountID) then
            return i;
        end
    end
    
end


--[[ No longer needed it would appear that the WoW API is now using the ID instead
function Util.LookupEquipmentSetIndex(EquipmentSetID)

    local Total = GetNumEquipmentSets();
    for i = 1, Total do
        if (select(3, GetEquipmentSetInfo(i)) == EquipmentSetID) then
            return i;
        end
    end
    return nil;
    
end
]]


function Util.GetLocaleString(Value)

    return L[Value];
    
end



function Util.GetDefaultButtonFrameName()

    local FrameName;
    repeat
        FrameName = string.format(S.DefaultButtonFrameNameFormat, S.DefaultButtonSeq);
        S.DefaultButtonSeq = S.DefaultButtonSeq + 1;
    until _G[FrameName] == nil;
    return FrameName;
    
end



function Util.GetCursorOverlayFrameName()

    local FrameName;
    repeat
        FrameName = string.format(S.CursorOverlayFrameNameFormat, S.CursorOverlaySeq);
        S.CursorOverlaySeq = S.CursorOverlaySeq + 1;
    until _G[FrameName] == nil;
    return FrameName;
    
end



function Util.GetFlyoutDirectionUIFrameName()

    local FrameName;
    repeat
        FrameName = string.format(S.FlyoutDirectionUIFrameNameFormat, S.FlyoutDirectionUISeq);
        S.FlyoutDirectionUISeq = S.FlyoutDirectionUISeq + 1;
    until _G[FrameName] == nil;
    return FrameName;

end



function Util.NoCombatAndValidButtonTest(ActionButton)

    if (InCombatLockdown()) then
        return C.ERROR_IN_COMBAT_LOCKDOWN;
    elseif (not ActionButton.BFButton) then				-- If not a table, tough!
        return C.ERROR_NOT_BUTTONFORGE_ACTIONBUTTON;
    end
    
end



function Util.FindInTable(Table, Value, Start)

    for i = Start or 1, #Table do
        if (Table[i] == Value) then
            return i;
        end
    end
    return nil;
    
end



function Util.FindInTableWhere(Table, Delegate)

    for i = 1, #Table do
        if (Delegate(Table[i])) then
            return Table[i];
        end
    end
    
end



function Util.RemoveFromTable(Table, Value, Start)

    for i = Start or 1, #Table do
        if (Table[i] == Value) then
            return table.remove(Table, i);
        end
    end
    return nil;
    
end



function Util.ClearTable(Table)

    for k, v in pairs(Table) do
        Table[k] = nil;
    end
    
end



function Util.FindInPairs(Table, Value)

    for k, v in pairs(Table) do
        if (v == Value) then
            return k;
        end
    end
    return nil;
    
end



function Util.TableAddUnsetKeys(Table, AddTable)

    for k, v in pairs(AddTable) do
        if (Table[k] == nil) then
            Table[k] = v;
        end
    end
    
end



function Util.Count(Table)

    local Count = 0;
    for k, v in pairs(Table) do
        Count = Count + 1;
    end
    return Count;
    
end



--[[------------------------------------------------
    * Copy Pasta from http://lua-users.org/wiki/CopyTable
--------------------------------------------------]]
local function DeepCopy(orig)

    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[DeepCopy(orig_key)] = DeepCopy(orig_value)
        end
        setmetatable(copy, DeepCopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
    
end
Util.DeepCopy = DeepCopy;


