--[[
	Author: Alternator (Massiner of Nathrezim)
	Date:	2023

]]


local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local PackageEmpty = Engine.PackageEmpty;
local Scripts = Engine.Scripts;


function PackageEmpty.SetAction(BFButton)

	local Action	= BFButton.Action;
	BFButton.Type	= "empty";
	Action.Type		= "empty";

	if (not InCombatLockdown()) then
		BFButton:UpdateAttributes();
		BFButton:UpdateSecureCursor();
		BFButton:UpdateHideOnCombat();
        BFButton:UpdateShown();
        BFButton:UpdateKeybind();
	end

end


function PackageEmpty.UpdateAttributes(BFButton)
	local ABW = BFButton.ABW;
	ABW:SetAttribute("type", nil);
    ABW:SetAttribute("typerelease", nil);
end


function PackageEmpty.Get_Attributes(Action)
	--return "empty";
end


function PackageEmpty.UpdateSecureCursor(BFButton)
	local ABW = BFButton.ABW;
	ABW:SetAttribute("ActionType", "empty");
	ABW:SetAttribute("ActionParameter1", nil);
	ABW:SetAttribute("ActionParameter2", nil);
end


function PackageEmpty.Get_OrderedParameters(Action)
	return "empty";
end


--[[------------------------------------------------

    Finalize will reset certain elements of the button that might not be
    affected by the new action (rather than a blanket reset, this is intended to be more performant)

--------------------------------------------------]]
function PackageEmpty.Finalize(BFButton)
    if (not InCombatLockdown()) then
		-- By setting Type = nil, UpdateShown will no longer treat the button as empty, this allows the UpdateShown() here to start displaying the button
		-- in anticipation of the action type the button is going to be set to.
		-- It is done in the Empty finalize so that the step does not have to be added to every type of action
		BFButton.Type = nil;
		BFButton:UpdateHideOnCombat();
        BFButton:UpdateShown();
        BFButton:UpdateKeybind();
    end
end

PackageEmpty.UpdateAction				= Scripts.EmptyF;
PackageEmpty.FullUpdate					= Scripts.EmptyF;
PackageEmpty.UpdateIcon					= Scripts.EmptyF;
PackageEmpty.UpdateChecked				= Scripts.EmptyChecked;
PackageEmpty.UpdateUsable				= Scripts.EmptyF;
PackageEmpty.UpdateCooldown				= Scripts.EmptyF;
PackageEmpty.UpdateEquipped				= Scripts.EmptyF;
PackageEmpty.UpdateText					= Scripts.EmptyF;
PackageEmpty.UpdateGlow					= Scripts.EmptyF;
PackageEmpty.UpdateShine				= Scripts.EmptyF;
PackageEmpty.UpdateMacro				= Scripts.EmptyF;
PackageEmpty.GetCursor					= Scripts.EmptyF;
PackageEmpty.UpdateFlashRegistration	= Scripts.EmptyF;
PackageEmpty.UpdateRangeCheckRegistration	= Scripts.EmptyF;
PackageEmpty.SwapActionWithButtonAction	= Scripts.SwapActionWithButtonAction;
PackageEmpty.UpdateTooltip				= Scripts.EmptyF;
