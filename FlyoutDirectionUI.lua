--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2016

]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local C = Engine.Constants;

local Util = Engine.Util;
local FlyoutUI = Engine.FlyoutUI;
local Methods = Engine.Methods;

local AttachedBFButton;

local FlyoutDirectionUIFrame = CreateFrame("FRAME", Util.GetFlyoutDirectionUIFrameName());
tinsert(UISpecialFrames, FlyoutDirectionUIFrame:GetName());


local function OnClick(self)
    AttachedBFButton:SetFlyoutDirection(self.Direction);
    FlyoutUI.SetDirection(self.Direction);
    FlyoutUI.DetachFlyoutUI();
end


local function SetupButton(Direction, Rotation, Width, Height, Side, XInset, YInset)

    local Button = CreateFrame("CHECKBUTTON", nil, FlyoutDirectionUIFrame); --, nil, "SecureHandlerBaseTemplate");
    Button:SetScale(1.2);
    Button:SetNormalAtlas("UI-HUD-ActionBar-Flyout");
    local Texture = Button:GetNormalTexture();
    SetClampedTextureRotation(Texture, Rotation);

    Button:SetHighlightAtlas("UI-HUD-ActionBar-Flyout-Mouseover");
    Texture = Button:GetHighlightTexture();
    SetClampedTextureRotation(Texture, Rotation);

    Button:SetPushedAtlas("UI-HUD-ActionBar-Flyout-Down");
    Texture = Button:GetPushedTexture(Texture);
    SetClampedTextureRotation(Texture, Rotation);

    local Atlas = C_Texture.GetAtlasInfo("UI-HUD-ActionBar-Flyout-Mouseover");
    Button:SetCheckedTexture(Atlas.file);
    Texture = Button:GetCheckedTexture();
    Texture:SetTexCoord(Atlas.leftTexCoord, Atlas.rightTexCoord, Atlas.topTexCoord, Atlas.bottomTexCoord);
    SetClampedTextureRotation(Texture, Rotation)

    Button:SetSize(Width, Height);
    Button:SetPoint(Side, XInset, YInset);
    Button.Direction = Direction;
    Button:SetScript("OnClick", OnClick);

    return Button;

end


local Inset = 2;
local Width = 18
local Height = 7;
local UPButton      = SetupButton("UP"      , 0     , Width, Height, "TOP"      , 0, Inset);
local RIGHTButton   = SetupButton("RIGHT"   , 90    , Height, Width, "RIGHT"    , Inset, 0);
local DOWNButton    = SetupButton("DOWN"    , 180   , Width, Height, "BOTTOM"   , 0, -Inset);
local LEFTButton    = SetupButton("LEFT"    , 270   , Height, Width, "LEFT"     , -Inset, 0);
local Buttons = {UPButton, RIGHTButton, DOWNButton, LEFTButton};


function FlyoutUI.AttachFlyoutUI(BFButton)

    if (InCombatLockdown()) then
        return;
    end

    AttachedBFButton = BFButton;
    local ABW = BFButton.ABW;
    FlyoutDirectionUIFrame:SetParent(ABW);
    FlyoutDirectionUIFrame:ClearAllPoints();
    FlyoutDirectionUIFrame:SetPoint("TOPLEFT");
    FlyoutDirectionUIFrame:SetPoint("BOTTOMRIGHT");

    FlyoutUI.SetDirection(BFButton:GetFlyoutDirection());
    FlyoutDirectionUIFrame:Show();
    FlyoutDirectionUIFrame:RegisterEvent("PLAYER_REGEN_DISABLED");

    ABW.FlyoutArrowContainer:Hide();

end


function FlyoutUI.DetachFlyoutUI()

    if (AttachedBFButton == nil) then
        return;
    end

    if (AttachedBFButton.Type == "flyout") then
        AttachedBFButton.ABW.FlyoutArrowContainer:Show();
    end

    AttachedBFButton = nil;

    FlyoutDirectionUIFrame:Hide();
    FlyoutDirectionUIFrame:UnregisterEvent("PLAYER_REGEN_DISABLED");

end


FlyoutDirectionUIFrame:SetScript("OnHide", FlyoutUI.DetachFlyoutUI);
FlyoutDirectionUIFrame:SetScript("OnEvent", FlyoutUI.DetachFlyoutUI);


function FlyoutUI.SetDirection(Direction)
    for i, b in ipairs(Buttons) do
        b:SetChecked(b.Direction == Direction);
    end
end


function FlyoutUI.GetAttachedBFButton()
    return AttachedBFButton;
end
