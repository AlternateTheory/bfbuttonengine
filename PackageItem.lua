--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

    Package for "item" action type

    Notes:

        * WoW v10.0 (and prior)
            - Items are triggered from ItemName
            - Item link is sometimes used for the Tooltip
            - ItemID is used in the retail API calls

]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local PackageItem = Engine.PackageItem;
local Scripts = Engine.Scripts;

-- Take local copy of functions to slightly improve performance (in theory)
local Util = Engine.Util;
local LookupItemBagSlot         = Util.LookupItemBagSlot;
local LookupItemInventorySlot   = Util.LookupItemInventorySlot;

local Events                = Engine.Events;
local WatchForItemInfoEvent = Events.WatchForItemInfoEvent;

local Core                          = Engine.Core;
local ReportEvent                   = Core.ReportEvent;
local Core_RegisterForRangeChecks   = Core.RegisterForRangeChecks;

local C                     = Engine.Constants;
local EVENT_UPDATEACTION    = C.EVENT_UPDATEACTION;
local QUESTION_MARK         = C.QUESTION_MARK;

local GetToyInfo                    = C_ToyBox.GetToyInfo;
local IsToyUsable                   = C_ToyBox.IsToyUsable;

local CooldownFrame_Set             = CooldownFrame_Set;
--local GameTooltip                   = GameTooltip;
--local GameTooltip_SetDefaultAnchor  = GameTooltip_SetDefaultAnchor;
local GetItemCooldown               = C_Item.GetItemCooldown;
local GetItemCount                  = C_Item.GetItemCount;
local GetItemIconByID               = C_Item.GetItemIconByID;
local GetItemInfo                   = C_Item.GetItemInfo;
local GetItemSpell                  = C_Item.GetItemSpell;
local GetMouseFocus                 = Util.GetMouseFocus;
local InCombatLockdown              = InCombatLockdown;
local IsConsumableItem              = C_Item.IsConsumableItem;
local IsCurrentItem                 = C_Item.IsCurrentItem;
local IsEquippableItem              = C_Item.IsEquippableItem;
local IsEquippedItem                = C_Item.IsEquippedItem;
local IsItemInRange                 = C_Item.IsItemInRange;
local IsUsableItem                  = C_Item.IsUsableItem;
local strmatch                      = strmatch;


PackageItem.UpdateIcon                  = Scripts.EmptyF;
PackageItem.UpdateGlow                  = Scripts.EmptyF;
PackageItem.UpdateShine                 = Scripts.EmptyF;
PackageItem.UpdateFlashRegistration     = Scripts.EmptyF;
PackageItem.SwapActionWithButtonAction  = Scripts.SwapActionWithButtonAction;


--[[------------------------------------------------

    - Set Item
    - Capture the base spell and set the Action to that

    - While not in combat the Buttons onclick action will be set
        - values to use in the restricted environment will also be set

    - This function can be called while in combat if the action was swapped using the SecureActionSwap
        - The SecureActionSwap will handle setting the onclick and restricted environment up

--------------------------------------------------]]
function PackageItem.SetAction(BFButton, ItemID, ItemName)

    local Action            = BFButton.Action;
    local RefreshItemName   = GetItemInfo(ItemID);

    if (not RefreshItemName) then
        Events.WatchForItemInfoEvent(ItemID);
        RefreshItemName = ItemName;
    end

    BFButton.Type           = "item";
    BFButton.ItemID         = ItemID;
    BFButton.ItemName       = RefreshItemName;
    BFButton.IsToy          = C_ToyBox.GetToyInfo(ItemID) ~= nil;

    Action.Type             = "item";
    Action.ItemID           = ItemID;
    Action.ItemName         = RefreshItemName;

    if (not InCombatLockdown()) then
        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();
    end

    BFButton:FullUpdate();

end


--[[------------------------------------------------
    When an event occurs that indicates the items might have changed

    This is focused on updating Name
--------------------------------------------------]]
function PackageItem.UpdateAction(BFButton, Type)

    if (Type and Type ~= "item") then
        return;
    end

    local ItemID = BFButton.ItemID;
    local ItemName = GetItemInfo(ItemID);

    if (not ItemName) then
        -- we had a miss on getting item info so wait and try again
        Events.WatchForItemInfoEvent(ItemID);
        return;
    end

    BFButton.IsToy = C_ToyBox.GetToyInfo(ItemID) ~= nil;
    if (BFButton.ItemName ~= ItemName) then
        -- The item name is not correct so update it
        BFButton.ItemName = ItemName;

        local Action = BFButton.Action;
        Action.ItemName = ItemName;

        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();
        ReportEvent(BFButton, EVENT_UPDATEACTION, Action);
    end

end


--[[------------------------------------------------

    Set the on click action that will be triggered

--------------------------------------------------]]
function PackageItem.UpdateAttributes(BFButton)

    local ABW = BFButton.ABW;
    ABW:SetAttribute("type", "item");
    ABW:SetAttribute("typerelease", "item");
    ABW:SetAttribute("item", BFButton.ItemName);

end


--[[------------------------------------------------

    Get the attributes that are used for the on click action
    This function can be called directly to allow the on click attributes to be
    fed into the restricted environment of BF Bars

--------------------------------------------------]]
function PackageItem.Get_Attributes(Action)
    -- return   TypeValue , ActionAttribute , ActionValue
    return "item", "item", Action.ItemName;
end


--[[------------------------------------------------

    Set parameters to describe the action in the restricted environment of the button
    This will support secure changing of the action

--------------------------------------------------]]
function PackageItem.UpdateSecureCursor(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("ActionType", "item");
    ABW:SetAttribute("ActionParameter1", BFButton.ItemID);
    ABW:SetAttribute("ActionParameter2", BFButton.ItemName);
end


--[[------------------------------------------------

    Finalize will reset certain elements of the button that might not be
    affected by the new action (rather than a blanket reset, this is intended to be more performant)

--------------------------------------------------]]
function PackageItem.Finalize(BFButton)

    local Action = BFButton.Action;
    Action.ItemID = nil;
    Action.ItemName = nil;

    local ABW = BFButton.ABW;

    -- Checked
    ABW:SetChecked(false);

    -- Icon
    BFButton.ABWIcon:SetTexture(nil);

    -- Usable
    BFButton:SetDisplayUsable(true);

    -- Cooldown
    BFButton.ABWCooldown:Clear();

    -- Equipped
    BFButton.ABWBorder:Hide();

    -- Text
    BFButton.ABWCount:SetText(nil);

    -- Range (clear registration)
    Core_RegisterForRangeChecks(BFButton);

end


--[[------------------------------------------------
    Full Update
--------------------------------------------------]]
function PackageItem.FullUpdate(BFButton)
    local IconTexture = GetItemIconByID(BFButton.ItemID) or QUESTION_MARK;
    BFButton.ABWIcon:SetTexture(IconTexture);
    BFButton:UpdateChecked();
    BFButton:UpdateEquipped();
    BFButton:UpdateUsable();
    BFButton:UpdateCooldown();
    BFButton:UpdateText();
    BFButton:UpdateRangeCheckRegistration();

    if (GetMouseFocus() == BFButton.ABW) then
        BFButton:UpdateTooltip();
    end
end


--[[------------------------------------------------
    Checked
--------------------------------------------------]]
function PackageItem.UpdateChecked(BFButton)
    BFButton.ABW:SetChecked( IsCurrentItem(BFButton.ItemID) );
end


--[[------------------------------------------------
    Equipped
--------------------------------------------------]]
function PackageItem.UpdateEquipped(BFButton)
    if (IsEquippedItem(BFButton.ItemID)) then
        BFButton.ABWBorder:SetVertexColor(0, 1.0, 0, 0.5);
        BFButton.ABWBorder:Show();
    else
        BFButton.ABWBorder:Hide();
    end
end


--[[------------------------------------------------
    Usable
    Notes:
        Denote if the item should be shaded or fully visible
        There are a lot of considerations when determining this

        Concepts (given in order of priority):
            - If item is equipped then it should be fully visible
                - Unless the item has a use spell, and the spell is not currently usable

            - If item is in bag then it should be fully visible
                - Unless the item has a use spell, and the spell is not currently usable, and the item is not equippable

            - Not equipped and not in bags, then shade

        With toys we just use the IsToyUsable
            - This API call is not always correct
                - In the one example I know of, the IsUsableItem() gives the correct answer
                  But there is no way to tell when (that I am aware of)
                - e.g. X-52 Rocket Helment (inside vs outside)

--------------------------------------------------]]
function PackageItem.UpdateUsable(BFButton)

    local ItemID = BFButton.ItemID;

    if (BFButton.IsToy) then

        BFButton:SetDisplayUsable(IsToyUsable(ItemID));

    elseif (LookupItemInventorySlot(ItemID)) then

        -- Equipped
        if (GetItemSpell(ItemID)) then
            -- Has Use action
            BFButton:SetDisplayUsable(IsUsableItem(ItemID));
        else
            BFButton:SetDisplayUsable(true);
        end

    elseif (LookupItemBagSlot(ItemID)) then

        -- In Bags
        if (IsEquippableItem(ItemID)) then
            -- Is Equippable
            BFButton:SetDisplayUsable(true);
        elseif (GetItemSpell(ItemID)) then
            -- Has Use action
            BFButton:SetDisplayUsable(IsUsableItem(ItemID));
        else
            BFButton:SetDisplayUsable(true);
        end

    else

        -- Not equipped or in bags
        BFButton:SetDisplayUsable(false);

    end

end


--[[------------------------------------------------
    Cooldown
--------------------------------------------------]]
function PackageItem.UpdateCooldown(BFButton)
    CooldownFrame_Set(BFButton.ABWCooldown, GetItemCooldown(BFButton.ItemID));
end


--[[------------------------------------------------
    Text    (Charges, or Counts)
--------------------------------------------------]]
function PackageItem.UpdateText(BFButton)
    local ItemID = BFButton.ItemID;
    local Count = GetItemCount(ItemID, false, true);
    local itemName, itemLink, itemQuality, itemLevel, itemMinLevel, itemType, itemSubType, itemStackCount = GetItemInfo(ItemID);

    if itemStackCount and itemStackCount > 1 then
        BFButton.ABWCount:SetText(Count);
        return;
    end
    BFButton.ABWCount:SetText(nil);
end


--[[------------------------------------------------
    Tooltip
--------------------------------------------------]]
function PackageItem.UpdateTooltip(Obj)

    local ABW = Obj.ABW or Obj;
    local BFButton = ABW.BFButton;
    local ItemID = BFButton.ItemID;

    if (BFButton.IsToy) then
        ButtonForgeGameTooltip:SetToyByItemID(ItemID);
        return;
    end

    local InventorySlot = LookupItemInventorySlot(ItemID);
    if (InventorySlot) then
        ButtonForgeGameTooltip:SetInventoryItem("player", InventorySlot);
        return;
    end

    local Bag, Slot = LookupItemBagSlot(ItemID);
    if (Bag) then
        ButtonForgeGameTooltip:SetBagItem(Bag, Slot);
        return;
    end

    ButtonForgeGameTooltip:SetItemByID(ItemID);

end


--[[------------------------------------------------
    Range Registration
    Note:
        The Target value is not set for Items
        However if this function is triggered by a Macro action then the Target might be set
--------------------------------------------------]]
function PackageItem.UpdateRangeCheckRegistration(BFButton)
    --Core_RegisterForRangeChecks(BFButton, IsItemInRange(BFButton.ItemID, BFButton.Target));
end


--[[------------------------------------------------
    Range Check
    Note:
        The Target value is not set for Items
        However if this function is triggered by a Macro action then the Target might be set
--------------------------------------------------]]
function PackageItem.IsInRange(BFButton)
    return IsItemInRange(BFButton.ItemID, BFButton.Target);
end


--[[------------------------------------------------
    Cursor
--------------------------------------------------]]
function PackageItem.GetCursor(BFButton)
    return "item", BFButton.ItemID, nil, nil;
end


--[[------------------------------------------------
    Get Ordered Parameters
--------------------------------------------------]]
function PackageItem.Get_OrderedParameters(Action)
    -- ActionType , ActionParameter1 , ActionParameter2
    return "item", Action.ItemID, Action.ItemName;
end
