--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine                = AddonTable.ButtonEngine;
local PackageBattlePet      = Engine.PackageBattlePet;
local Scripts               = Engine.Scripts;
local Events                = Engine.Events;
local Util                  = Engine.Util

-- Take local copy of functions to slightly improve performance (in theory)
local C                                 = Engine.Constants;
local C_SUMMON_RANDOM_FAVORITE_PET_GUID = C.SUMMON_RANDOM_FAVORITE_PET_GUID;

local Methods                               = Engine.Methods;
local Methods_SetAction_OrderedParameters   = Methods.SetAction_OrderedParameters;

local C_PetJournal_GetPetInfoByPetID            = C_PetJournal.GetPetInfoByPetID;
local C_PetJournal_GetSummonBattlePetCooldown   = C_PetJournal.GetSummonBattlePetCooldown;
local C_PetJournal_GetSummonedPetGUID           = C_PetJournal.GetSummonedPetGUID;
local C_PetJournal_PetIsSummonable              = C_PetJournal.PetIsSummonable;

local CooldownFrame_Set             = CooldownFrame_Set;
--local GameTooltip                   = GameTooltip;
--local GameTooltip_SetDefaultAnchor  = GameTooltip_SetDefaultAnchor;
local GetMouseFocus                 = Util.GetMouseFocus;
local InCombatLockdown              = InCombatLockdown;


PackageBattlePet.UpdateIcon                 = Scripts.EmptyF;
PackageBattlePet.UpdateAction               = Scripts.EmptyF;
PackageBattlePet.UpdateEquipped				= Scripts.EmptyF;
PackageBattlePet.UpdateText					= Scripts.EmptyF;
PackageBattlePet.UpdateGlow					= Scripts.EmptyF;
PackageBattlePet.UpdateShine				= Scripts.EmptyF;
PackageBattlePet.UpdateFlashRegistration	= Scripts.EmptyF;
PackageBattlePet.UpdateRangeCheckRegistration	= Scripts.EmptyF;
PackageBattlePet.SwapActionWithButtonAction	= Scripts.SwapActionWithButtonAction;


--[[------------------------------------------------

    - Set BattlePet action

    - While not in combat the Buttons onclick action will be set
        - values to use in the restricted environment will also be set

    - This function can be called while in combat if the action was swapped using the SecureActionSwap
        - The SecureActionSwap will handle setting the onclick and restricted environment up

--------------------------------------------------]]
function PackageBattlePet.SetAction(BFButton, BattlePetGUID)

    if (BattlePetGUID == C_SUMMON_RANDOM_FAVORITE_PET_GUID) then
        -- It's the random favorite button, use a different code path
        return BFButton:SetAction_OrderedParameters("favoritebattlepet");
    end

    local speciesID, customName, level, xp, maxXp, displayID, isFavorite, name, icon = C_PetJournal_GetPetInfoByPetID(BattlePetGUID);

    BFButton.Type           = "battlepet";
    BFButton.BattlePetGUID  = BattlePetGUID;

    local Action            = BFButton.Action;
    Action.Type             = "battlepet";
    Action.BattlePetGUID    = BattlePetGUID;
    Action.BattlePetName    = customName or name or Action.BattlePetName;

    if (not InCombatLockdown()) then
        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();
    end

    if not icon and not BFButton.RegisteredForCallback then
        Events.RegisterEventCallback("UPDATE_SUMMONPETS_ACTION", PackageBattlePet.CallbackBattlePetsAvailable, BFButton)
        BFButton.RegisteredForCallback = true
    end
    BFButton.ABWIcon:SetTexture(icon);
    BFButton:FullUpdate();

end


function PackageBattlePet.CallbackBattlePetsAvailable(BFButton)
    BFButton:UpdateAction("battlepet")
end


function PackageBattlePet.UpdateAction(BFButton, Type)
    if Type ~= "battlepet" then
        return
    end
    local speciesID, customName, level, xp, maxXp, displayID, isFavorite, name, icon = C_PetJournal_GetPetInfoByPetID(BFButton.BattlePetGUID);
    if icon then
        BFButton.Action.BattlePetName    = customName or name;
        BFButton.ABWIcon:SetTexture(icon)
        BFButton:FullUpdate()
        Events.UnregisterEventCallback("UPDATE_SUMMONPETS_ACTION", PackageBattlePet.CallbackBattlePetsAvailable, BFButton)
        BFButton.RegisteredForCallback = nil
    end
end


--[[------------------------------------------------

    Set the on click action that will be triggered

--------------------------------------------------]]
function PackageBattlePet.UpdateAttributes(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("type", "macro");
    ABW:SetAttribute("typerelease", "macro");
    ABW:SetAttribute("macrotext", format("/summonpet %s", BFButton.BattlePetGUID));
end


--[[------------------------------------------------

    Get the attributes that are used for the on click action
    This function can be called directly to allow the on click attributes to be
    fed into the restricted environment of BF Bars

--------------------------------------------------]]
function PackageBattlePet.Get_Attributes(Action)
    -- return   TypeValue , ActionAttribute , ActionValue
    return      "macro"   , "macrotext"     , format( "/summonpet %s" , Action.BattlePetGUID ) ;
end


--[[------------------------------------------------

    Set parameters to describe the action in the restricted environment of the button
    This will support secure changing of the action

--------------------------------------------------]]
function PackageBattlePet.UpdateSecureCursor(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("ActionType", "battlepet");
    ABW:SetAttribute("ActionParameter1", BFButton.BattlePetGUID);
    ABW:SetAttribute("ActionParameter2", nil);
    ABW:SetAttribute("ActionParameter3", nil);
end


--[[------------------------------------------------

    Finalize will reset certain elements of the button that might not be
    affected by the new action (rather than a blanket reset, this is intended to be more performant)

--------------------------------------------------]]
function PackageBattlePet.Finalize(BFButton)

    local ABW = BFButton.ABW;

    -- Checked
    ABW:SetChecked(false);

    -- Icon
    BFButton.ABWIcon:SetTexture(nil);

    -- Usable
    BFButton:SetDisplayUsable(true);

    -- Cooldown
    BFButton.ABWCooldown:Clear();

    if BFButton.RegisteredForCallback then
        Events.UnregisterEventCallback("UPDATE_SUMMONPETS_ACTION", PackageBattlePet.CallbackBattlePetsAvailable, BFButton)
        BFButton.RegisteredForCallback = nil
    end
end


--[[------------------------------------------------
    Full Update
--------------------------------------------------]]
function PackageBattlePet.FullUpdate(BFButton)
    BFButton:UpdateChecked();
    BFButton:UpdateUsable();
    BFButton:UpdateCooldown();
    
    if (GetMouseFocus() == BFButton.ABW) then
        BFButton:UpdateTooltip();
    end
end


--[[------------------------------------------------
    Checked
--------------------------------------------------]]
function PackageBattlePet.UpdateChecked(BFButton)
    if (BFButton.BattlePetGUID == C_PetJournal_GetSummonedPetGUID()) then
        BFButton.ABW:SetChecked(true);
    else
        BFButton.ABW:SetChecked(false);
    end
end


--[[------------------------------------------------
    Usable
--------------------------------------------------]]
function PackageBattlePet.UpdateUsable(BFButton)
    BFButton:SetDisplayUsable(C_PetJournal_PetIsSummonable(BFButton.BattlePetGUID));
end


--[[------------------------------------------------
    Cooldown
--------------------------------------------------]]
function PackageBattlePet.UpdateCooldown(BFButton)
    CooldownFrame_Set(BFButton.ABWCooldown, C_PetJournal_GetSummonBattlePetCooldown());
end


--[[------------------------------------------------
    Tooltip	(Copied from how the Pet Journal Shows the Tooltip)
--------------------------------------------------]]
function PackageBattlePet.UpdateTooltip(Obj)
    local ABW = Obj.ABW or Obj;
    ButtonForgeGameTooltip:SetCompanionPet(ABW.BFButton.BattlePetGUID);
end


--[[------------------------------------------------
    Cursor
--------------------------------------------------]]
function PackageBattlePet.GetCursor(BFButton)
    return "battlepet", BFButton.BattlePetGUID, nil, nil;
end


--[[------------------------------------------------
    Get Ordered Parameters
--------------------------------------------------]]
function PackageBattlePet.Get_OrderedParameters(Action)
    -- ActionType , ActionParameter1 , ActionParameter2 , ActionParameter3
    return "battlepet", Action.BattlePetGUID;
end
