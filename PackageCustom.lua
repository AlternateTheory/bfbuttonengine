--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2020
    
    Scripts for Custom Actions

]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local PackageCustom = Engine.PackageCustom;
local Scripts = Engine.Scripts;
local CustomActionRegistry = Engine.CustomActionRegistry;

local Core = Engine.Core;
local Util = Engine.Util;
local C = Engine.Constants;

local GetMouseFocus = Util.GetMouseFocus

PackageCustom.UpdateEquipped 			    = Scripts.EmptyF;
PackageCustom.UpdateShine				    = Scripts.EmptyF;
PackageCustom.SwapActionWithButtonAction	= Scripts.SwapActionWithButtonAction;

--[[------------------------------------------------

    - Set Custom

    - While not in combat the Buttons onclick action will be set
        - values to use in the restricted environment will also be set

    - This function can be called while in combat if the action was swapped using the SecureActionSwap
        - The SecureActionSwap will handle setting the onclick and restricted environment up

--------------------------------------------------]]
function PackageCustom.SetAction(BFButton, CustomName, CustomData)

    local Action        = BFButton.Action;

    BFButton.Type           = "custom";
    BFButton.CustomName     = CustomName;
    BFButton.CustomData     = CustomData;
    BFButton.CustomAction   = CustomActionRegistry.GetCustomAction(CustomName);

    Action.Type             = "custom";
    Action.CustomName       = CustomName;
    Action.CustomData       = CustomData;

    if (not InCombatLockdown()) then
        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();
    end

    BFButton:FullUpdate();

end


--[[------------------------------------------------

--------------------------------------------------]]
function PackageCustom.UpdateAction(BFButton, Type)
    if (not Type or Type == "custom") then
        BFButton:UpdateAttributes();
    end
end


--[[------------------------------------------------

    Set the on click action that will be triggered

--------------------------------------------------]]
function PackageCustom.UpdateAttributes(BFButton)

    local ABW = BFButton.ABW;
    local Type, Attr, Val = BFButton.CustomAction.GetAction( BFButton.CustomData );
    ABW:SetAttribute("type", Type);
    ABW:SetAttribute("typerelease", Type);
    if Attr == "ID" then
        ABW:SetID(Val)
    else
        ABW:SetAttribute(Attr, Val);
    end

end


--[[------------------------------------------------

    Get the attributes that are used for the on click action
    This function can be called directly to allow the on click attributes to be
    fed into the restricted environment of BF Bars

--------------------------------------------------]]
function PackageCustom.Get_Attributes(Action)

    local CustomAction = CustomActionRegistry.GetCustomAction( Action.CustomName );

    -- return   TypeValue , ActionAttribute , ActionValue
    return CustomAction.GetAction( Action.CustomData );

end


--[[------------------------------------------------

    Set parameters to describe the action in the restricted environment of the button
    This will support secure changing of the action

--------------------------------------------------]]
function PackageCustom.UpdateSecureCursor(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("ActionType", "custom");
    ABW:SetAttribute("ActionParameter1", BFButton.CustomName);
    ABW:SetAttribute("ActionParameter2", BFButton.CustomData);
end


--[[------------------------------------------------

    Finalize will reset certain elements of the button that might not be
    affected by the new action (rather than a blanket reset, this is intended to be more performant)

--------------------------------------------------]]
function PackageCustom.Finalize(BFButton)

    local Action = BFButton.Action;
    Action.CustomName = nil;
    Action.CustomData = nil;

    local ABW = BFButton.ABW;

    -- Checked
    ABW:SetChecked(false);

    -- Glow
    ActionButton_HideOverlayGlow(ABW);

    -- Icon
    local ABWIcon = BFButton.ABWIcon;
    ABWIcon:SetTexture(nil);
    ABWIcon:SetTexCoord(0, 1, 0, 1);

    -- Usable
    BFButton:SetDisplayUsable(true);

    -- Cooldown
    Core.ClearChargeCooldown(ABW);
    BFButton.ABWCooldown:Clear();

    -- Equipped

    -- Text
    BFButton.ABWCount:SetText(nil);

    -- Flash
    Core.RegisterForFlash(BFButton);

    -- Range
    Core.RegisterForRangeChecks(BFButton);

    if not InCombatLockdown() then
        ABW:SetID(0)
    end

end


--[[------------------------------------------------
    Full Update
--------------------------------------------------]]
function PackageCustom.FullUpdate(BFButton)
    BFButton:UpdateIcon();
    BFButton:UpdateChecked();
    BFButton:UpdateUsable();
    BFButton:UpdateCooldown();
    BFButton:UpdateText();
    BFButton:UpdateGlow();
    BFButton:UpdateFlashRegistration();
    BFButton:UpdateRangeCheckRegistration();

    if (GetMouseFocus() == BFButton.ABW) then
        BFButton:UpdateTooltip();
    end
end


--[[------------------------------------------------
    Icon
--------------------------------------------------]]
function PackageCustom.UpdateIcon(BFButton)
    local Icon, left, right, top, bottom = BFButton.CustomAction.GetIcon(BFButton.ABW, BFButton.CustomData);
    if (left) then
        BFButton.ABWIcon:SetTexCoord(left, right, top, bottom);
    end
    BFButton.ABWIcon:SetTexture(Icon);
end


--[[------------------------------------------------
    Checked
--------------------------------------------------]]
function PackageCustom.UpdateChecked(BFButton)
    BFButton.ABW:SetChecked(BFButton.CustomAction.IsChecked(BFButton.ABW, BFButton.CustomData));
end


--[[------------------------------------------------
    Usable
--------------------------------------------------]]
function PackageCustom.UpdateUsable(BFButton)
    local IsUsable = BFButton.CustomAction.IsUsable;
    if (IsUsable) then
        BFButton:SetDisplayUsable(IsUsable(BFButton.ABW, BFButton.CustomData));
    end
end


--[[------------------------------------------------
    Cooldown
--------------------------------------------------]]
function PackageCustom.UpdateCooldown(BFButton)
    local GetCooldown = BFButton.CustomAction.GetCooldown;
    if (GetCooldown) then
        CooldownFrame_Set(BFButton.ABWCooldown, GetCooldown(BFButton.ABW, BFButton.CustomData));
    end
end


--[[------------------------------------------------
    Text
--------------------------------------------------]]
function PackageCustom.UpdateText(BFButton)
    local GetText = BFButton.CustomAction.GetText;
    if (GetText) then
        BFButton.ABWName:SetText(GetText(BFButton.ABW, BFButton.CustomData));
    end
end


--[[------------------------------------------------
    Glow
--------------------------------------------------]]
function PackageCustom.UpdateGlow(BFButton)
    local IsGlowing = BFButton.CustomAction.IsGlowing;
    if (not IsGlowing) then
        return;
    elseif (IsGlowing(BFButton.ABW, BFButton.CustomData)) then
        BFButton:ShowOverlayGlow()
    else
        BFButton:HideOverlayGlow()
    end
end


--[[------------------------------------------------
    Tooltip
--------------------------------------------------]]
function PackageCustom.UpdateTooltip(Obj)
    local ABW = Obj.ABW or Obj;
    local UpdateTooltip = ABW.BFButton.CustomAction.UpdateTooltip
    if UpdateTooltip then
        UpdateTooltip(ABW, ButtonForgeGameTooltip, ABW.BFButton.CustomData);
    end
end


--[[------------------------------------------------
    Flash	(registers and Deregisters for flash)
--------------------------------------------------]]
function PackageCustom.UpdateFlashRegistration(BFButton)
    local IsFlashing = BFButton.CustomAction.IsFlashing;
    if (IsFlashing) then
        Core.RegisterForFlash(BFButton, IsFlashing(BFButton.ABW, BFButton.CustomData));
    end
end


--[[------------------------------------------------
    Range Registration
--------------------------------------------------]]
function PackageCustom.UpdateRangeCheckRegistration(BFButton)
    local IsInRange = BFButton.CustomAction.IsInRange;
    if (IsInRange) then
        Core.RegisterForRangeChecks(BFButton, IsInRange(BFButton.ABW, BFButton.CustomData));
    end
end


--[[------------------------------------------------
    Range Check
--------------------------------------------------]]
function PackageCustom.IsInRange(BFButton)
    local IsInRange = BFButton.CustomAction.IsInRange;
    if (IsInRange) then
        return IsInRange(BFButton.ABW, BFButton.CustomData);
    end
end


--[[------------------------------------------------
    Cursor
--------------------------------------------------]]
function PackageCustom.GetCursor(BFButton)
    return "custom", BFButton.CustomName, BFButton.CustomData, nil, BFButton.CustomAction.GetIcon(BFButton.ABW, BFButton.CustomData);
end


--[[------------------------------------------------
    Get Ordered Parameters
--------------------------------------------------]]
function PackageCustom.Get_OrderedParameters(Action)
    -- ActionType , ActionParameter1 , ActionParameter2
    return "custom", Action.CustomName, Action.CustomData;
end
