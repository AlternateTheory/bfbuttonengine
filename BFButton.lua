--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;

local C = Engine.Constants;
local Core = Engine.Core;
local Util = Engine.Util;
local SecureActionSwap = Engine.SecureActionSwap;
local SecureManagement = Engine.SecureManagement;

local Cursor = Engine.Cursor;

local FlyoutUI = Engine.FlyoutUI;
local PackageEmpty = Engine.PackageEmpty;
local PackageSpell = Engine.PackageSpell;
local PackageItem = Engine.PackageItem;
local PackageMacro = Engine.PackageMacro;
local PackageMount = Engine.PackageMount;
local PackageMountFavorite = Engine.PackageMountFavorite;
local PackageBattlePet = Engine.PackageBattlePet;
local PackageBattlePetFavorite = Engine.PackageBattlePetFavorite;
local PackageFlyout = Engine.PackageFlyout;
local PackageEquipmentSet = Engine.PackageEquipmentSet;
local PackageCustom = Engine.PackageCustom;


local Packages = {};
Packages["empty"] = PackageEmpty;
Packages["spell"] = PackageSpell;
Packages["item"] = PackageItem;
Packages["macro"] = PackageMacro;
Packages["mount"] = PackageMount;
Packages["favoritemount"] = PackageMountFavorite;
Packages["battlepet"] = PackageBattlePet;
Packages["favoritebattlepet"] = PackageBattlePetFavorite;
Packages["flyout"] = PackageFlyout;
Packages["equipmentset"] = PackageEquipmentSet;
Packages["custom"] = PackageCustom;


local BFButtonMixin = {};


--[[------------------------------------------------
    Set/GetFlyoutDirection
    * Sets attributes, so cannot be done during combat
--------------------------------------------------]]
function BFButtonMixin:SetFlyoutDirection(Direction)
    Direction = Direction or "UP";
    self.FlyoutDirection = Direction;
    if (self.ABW:GetAttribute("flyoutDirection") ~= Direction) then
        self.ABW:SetAttribute("flyoutDirection", Direction);
        SpellFlyout:Hide();
        ButtonForge_SpellFlyout:Hide();
        if (self.Type == "flyout") then
            self:FullUpdate();
        end
    end

    Core.ReportEvent(self, C.EVENT_FLYOUTDIRECTION, Direction);
end
function BFButtonMixin:GetFlyoutDirection()
    return self.FlyoutDirection;
end


--[[------------------------------------------------
    SetAllowKeybindMode
    * Determines if the button will show the KeybindHighlightTexture in keybind mode
--------------------------------------------------]]
function BFButtonMixin:SetAllowKeybindMode(Value)
    self.AllowKeybindMode = Value;
    self:UpdateKeybindHighlight();
end
function BFButtonMixin:GetAllowKeybindMode()
    return self.AllowKeybindMode;
end


--[[------------------------------------------------
    Set/GetKeybind
    * Set the keybind itself
--------------------------------------------------]]
function BFButtonMixin:SetKeybind(Keybind)

    self.Keybind = Keybind;
    if (not InCombatLockdown()) then
        local ABW = self.ABW;
        ABW:SetAttribute("Keybind", Keybind);
        ABW:Execute([[ Keybind = self:GetAttribute("Keybind") ]]);
        self:UpdateKeybind();
    end
    --Core.ReportEvent(self, C.EVENT_KEYBIND, Keybind);

end
function BFButtonMixin:GetKeybind()
    return self.Keybind;
end


--[[------------------------------------------------
    Set/GetKeyBindText
    * Set the keybind text that will be shown
--------------------------------------------------]]
function BFButtonMixin:SetKeyBindText(KeyBindText)

    self.KeyBindText = KeyBindText;
    self:UpdateHotKey();
    Core.ReportEvent(self, C.EVENT_KEYBINDTEXT, KeyBindText);

end
function BFButtonMixin:GetKeyBindText()
    return self.KeyBindText;
end


--[[------------------------------------------------
    Set/GetEnabled
    * This can be called from the restricted environment during combat
      In this case the restricted environment will have handled the hide/show and attribute setting
--------------------------------------------------]]
function BFButtonMixin:SetEnabled(Enabled, BlockFullUpdate)

    self.Enabled = Enabled;
    if (not InCombatLockdown()) then
        local ABW = self.ABW;
        ABW:SetAttribute("Enabled", Enabled);
        ABW:Execute( [[ Enabled = self:GetAttribute("Enabled") ]]);
        self:UpdateHideOnCombat();
        self:UpdateShown();
        self:UpdateKeybind();
    end

    if (Enabled and not BlockFullUpdate) then
        self:FullUpdate();
    --else
        --Core.ReleaseTempButtonResources(BFButton);
    end

end
function BFButtonMixin:GetEnabled()
    return self.Enabled;
end


--[[------------------------------------------------
    Set/GetRespondToMouse
--------------------------------------------------]]
function BFButtonMixin:SetRespondToMouse(RespondToMouse)
    self.RespondToMouse = RespondToMouse;
    if (not InCombatLockdown()) then
        local ABW = self.ABW;
        ABW:SetAttribute("RespondToMouse", RespondToMouse);
        ABW:Execute( [[ RespondToMouse = self:GetAttribute("RespondToMouse") ]] );
        self:UpdateDisableMouseOnCombat();
        self:UpdateRespondToMouse();
    end
    Core.ReportEvent(self, C.EVENT_RESPONDTOMOUSE, RespondToMouse);
end
function BFButtonMixin:GetRespondToMouse()
    return self.RespondToMouse;
end


--[[------------------------------------------------
    Set/GetAlpha
--------------------------------------------------]]
function BFButtonMixin:SetAlpha(Alpha)
    self.Alpha = Alpha;
    self:UpdateAlpha();
    Core.ReportEvent(self, C.EVENT_ALPHA, Alpha);

end
function BFButtonMixin:GetAlpha()
    return self.Alpha;
end


--[[------------------------------------------------
    Set/GetLocked
    * Cannot be called during Combat
    * Actions can still be dragged off buttons by holding the PICKUP key down
    * Or by swapping another action onto the button
--------------------------------------------------]]
function BFButtonMixin:SetLocked(Locked)
    self.Locked = Locked;
    self.ABW:SetAttribute("Locked", Locked);
    Core.ReportEvent(self, C.EVENT_LOCKED, Locked);
end
function BFButtonMixin:GetLocked()
    return self.Locked;
end


--[[------------------------------------------------
    Set/GetFullLock
--------------------------------------------------]]
function BFButtonMixin:SetFullLock(FullLock)
    self.FullLock = FullLock;
    self.ABW:SetAttribute("FullLock", FullLock);
    Core.ReportEvent(self, C.EVENT_FULLLOCK, FullLock);
end
function BFButtonMixin:GetFullLock()
    return self.FullLock;
end


--[[------------------------------------------------
    Set/GetSource
    * The cursor will set itself to the button with a drag action
      But the button will retain the action
--------------------------------------------------]]
function BFButtonMixin:SetSource(Source)
    self.Source = Source;
    self.ABW:SetAttribute("Source", Source);
    Core.ReportEvent(self, C.EVENT_SOURCE, Source);
end
function BFButtonMixin:GetSource()
    return self.Source;
end
    

--[[------------------------------------------------
    Set/GetAlwaysShowGrid A.K.A ShowEmptyButtons
    * Badly named! (though related to the retail term)
    * If not set, empty buttons will be hidden
    * These buttons will show when the cursor holds an action that could be put on them
--------------------------------------------------]]
function BFButtonMixin:SetAlwaysShowGrid(AlwaysShowGrid)
    self.AlwaysShowGrid = AlwaysShowGrid;
    
    if (not InCombatLockdown()) then
        local ABW = self.ABW;
        ABW:SetAttribute("AlwaysShowGrid", AlwaysShowGrid);
        ABW:Execute([[ AlwaysShowGrid = self:GetAttribute("AlwaysShowGrid") ]]);
        self:UpdateHideOnCombat();
        self:UpdateShown();
        self:UpdateKeybind();
    end

    Core.ReportEvent(self, C.EVENT_ALWAYSSHOWGRID, AlwaysShowGrid);
end
function BFButtonMixin:GetAlwaysShowGrid()
    return self.AlwaysShowGrid;
end


--[[------------------------------------------------
    Set/GetShowTooltip
--------------------------------------------------]]
function BFButtonMixin:SetShowTooltip(ShowTooltip)
    self.ShowTooltip = ShowTooltip;
    Core.ReportEvent(self, C.EVENT_SHOWTOOLTIP, ShowTooltip);
end
function BFButtonMixin:GetShowTooltip()
    return self.ShowTooltip;
end


--[[------------------------------------------------
    Set/GetShowKeyBindText
--------------------------------------------------]]
function BFButtonMixin:SetShowKeyBindText(ShowKeyBindText)
    self.ShowKeyBindText = ShowKeyBindText;
    self:UpdateHotKey();
    Core.ReportEvent(self, C.EVENT_SHOWKEYBINDTEXT, ShowKeyBindText);
end
function BFButtonMixin:GetShowKeyBindText()
    return self.ShowKeyBindText;
end


--[[------------------------------------------------
    Set/GetShowCounts
--------------------------------------------------]]
function BFButtonMixin:SetShowCounts(ShowCounts)
    self.ShowCounts = ShowCounts;
    Core.ReportEvent(self, C.EVENT_SHOWCOUNTS, ShowCounts);
end
function BFButtonMixin:GetShowCounts()
    return self.ShowCounts;
end


--[[------------------------------------------------
    Set/GetShowMacroName
--------------------------------------------------]]
function BFButtonMixin:SetShowMacroName(ShowMacroName)
    self.ShowMacroName = ShowMacroName;
    self:UpdateText();
    Core.ReportEvent(self, C.EVENT_SHOWMACRONAME, ShowMacroName);
end
function BFButtonMixin:GetShowMacroName()
    return self.ShowMacroName;
end


--[[------------------------------------------------
    Cooldown
--------------------------------------------------]]
function BFButtonMixin:SetCooldownType(Type)
    local Cooldown = self.ABWCooldown;
    if (Cooldown.currentCooldownType ~= Type) then
        if (Type == COOLDOWN_TYPE_LOSS_OF_CONTROL) then
            Cooldown:SetEdgeTexture("Interface\\Cooldown\\UI-HUD-ActionBar-LoC");
            Cooldown:SetSwipeColor(0.17, 0, 0);
            Cooldown:SetHideCountdownNumbers(true);
            Cooldown.currentCooldownType = COOLDOWN_TYPE_LOSS_OF_CONTROL;
        else
            Cooldown:SetEdgeTexture("Interface\\Cooldown\\UI-HUD-ActionBar-SecondaryCooldown");
            Cooldown:SetSwipeColor(0, 0, 0);
            Cooldown:SetHideCountdownNumbers(false);
            Cooldown.currentCooldownType = COOLDOWN_TYPE_NORMAL;
        end
    end
end


--[[------------------------------------------------
    Set the usable indication of the button
--------------------------------------------------]]
function BFButtonMixin:SetDisplayUsable(IsUsable, NotEnoughMana)
    if (IsUsable) then
        self.ABWIcon:SetVertexColor(1.0, 1.0, 1.0);
        self.ABWNormalTexture:SetVertexColor(1.0, 1.0, 1.0);
    elseif (NotEnoughMana) then
        self.ABWIcon:SetVertexColor(0.5, 0.5, 1.0);
        self.ABWNormalTexture:SetVertexColor(0.5, 0.5, 1.0);
    else
        self.ABWIcon:SetVertexColor(0.4, 0.4, 0.4);
        self.ABWNormalTexture:SetVertexColor(1.0, 1.0, 1.0);
    end
end


--[[------------------------------------------------
    The spell glow functions are lifted from Bliz code and adapted
    A little so it would function as expected for buttons
    that can be hidden and reappear
--------------------------------------------------]]
local function SpellActivationAlert_OnShow(self)
    self.ProcStartAnim:Play()
end

local function BFActionButton_SetupOverlayGlow(ABW)
    ActionButton_SetupOverlayGlow(ABW)
    ABW.SpellActivationAlert:SetScript("OnShow", SpellActivationAlert_OnShow)
end

function BFButtonMixin:ShowOverlayGlow()
    local ABW = self.ABW
    BFActionButton_SetupOverlayGlow(ABW)
    if not ABW.SpellActivationAlert:IsShown() then
        ABW.SpellActivationAlert:Show()
    end
end

function BFButtonMixin:HideOverlayGlow()
    local ABW = self.ABW
    if not ABW.SpellActivationAlert then
        return
    end
    if ABW.SpellActivationAlert.ProcStartAnim:IsPlaying() then
        ABW.SpellActivationAlert.ProcStartAnim:Stop()
    end
    ABW.SpellActivationAlert:Hide()
end

-- FIXME this and the above is code quickly introduced to resolve specific issues
-- as the whole Core / BFButton warrants refactor it can be tidied then
--[[------------------------------------------------
    The Charge Cooldown code is lifted from Blizz code
    The main change is the allocation of the name
    The reason to do this though is a taint issue when calling their function.

    I suspect my addon calling this will taint the numChargeCooldowns
    counter when an on demand cooldown frame is allocated
    then if a Blizz button has to go through and get an on demand frame the taint gets introduced to the Blizz
    object, it's not ideal, and hopefully the frame name does not have relevance (doubtful it does)
--------------------------------------------------]]
-- local numChargeCooldowns = 0;
local function CreateChargeCooldownFrame(parent)
    -- numChargeCooldowns = numChargeCooldowns + 1;
    -- local cooldown = CreateFrame("Cooldown", "ChargeCooldown"..numChargeCooldowns, parent, "CooldownFrameTemplate");
    local cooldown = CreateFrame("Cooldown", parent:GetName() .. "ChargeCooldown", parent, "CooldownFrameTemplate")
    cooldown:SetHideCountdownNumbers(true);
    cooldown:SetDrawSwipe(false);
    local icon = parent.Icon or parent.icon;
    cooldown:SetPoint("TOPLEFT", icon, "TOPLEFT", 2, -2);
    cooldown:SetPoint("BOTTOMRIGHT", icon, "BOTTOMRIGHT", -2, 2);
    cooldown:SetFrameLevel(parent:GetFrameLevel());
    return cooldown;
end

function Core.StartChargeCooldown(parent, chargeStart, chargeDuration, chargeModRate)
    if chargeStart == 0 then
        Core.ClearChargeCooldown(parent);
        return;
    end

    parent.chargeCooldown = parent.chargeCooldown or CreateChargeCooldownFrame(parent);

    CooldownFrame_Set(parent.chargeCooldown, chargeStart, chargeDuration, true, true, chargeModRate);
end

function Core.ClearChargeCooldown(parent)
    if parent.chargeCooldown then
        parent.chargeCooldown:Clear()
    end
end


--[[------------------------------------------------
    Refresh the displayed HotKey
--------------------------------------------------]]
function BFButtonMixin:UpdateHotKey()

    local HotKey = self.ABWHotKey;
    local KeyBindText = self.KeyBindText or "";
    if (KeyBindText == "" or not self.ShowKeyBindText) then
        HotKey:SetText(RANGE_INDICATOR);
        HotKey:Hide();
    else
        HotKey:SetText(KeyBindText);
        HotKey:Show();
    end

    -- Lifted this code from Retain WoW ActionBarActionButtonMixin:UpdateHotkeys
    local ABW = self.ABW;
    local frameWidth, frameHeight = ABW:GetSize();

    if ( IsBindingForGamePad(KeyBindText) ) then
        -- Allow gamepad binding to go all the way across and overlap the border for more space
        HotKey:SetSize(frameWidth, 16);
        HotKey:SetPoint("TOPRIGHT", ABW, "TOPRIGHT", 0, 0);
    else
        -- Tuck in KBM binding a bit to be inside the border
        HotKey:SetSize(frameWidth-8, 10);
        HotKey:SetPoint("TOPRIGHT", ABW, "TOPRIGHT", -4, -5);
    end

    self:UpdateRangeCheckRegistration();

end


--[[------------------------------------------------
    The Keybind highlight is used to indicate that the button is in keybind mode
    The code path for setting up the keybinding itself is in the CustomAction_KeybindMode file
--------------------------------------------------]]
function BFButtonMixin:UpdateKeybindHighlight()

    local ABW = self.ABW;
    if (Core.InKeybindMode() and self.AllowKeybindMode) then
        ABW.KeybindHighlightTexture:Show();
        if Util.GetMouseFocus() == ABW then
            ABW.KeybindHighlightTexture:SetAlpha(1);
        else
            ABW.KeybindHighlightTexture:SetAlpha(0.4);
        end
    else
        ABW.KeybindHighlightTexture:Hide();
    end

end


--[[------------------------------------------------
    Set indication for if the button action is in range or not of the target
--------------------------------------------------]]
function BFButtonMixin:SetInRangeIndication(IsInRange)
    if IsInRange then
        self.ABWHotKey:SetVertexColor(ACTIONBAR_HOTKEY_FONT_COLOR:GetRGB());
    else
        self.ABWHotKey:SetVertexColor(RED_FONT_COLOR:GetRGB());
    end
end


--[[------------------------------------------------
    Notes:
        * Not combat safe
        * The functions that call this will perform the combat check
--------------------------------------------------]]
function BFButtonMixin:UpdateHideOnCombat()

    if self.AlwaysShowGrid == false and self.Type == "empty" and self.Enabled then
        SecureManagement.RegisterForHideOnCombat(self.ABW);
    else
        SecureManagement.DeregisterForHideOnCombat(self.ABW);
    end

end


--[[------------------------------------------------
    Notes:
        * Not combat safe
        * The functions that call this will perform the combat check
--------------------------------------------------]]
function BFButtonMixin:UpdateDisableMouseOnCombat()

    if not self.RespondToMouse then
        SecureManagement.RegisterForDisableMouseOnCombat(self.ABW);
    else
        SecureManagement.DeregisterForDisableMouseOnCombat(self.ABW);
    end

end


function BFButtonMixin:UpdateAlpha()
    local Alpha = self.Alpha;
    local ABW = self.ABW;
    if Alpha < 0.65 and not InCombatLockdown() and ( Cursor.HasValidAction() and IsModifiedClick("PICKUPACTION") ) then
        self.ABW:SetAlpha(0.65);
    elseif ABW:GetAlpha() ~= Alpha then
        self.ABW:SetAlpha(Alpha);
    end
end

--[[------------------------------------------------
    Notes:
        * Not combat safe
        * The functions that call this will perform the combat check
--------------------------------------------------]]
function BFButtonMixin:UpdateShown()

    local ABW = self.ABW;
    if self.Enabled and ( self.AlwaysShowGrid or self.Type ~= "empty" or Cursor.HasValidAction() ) then
        if not ABW:IsShown() then
            ABW:Show();
        end
    else
        if ABW:IsShown() then
            ABW:Hide();
        end
    end

end


--[[------------------------------------------------
    Notes:
        * Not combat safe
        * The functions that call this will perform the combat check
--------------------------------------------------]]
function BFButtonMixin:UpdateKeybind()

    local ABW = self.ABW;
    local Keybind = self.Keybind;
    local ActiveKeybind = ABW:GetAttribute("ActiveKeybind");
    if self.Enabled and (self.AlwaysShowGrid or self.Type ~= "empty") and Keybind ~= nil and Keybind ~= "" then
        if (ActiveKeybind ~= Keybind) then
            ABW:SetAttribute("ActiveKeybind", Keybind);
            ABW:Execute([[ ActiveKeybind = self:GetAttribute("ActiveKeybind") ]]);
            ClearOverrideBindings(ABW);
            SetOverrideBindingClick(ABW, false, Keybind, ABW:GetName(), "KeyBind");
        end
    else
        if ActiveKeybind ~= nil then
            ABW:SetAttribute("ActiveKeybind", nil);
            ABW:Execute([[ ActiveKeybind = nil ]]);
            ClearOverrideBindings(ABW);
        end
    end

end


--[[------------------------------------------------
    Notes:
        * Not combat safe
        * The functions that call this will perform the combat check
--------------------------------------------------]]
function BFButtonMixin:UpdateRespondToMouse()

    local ABW = self.ABW;
    if self.RespondToMouse or ( Cursor.HasValidAction() and IsModifiedClick("PICKUPACTION") ) then
        if not ABW:IsMouseEnabled() then
            ABW:EnableMouse(true);
        end
    else
        if ABW:IsMouseEnabled() then
            ABW:EnableMouse(false);
        end
    end

end


--[[------------------------------------------------
    Set Action From Cursor Values
--------------------------------------------------]]
function BFButtonMixin:SetAction_CursorValues(Command, Data, Subvalue, SubSubvalue)
    if (InCombatLockdown()) then
        return false, C.ERROR_IN_COMBAT_LOCKDOWN;
    end

    if (Command == "spell") then            self:SetAction_OrderedParameters("spell", SubSubvalue);	    -- Data = Index, Subvalue = Book (spell/pet), SubSubvalue = spell ID
    elseif (Command == "item") then         self:SetAction_OrderedParameters("item", Data);			    -- Data = ID, Subvalue = Link
    elseif (Command == "macro") then        self:SetAction_OrderedParameters("macro", Data);			-- Data = Index
    elseif (Command == "mount") then        self:SetAction_OrderedParameters("mount", Data);			-- Data = MountID
    elseif (Command == "battlepet") then    self:SetAction_OrderedParameters("battlepet", Data);		-- Data = GUID
    elseif (Command == "flyout") then       self:SetAction_OrderedParameters("flyout", Data);	        -- Data = ID, Subvalue = Icon
    elseif (Command == "equipmentset") then self:SetAction_OrderedParameters("equipmentset", Data);		-- Data = Name
    elseif (Command == "custom") then       self:SetAction_OrderedParameters("custom", Data, Subvalue);	-- Data = CustomName, Subvalue = CustomData
    elseif (Command == nil) then            self:SetAction_OrderedParameters("empty");
    else
        return false;
    end
    return true;
end


--[[------------------------------------------------
    Provide a table with Named Parameters that will be ordered
    and passed to the SetAction_OrderedParameters to set the Action

    As part of this function the table can be linked and will be updated
--------------------------------------------------]]
function BFButtonMixin:SetAction_NamedParameters(Action, LinkActionTable)
    local Type = Action.Type;
    if (LinkActionTable) then
        self.Action = Action;
    end

    if		(Type == "spell") then              return self:SetAction_OrderedParameters(Type, Action.SpellID);
    elseif	(Type == "item") then               return self:SetAction_OrderedParameters(Type, Action.ItemID, Action.ItemName);
    elseif	(Type == "macro") then              return self:SetAction_OrderedParameters(Type, Action.MacroIndex, Action.MacroName, Action.MacroBody);
    elseif	(Type == "mount") then              return self:SetAction_OrderedParameters(Type, Action.MountID);
    elseif	(Type == "favoritemount") then      return self:SetAction_OrderedParameters(Type);
    elseif	(Type == "battlepet") then          return self:SetAction_OrderedParameters(Type, Action.BattlePetGUID);
    elseif	(Type == "favoritebattlepet") then  return self:SetAction_OrderedParameters(Type);
    elseif	(Type == "flyout") then             return self:SetAction_OrderedParameters(Type, Action.FlyoutID);
    elseif	(Type == "equipmentset") then       return self:SetAction_OrderedParameters(Type, Action.EquipmentSetName, Action.EquipmentSetID);
    elseif	(Type == "custom") then             return self:SetAction_OrderedParameters(Type, Action.CustomName, Action.CustomData);
    elseif	(Type == "empty") then              return self:SetAction_OrderedParameters(Type);
    elseif	(Type == nil) then                  return self:SetAction_OrderedParameters("empty");
    else                                        return C.ERROR_UNRECOGNISED_ACTION;
    end
end


--[[------------------------------------------------
    SetAction_OrderedParameters

    Passes through the parameters for the specific
    SetAction function
    A table is setup to provide a hash map of each function based on ActionType

    No matter what method is used to change the action of a button, this method will be called
    (including when done in the restricted environment)
--------------------------------------------------]]
function BFButtonMixin:SetAction_OrderedParameters(ActionType, ...)

    if (ActionType ~= self.Type) then
        self:Finalize();
        self.mt.__index = Packages[ActionType];
        self.ABW.UpdateTooltip = Packages[ActionType].UpdateTooltip
    end

    self:SetAction(...);

end


--[[------------------------------------------------
    Set the button action from the passed in values
    And
    Set the cursor to the action the button previously had
--------------------------------------------------]]
function BFButtonMixin:SwapActionToCursor(...)

    -- Set the cursor from the buttons current action
    Cursor.SetCursor(self:GetCursor());

    -- Set the action on the BFButton
    if (self:SetAction_CursorValues(...)) then

        -- If appropriate provide the flyout direction UI to choosing a flyout direction
        if (self.Type == "flyout" and IsModifiedClick("PICKUPACTION")) then
            FlyoutUI.AttachFlyoutUI(self);
        end

        -- Report that the action on the button has changed from a cursor interaction (this allows Button Forge Bars to update what the action for the button in the current state/tab is, if in combat then a secure path will trigger instead)
        Core.ReportEvent(self, C.EVENT_SETACTIONFROMCURSOR, self.Action);
    end

end


-- The ABWMixin holds the script handlers for the actual action button widget
local ABWMixin = {};


--[[------------------------------------------------
    Handler:	OnReceiveDrag
    * While out of combat
      And Not a Source button or fully locked
      If the cursor was holding an action attempt to swap it to the button
--------------------------------------------------]]
function ABWMixin:OnReceiveDrag()
    if (InCombatLockdown()) then
        return;
    elseif (self.BFButton.Source or self.BFButton.FullLock) then
        -- I'm undecided about doing a clear cursor, there might be fringe scenarios we don't want to touch the cursor
        -- Cursor.ClearCursor();
        return;
    end

    local Command, Data, Subvalue, SubSubvalue = Cursor.GetCursor();
    if (Command) then
        self.BFButton:SwapActionToCursor(Command, Data, Subvalue, SubSubvalue);
    end
end


--[[------------------------------------------------
    Handler:	OnDragStart
    * While out of combat
      And not locked unless PICKUPACTION is held
      Attempt to drag the action off the button
      And if the cursor was holding an action and the button is not a source button swap that action to the button
--------------------------------------------------]]
function ABWMixin:OnDragStart()
    if (InCombatLockdown()) then
        return;
    end
    local BFButton = self.BFButton;
    if (BFButton.FullLock) then
        return;
    elseif (BFButton.Locked and not IsModifiedClick("PICKUPACTION")) then
        return;
    elseif (BFButton.Source) then
        Cursor.SetCursor(BFButton:GetCursor());
        return;
    end

    BFButton:SwapActionToCursor(Cursor.GetCursor());

end


--[[------------------------------------------------
    Handler:	OnEnter

    Taint Issue: Found WoW v10.2.7 - FIXME (kind of)
        Tooltips that contain a currency component "can"
        cause taint. There is an on demand frame created for the
        the currency responsible for this.

        If ButtonForge shows a tooltip with currency first (e.g. sellable armour item)
        then the currency frame will be tainted by BF.
        If Blizz code shows a tooltip with currency first, then no issues!

    Implication:
        Tainting the currency part of a tooltip probably wont cause an actual issue
        But its a risk that something could happen where it spread since that is what taint does

    Work arounds:
        A) I've setup a ButtonForgeGameTooltip, it too will cause taint, but it wont be on
        the GameTooltip and is much less likely to spread to where it could cause a problem
            Upside: Still shows the currency when appropriate
            Downside: Zero effect taint will still occur and be logged
                      Other addons probably wont be able to inject info into tooltips shown from ButtonForge buttons
        B) Toggle isShopping flag on GameTooltip, that will stop the currency part showing/generating and so prevent taint occurring
            Upside: No taint occurs with this option
            Downside: currency will not show when the tooltip is shown for ButtonForge buttons

        C) Take one of the first two options, but only use the work around until the required currency frame has been created
            Upside: Limits the downside impact of the other options (assumes that checking 1 currency frame will be enough)

        I'm using a mix of A and C. It strikes me as the safest.
        Unforunately the best solution would be for WoW to setup the needed frame right at the beginning so it isn't ever an issue, but
        I don't know a way to trigger this happening without also tainting the frame
--------------------------------------------------]]
function ABWMixin:OnEnter()
    local BFButton = self.BFButton
    if BFButton.ShowTooltip then
        if ButtonForgeGameTooltip ~= GameTooltip and _G["GameTooltipMoneyFrame1"] then
            ButtonForgeGameTooltip:Hide()
            ButtonForgeGameTooltip = GameTooltip
        end
        GameTooltip_SetDefaultAnchor(ButtonForgeGameTooltip, self)
        BFButton:UpdateTooltip()
    end

    if (Core.InKeybindMode() and BFButton.AllowKeybindMode) then
        self.KeybindHighlightTexture:SetAlpha(1)
        if self.KeybindOnEnter then
            -- hand over to outer addon to manage keybinding
            self:KeybindOnEnter()
        end
    end
    --ABW:UpdateFlyout();
    --Core.ReportEvent(BFButton, C.EVENT_ONENTERBUTTON);
end


--[[------------------------------------------------
    Handler:	OnLeave
--------------------------------------------------]]
function ABWMixin:OnLeave()
    ButtonForgeGameTooltip:Hide()
    if (Core.InKeybindMode() and self.BFButton.AllowKeybindMode) then
        self.KeybindHighlightTexture:SetAlpha(0.4)
        if self.KeybindOnLeave then
            self:KeybindOnLeave()
        end
    end
    --ABW:UpdateFlyout();
    --Core.ReportEvent(ABW.BFButton, C.EVENT_ONLEAVEBUTTON);
end



--[[------------------------------------------------

    PreClick

    - If the cursor is holding an action and the button is in a position to swap to that action, do the following:
        - Store a copy of the action on the cursor (sometimes the action on the cursor is cleared )
        - Unset the type attribute but take a copy - this will prevent the SecureActionButton_OnClick from triggering the action currently on the button
--------------------------------------------------]]
local SwapAtPostClick;
local BackupType;
local ResetActionButtonUseKeyDown = false;
function ABWMixin:PreClick(Button, Down)

    if (InCombatLockdown() or Button == "KeyBind" or Down or self.BFButton.Source or self.BFButton.FullLock) then
        return;
    end

    local Command, Data, Subvalue, SubSubvalue = Cursor.GetCursor();
    if (Command) then
        -- We note that we might be swapping an action to the button in the post click
        -- Store the cursor in case SecureActionButtonTemplate clears in between now and PostClick
        -- Temporarily clear the ABW type, so that SABT cant trigger it
        SwapAtPostClick = true;
        Cursor.StoreCursor(Command, Data, Subvalue, SubSubvalue);
        BackupType = self:GetAttribute("type");
        self:SetAttribute("type", nil);
        self:SetAttribute("typerelease", nil);
    end
end


--[[------------------------------------------------

    PostClick

    - If Pre click set the swap at post flag then do the following:
		- Unset the swap at post flag
		- Restore the type attribute
		- Swap the action on the cursor to the button
--------------------------------------------------]]
function ABWMixin:PostClick(Button, Down)

    local BFButton = self.BFButton;
    BFButton:UpdateChecked();
    if (InCombatLockdown() or Button == "KeyBind" or Down) then
        return;
    end

    if (SwapAtPostClick) then
        SwapAtPostClick = false;
        self:SetAttribute("type", BackupType);
        self:SetAttribute("typerelease", BackupType);
        BFButton:SwapActionToCursor(Cursor.GetStoredCursor());
    end

    self:UpdateFlyout(Down);

end


--[[------------------------------------------------

    Button Clicks in WoW v10:
        Performing clicks has become very complicated in WoW v10 due to a change in the SecureActionButton_OnClick and also the new empower spells (for reference a copy of SecureActionButton_OnClick is taken below)

        Mouse clicks not working on the UP phase:
            - The issue is SecureActionButton_OnClick treats a mouse click from an addon the same as a keypress, and keypresses by default trigger an action on the DOWN phase, but mouse clicks should only trigger on the UP phase.
              (the keypress behaviour is governed by "ActionButtonUseKeyDown")

              WORKAROUND:
                - Set "pressAndHoldAction" on all BFButtons
                - Set "typerelease" to the same value as "type"
                - screen presses in the secure PreClick as appropriate
              Explanation:
                - The new Empower Spells have introduced the button attribute "pressAndHoldAction"
                    - When a retail WoW button is holding an empower spell, that attribute is set to allow the hold release casting to work
                    - This flag causes the "ActionButtonUseKeyDown" CVar to be ignored
                    - the keypress down will always activate
                    - the keypress up will always activate... however
                        - instead of triggering the "type" action, it will trigger the "typerelease" action
                        - On retail buttons "typerelease" is always set to "actionrelease" which affects both empower spells and also continuous cast
                        - But "typerelease" can actually trigger any type of action, so simply setting it to the same as "type" means the button now responds to both DOWN and UP the same (almost identically)
                        - The difference beyond the phase is that targetting attributes for spells that affect items will not be used for the "typerelease" action, probably a rarely used capability


        Empower spells (hold and release/double tap):
            - Mode of release, either HOLD and RELEASE, or double TAP, is determined by the CVar "empowerTapControls" (mouse clicks always work via double TAP)
            - HOLD AND RELEASE: They will start casting on the DOWN phase regardless of the CVar "ActionButtonUseKeyDown", and will release on the UP phase (the default behaviour)
            - TAP: They will start casting on the DOWN phase regardless of the CVar "ActionButtonUseKeyDown", a second keypress DOWN (or mouse click) will then release the spell
            - For these spells regardless of mode, the "pressAndHoldAction" attribute will be set true on the button, this flag determines how the DOWN/UP phase are handled for keypresses
            - Macros can only trigger via the TAP method... this is because the attribute "pressAndHoldAction" does not get set on the button
            - When "pressAndHoldAction" is true and the button click is an UP phase, the action type will be read from the "typerelease" attribute... Retail WoW sets this attribute to "actionrelease"
            - actionrelease will release the empower spell... provided CVar "empowerTapControls" is not set
            - Casting the spell again will also release the empower spell...
                - This method has a slight delay when compared to actionrelease
                - defined by "EmpowerTapControlsReleaseThreshold" which defaults to 300ms
                - When the threshold is set very low, e.g. 5ms then it can be released as fast as the actionrelease method

            = ButtonForge is not able to use actionrelease as that requires the spell to be in an action slot
            = Instead the hold release pattern can be simulated by casting the spell again on the up phase
            = It does have the threshold delay on release, but this is considered ignorable (and not appropriate for ButtonForge to adjust that setting)

        Spell Flyouts:
            - These have been broken as setting type="flyout" will lead to taint
            = The work around is a custom implementated Spell Flyout, the implementation is somewhat involved, but the core concept is the flyout is triggered via attribute changes on the button


        Implementation Notes:
            - screen out clicks triggered from a mouse button DOWN phase (the player should always release the mouse button for a click to occur)

            - If the spell IsPressHoldReleaseSpell(id), then set the "pressAndHoldAction" attribute
                - The secure PreClick and PostClick will need to temporarily unset that attribute if a mouse is clicking the button
                - ButtonForge cannot use the "ReleaseAction" action type, instead it can simulate the release by triggering the spell again when the keypress is released


        Continuous cast spells:
            - GetCVarBool("ActionButtonUseKeyHeldSpell") allows spells to continually cast when a keypress is held down, this is a cool retail feature added in v10
                - Unfortunately ButtonForge cannot emulate this; The continual casting is begun via the UseAction api call when the CVar has been set true and the click was triggered via KeyPress, it will stop on a "ReleaseAction"
                - ButtonForge is not able to work with UseAction since that is using action slots, which rules out using this capability
                - If a spell was continuous casting, then more or less the only way to stop it is to call ReleaseAction() for it.

--------------------------------------------------]]


--[[------------------------------------------------
    The SecureClickWrapperFrame will wrap the click for all BFButtons
    This wrapper adjusts the button clicking to function in the same manner as retail WoW.
--------------------------------------------------]]
local SecureClickWrapperFrame = CreateFrame("FRAME", nil, nil, "SecureHandlerBaseTemplate");
SecureClickWrapperFrame:SetFrameRef("spellflyout", SpellFlyout);
SecureClickWrapperFrame:SetFrameRef("buttonforgespellflyout", ButtonForge_SpellFlyout);
SecureClickWrapperFrame:Execute([[
    SpellFlyout = self:GetFrameRef("spellflyout");
    ButtonForge_SpellFlyout = self:GetFrameRef("buttonforgespellflyout");
]]);


function Core.SecureClickWrapperFrame_UpdateCVarInfo()
    if InCombatLockdown() then
        Util.RunOncePostCombatUnique(Core.SecureClickWrapperFrame_UpdateCVarInfo)
        return
    end
    SecureClickWrapperFrame:SetAttribute("CVar_empowerTapControls", GetCVarBool("empowerTapControls"));
    SecureClickWrapperFrame:SetAttribute("CVar_ActionButtonUseKeyDown", GetCVarBool("ActionButtonUseKeyDown"));
    SecureClickWrapperFrame:Execute(
        [[
            empowerTapControls = owner:GetAttribute("CVar_empowerTapControls");
            ActionButtonUseKeyDown = owner:GetAttribute("CVar_ActionButtonUseKeyDown");
        ]]);
end
Core.SecureClickWrapperFrame_UpdateCVarInfo();


--[[------------------------------------------------
    Secure PreClick

    - If the type attribute is set to "attribute" that means it has the custom flyout action
    - If the button that triggered the click is "KeyBind" then it was a keypress, this will now be translated to a "LeftButton" and proceed
    - If not a keypress, cancel the click if it is on the DOWN phase
    - If it is on the UP phase and pressAndHoldAction is set, temporarily unset it so that the mouse click will operate normally
--------------------------------------------------]]
SecureClickWrapperFrame.SecurePreClick = [[
    if (SpellFlyout:IsShown()) then
        SpellFlyout:Hide();

    end
    if (self:GetAttribute("type") ~= "attribute" and ButtonForge_SpellFlyout:IsShown()) then
        ButtonForge_SpellFlyout:Hide();

    end

    if (button == "KeyBind") then

        local IsEmpowerSpell = self:GetAttribute("IsEmpowerSpell");
        if (down) then
            if (ActionButtonUseKeyDown or IsEmpowerSpell) then
                return "LeftButton";
            else
                return false;
            end
        else
            if ((not ActionButtonUseKeyDown and not IsEmpowerSpell) or (IsEmpowerSpell and not empowerTapControls)) then
                return "LeftButton";
            else
                return false;
            end
        end

    elseif (down) then

        return false;

    end

]];


--[[------------------------------------------------
    WoW v10:
        - Copied from Blizzards SecureTemplates.lua purely for reference
        - ButtonForge PreClick needs to symbiotically work with this OnClick function
--------------------------------------------------
local function SecureActionButton_OnClick(self, inputButton, down, isKeyPress, isSecureAction)
	-- Why are we adding extra arguments, 'isKeyPress' and 'isSecureAction', to an _OnClick handler?
	-- We want to prevent mouse actions from triggering press-and-hold behavior for now, but we do want to allow AddOns
	-- to function as they did before. This is a problem since there's no difference between an AddOn's key press behavior
	-- and mouse click behavior. So if we don't know where this is coming from, it's from an AddOn and should be treated as
	-- a key press not a mouse press for 'useOnKeyDown' purposes.
	local isSecureMousePress = not isKeyPress and isSecureAction;
	local pressAndHoldAction = SecureButton_GetAttribute(self, "pressAndHoldAction");
	local useOnKeyDown = not isSecureMousePress and (GetCVarBool("ActionButtonUseKeyDown") or pressAndHoldAction);
	local clickAction = (down and useOnKeyDown) or (not down and not useOnKeyDown);
	local releasePressAndHoldAction = (not down) and (pressAndHoldAction or GetCVarBool("ActionButtonUseKeyHeldSpell"));

	if clickAction then
		-- Only treat a key down action as a key press. Treating key up actions as a key press will result in the held
		-- spell being cast indefinitely since there's no release to stop it.
		local treatAsKeyPress = down and isKeyPress;
		OnActionButtonClick(self, inputButton, down, treatAsKeyPress);
		return true;
	elseif releasePressAndHoldAction then
		OnActionButtonPressAndHoldRelease(self, inputButton);
		return true;
	end

	return false;
end
]]

--[[------------------------------------------------
    Get a BFButton

    This could either be from the deallocated BFButtons
    Or a new Button will be created
--------------------------------------------------]]
function Core.GetBFButton(ActionButtonName)
    local BFButton;

    -- Try acquire from deallocated buttons first
    -- When an ActionButtonName is supplied the ABW will be named to that, and exist in global
    if (not ActionButtonName) then
        BFButton = Core.GetDeallocatedButton();
    elseif (_G[ActionButtonName]) then
        BFButton = Core.GetDeallocatedButton(ActionButtonName);
        if (not BFButton) then
            return nil, C.ERROR_BUTTON_NAME_IN_USE;
        end
    end

    -- If not aquired then create a new BFButton
    if (not BFButton) then
        ActionButtonName = ActionButtonName or Util.GetDefaultButtonFrameName();
        BFButton = Core.CreateBFButton(ActionButtonName);
    end

    SecureManagement.RegisterForPostCombatUpdate(BFButton);
    --BFButton.ABW:Show();

    Core.AllocateButton(BFButton);

    BFButton:UpdateKeybindHighlight();

    Core.ReportEvent(nil, C.EVENT_CREATEBUTTON, BFButton);
    return BFButton;
end


--[[------------------------------------------------
    Create Button
--------------------------------------------------]]
function Core.CreateBFButton(ActionButtonName)

    -- New BFButton
    local BFButton = {};
    BFButton.mt = {};
    setmetatable(BFButton, BFButton.mt);
    Mixin(BFButton, BFButtonMixin);

    -- Init with the Empty action type
    BFButton.mt.__index = PackageEmpty;

    -- Create the Action Button Widget (ABW) and set generic properties for how it will behave
    local ABW = CreateFrame("CheckButton", ActionButtonName, nil, "SecureActionButtonTemplate, SecureHandlerBaseTemplate, ActionButtonTemplate");
    Mixin(ABW, ABWMixin);
    ABW:SetAttribute("checkselfcast", true);
    ABW:SetAttribute("checkfocuscast", true);
    ABW:SetAttribute("checkmouseovercast", true);

    -- A hack to work around "ActionButtonUseKeyDown" CVar issues with mouse clicks
    ABW:SetAttribute("pressAndHoldAction", true);

    -- This is an old hack to allow flyouts to work (they used to require an actionslot, even if its not a valid one)
    ABW.action = 10000;

    -- Left or Right mouse buttons can drag actions on/off the button
    ABW:RegisterForDrag("LeftButton", "RightButton");

    -- Handlers to manage swapping actions in/out
    ABW:SetScript("OnReceiveDrag"	, ABW.OnReceiveDrag);
    ABW:SetScript("OnDragStart"		, ABW.OnDragStart);
    ABW:SetScript("PostClick"		, ABW.PostClick);
    ABW:SetScript("PreClick"		, ABW.PreClick);

    -- Open the possibility for all valid click actions to trigger the button on both the DOWN and UP phase, note the PreClick handler will ensure only valid clicks go through
    ABW:RegisterForClicks("AnyDown", "AnyUp");
    SecureClickWrapperFrame:WrapScript(ABW, "OnClick", SecureClickWrapperFrame.SecurePreClick);

    -- Handlers for tooltips
    ABW:SetScript("OnEnter", ABW.OnEnter);
    ABW:SetScript("OnLeave", ABW.OnLeave);

    -- Add a Keybind highlight texture to indicate quick binding for the buttons when in keybind mode
    local KeybindHighlightTexture = ABW:CreateTexture(nil, "OVERLAY");
    KeybindHighlightTexture:SetAtlas("UI-HUD-ActionBar-IconFrame-Mouseover");
    KeybindHighlightTexture:SetBlendMode("ADD");
    KeybindHighlightTexture:SetAlpha(0.4);
    KeybindHighlightTexture:SetPoint("TOPLEFT");
    KeybindHighlightTexture:SetSize(46, 45);
    KeybindHighlightTexture:Hide();
    ABW.KeybindHighlightTexture = KeybindHighlightTexture;

    -- Set bidirectional references
    ABW.BFButton = BFButton;
    BFButton.ABW = ABW;

    -- Setup references to ABW subcomponents
    BFButton.ABWIcon				= ABW.icon;
    BFButton.ABWFlash				= ABW.Flash;
    BFButton.ABWFlyoutBorderShadow	= ABW.FlyoutBorderShadow;
    BFButton.ABWFlyoutArrow			= ABW.FlyoutArrow;
    BFButton.ABWHotKey				= ABW.HotKey;
    BFButton.ABWCount				= ABW.Count;
    BFButton.ABWName				= ABW.Name;
    BFButton.ABWBorder				= ABW.Border;
    BFButton.ABWNewActionTexture	= ABW.NewActionTexture;
    BFButton.ABWCooldown			= ABW.cooldown;
    BFButton.ABWNormalTexture		= ABW.NormalTexture;
    BFButton.ABWSpellHighlightTexture   = ABW.SpellHighlightTexture;
    BFButton.ABWAutoCastable        = ABW.AutoCastable;
    BFButton.ABWLevelLinkLockIcon   = ABW.LevelLinkLockIcon;
    BFButton.ABWSpellHighlightAnim  = ABW.SpellHighlightAnim;
    BFButton.ABWFlyoutArrowContainer    = ABW.FlyoutArrowContainer;

    -- Callbacks for various events on the Button (Candidate to remove??)
    BFButton.Callbacks				= {};

    -- Table to record the action assigned to the button
    BFButton.Action 				= {};

    -- initialise elements of the button
    BFButton:SetCooldownType(COOLDOWN_TYPE_NORMAL);
    BFButton:UpdateHotKey();

    -- Add the secure code paths for the new button to allow changing the button action during combat
    SecureActionSwap.SetupButton(BFButton.ABW);

    -- Add the flyout code paths for the new button to allow the custom flyout to work (including during combat)
    PackageFlyout.SetupButton(BFButton.ABW);

    return BFButton;

end


--[[------------------------------------------------
    RemoveBFButton
--------------------------------------------------]]
function Core.RemoveBFButton(BFButton)

    --Core.ResetBFButton(BFButton);
    Core.ReportEvent(BFButton, C.EVENT_REMOVEBUTTON, BFButton.Action);
    Util.ClearTable(BFButton.Callbacks);
    SecureActionSwap.UnsetSecureListener(BFButton.ABW);
    SecureManagement.DeregisterForPostCombatUpdate(BFButton);
    BFButton:SetKeybind(nil)
    BFButton.ABW:Hide();
    Core.DeallocateButton(BFButton);

end


--[[------------------------------------------------
    ReleaseTempButtonResources
    Notes:
        * Unsecure due to SpellFlyout Hide
        * intended to be called out of combat for enabled
            or for changing the button action
--------------------------------------------------]]
function Core.ReleaseTempButtonResources(BFButton)
    -- Cooldown resources
    Core.ClearChargeCooldown(BFButton.ABW);	--Blizzard Function
    BFButton.ABWCooldown:Clear();

    -- Glow
    ActionButton_HideOverlayGlow(BFButton.ABW);	-- Blizzard Function

    -- Flash
    Core.UpdateButtonFlashRegistration(BFButton);

    -- Range
    Core.UpdateButtonRangeRegistration(BFButton)

    -- Flyout
    if (SpellFlyout:GetParent() == BFButton.ABW and not InCombatLockdown()) then
        SpellFlyout:Hide();
    end

    -- Tooltip
    if Util.GetMouseFocus() == BFButton.ABW then
        ButtonForgeGameTooltip:Hide();
    end
end
