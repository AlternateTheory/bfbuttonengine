--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local PackageEquipmentSet = Engine.PackageEquipmentSet;
local ScriptsEquipmentSet = Engine.ScriptsEquipmentSet;
local Scripts = Engine.Scripts;
local Util                  = Engine.Util

-- Take local copy of functions to slightly improve performance (in theory)
local C                     = Engine.Constants;
local C_EVENT_UPDATEACTION  = C.EVENT_UPDATEACTION;

local Core              = Engine.Core;
local Core_ReportEvent  = Core.ReportEvent;

local Methods                               = Engine.Methods;
local Methods_SetAction_OrderedParameters   = Methods.SetAction_OrderedParameters;

local C_EquipmentSet_GetEquipmentSetInfo    = C_EquipmentSet.GetEquipmentSetInfo;
local C_EquipmentSet_GetEquipmentSetID      = C_EquipmentSet.GetEquipmentSetID;


--local GameTooltip                   = GameTooltip;
--local GameTooltip_SetDefaultAnchor  = GameTooltip_SetDefaultAnchor;
local GetMouseFocus                 = Util.GetMouseFocus;
local InCombatLockdown              = InCombatLockdown;


PackageEquipmentSet.UpdateIcon					= Scripts.EmptyF;
PackageEquipmentSet.UpdateChecked				= Scripts.EmptyChecked;
PackageEquipmentSet.UpdateCooldown				= Scripts.EmptyF;
PackageEquipmentSet.UpdateEquipped				= Scripts.EmptyF;
PackageEquipmentSet.UpdateGlow					= Scripts.EmptyF;
PackageEquipmentSet.UpdateText					= Scripts.EmptyF;
PackageEquipmentSet.UpdateShine					= Scripts.EmptyF;
PackageEquipmentSet.UpdateUsable				= Scripts.EmptyF;
PackageEquipmentSet.UpdateFlashRegistration		= Scripts.EmptyF;
PackageEquipmentSet.UpdateRangeCheckRegistration		= Scripts.EmptyF;
PackageEquipmentSet.SwapActionWithButtonAction	= Scripts.SwapActionWithButtonAction;


--[[------------------------------------------------

    - Set Equipment Set
    - Capture the base spell and set the Action to that

    - While not in combat the Buttons onclick action will be set
        - values to use in the restricted environment will also be set

    - This function can be called while in combat if the action was swapped using the SecureActionSwap
        - The SecureActionSwap will handle setting the onclick and restricted environment up

--------------------------------------------------]]
function PackageEquipmentSet.SetAction(BFButton, SetName, EquipmentSetID)

    local Action        = BFButton.Action;

    if (not EquipmentSetID) then
        EquipmentSetID = C_EquipmentSet.GetEquipmentSetID(SetName);
    end

    local EquipmentSetName, IconTexture = C_EquipmentSet.GetEquipmentSetInfo(EquipmentSetID);

    BFButton.Type               = "equipmentset";
    BFButton.EquipmentSetName   = EquipmentSetName;
    BFButton.EquipmentSetID     = EquipmentSetID;

    local Action            = BFButton.Action;
    Action.Type             = "equipmentset";
    Action.EquipmentSetName = EquipmentSetName;
    Action.EquipmentSetID   = EquipmentSetID;

    if (not InCombatLockdown()) then
        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();
    end

    BFButton.ABWIcon:SetTexture(IconTexture);
    BFButton.ABWName:SetText(EquipmentSetName);
    BFButton:FullUpdate();

end


--[[------------------------------------------------
    UpdateAction
--------------------------------------------------]]
function PackageEquipmentSet.UpdateAction(BFButton, Type)

    if (Type and Type ~= "equipmentset") then
        return;
    end

    local EquipmentSetName, IconTexture = C_EquipmentSet.GetEquipmentSetInfo(BFButton.EquipmentSetID);
    if (EquipmentSetName == nil) then
        -- This equip set is gone so clear it from the button
        return BFButton:SetAction_OrderedParameters("empty");
    end

    BFButton.ABWIcon:SetTexture(IconTexture);
    if (BFButton.EquipmentSetName ~= EquipmentSetName) then

        BFButton.ABWName:SetText(EquipmentSetName);

        local Action = BFButton.Action;
        BFButton.EquipmentSetName   = EquipmentSetName;
        Action.EquipmentSetName     = EquipmentSetName;

        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();

        Core.ReportEvent(BFButton, C.EVENT_UPDATEACTION, Action);

    end

end


--[[------------------------------------------------

    Set the on click action that will be triggered

--------------------------------------------------]]
function PackageEquipmentSet.UpdateAttributes(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("type", "equipmentset");
    ABW:SetAttribute("typerelease", "equipmentset");
    ABW:SetAttribute("equipmentset", BFButton.EquipmentSetName);
end


--[[------------------------------------------------

    Get the attributes that are used for the on click action

--------------------------------------------------]]
function PackageEquipmentSet.Get_Attributes(Action)
    -- return   TypeValue , ActionAttribute , ActionValue
    return      "equipmentset", "equipmentset", Action.EquipmentSetName;
end


--[[------------------------------------------------

    Set parameters to describe the action in the restricted environment of the button
    This will support secure changing of the action

--------------------------------------------------]]
function PackageEquipmentSet.UpdateSecureCursor(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("ActionType", "equipmentset");
    ABW:SetAttribute("ActionParameter1", BFButton.EquipmentSetName);
    ABW:SetAttribute("ActionParameter2", BFButton.EquipmentSetID);
    ABW:SetAttribute("ActionParameter3", nil);
end


--[[------------------------------------------------

    Finalize will reset certain elements of the button that might not be
    affected by the new action (rather than a blanket reset, this is intended to be more performant)

--------------------------------------------------]]
function PackageEquipmentSet.Finalize(BFButton)

    local Action = BFButton.Action;
    Action.EquipmentSetName = nil;
    Action.EquipmentSetID = nil;

    local ABW = BFButton.ABW;

    -- Icon
    BFButton.ABWIcon:SetTexture(nil);

    -- Text
    BFButton.ABWName:SetText(nil);

end


--[[------------------------------------------------
    Full Update
--------------------------------------------------]]
function PackageEquipmentSet.FullUpdate(BFButton)

    if (GetMouseFocus() == BFButton.ABW) then
        BFButton:UpdateTooltip();
    end
end


--[[------------------------------------------------
    Tooltip
--------------------------------------------------]]
function PackageEquipmentSet.UpdateTooltip(Obj)
    local ABW = Obj.ABW or Obj;
    ButtonForgeGameTooltip:SetEquipmentSet(ABW.BFButton.EquipmentSetID);
end


--[[------------------------------------------------
    Cursor
--------------------------------------------------]]
function PackageEquipmentSet.GetCursor(BFButton)
    return "equipmentset", BFButton.EquipmentSetID, nil, nil;
end


--[[------------------------------------------------
    Get Ordered Parameters
--------------------------------------------------]]
function PackageEquipmentSet.Get_OrderedParameters(Action)
    -- ActionType , ActionParameter1 , ActionParameter2 , ActionParameter3
    return "equipmentset", Action.EquipmentSetName, Action.EquipmentSetID;
end
