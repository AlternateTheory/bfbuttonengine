--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local API = Engine.API_V2;

local C = Engine.Constants;
local S = Engine.Settings;
local Core = Engine.Core;
local Events = Engine.Events;
local Cursor = Engine.Cursor;
local Methods = Engine.Methods;
local SecureActionSwap = Engine.SecureActionSwap;
local Util = Engine.Util;
local FlyoutUI = Engine.FlyoutUI;
local CustomActionRegistry = Engine.CustomActionRegistry;

local InCombatLockdown = InCombatLockdown;


--[[------------------------------------------------
    CreateButton
    ActionButtonName:	The requested name for the frame, nil means an auto generated name will be used
        Mainly the only reason to specify a specific frame name is when there is a setting that is reliant
        on that exact name; e.g. properly WoW managed KeyBindings need a fixed frame name
        
        Important to note, if necessary the ActionButtonName will be forcibly obtained (maining if the global key
            is already allocated this will overwrite it (it really is best to allow an auto generated name
            as those names test the key for existance first)
    
    Action: This table defines what the action for the Button should be set to
    
    LinkActionTable: True/False, if true the Action table will be linked to the button so that when other operations
                    occur the action table will be updated with those actions (to make it easy to save out state to
                    the main addon)
                    Note only one action store will ever be remembered for the button
        
    Returns ActionButton Widget (that is a BF Action Button)
    Tests
    - ActionButtonName = null, inactive buttons, (repeat x 3 so that none are left)
    - ActionButtonName = null, no inactive buttons	(test on fresh, and after test 1 leaves none)
    - ActionButtonName = non button global
    - ActionButtonName = active button
    - ActionButtonName = inactive button
    - ActionButtonName = unused
    
--------------------------------------------------]]
function API.CreateButton(ActionButtonName, Action, LinkActionTable)
    if (InCombatLockdown()) then
        return nil, C.ERROR_IN_COMBAT_LOCKDOWN;
    end
    local BFButton = Core.GetBFButton(ActionButtonName);
    BFButton:SetAction_NamedParameters(Action, LinkActionTable);
    BFButton:SetFlyoutDirection(S.ButtonDefaults.FlyoutDirection);
    BFButton:SetKeybind(S.ButtonDefaults.Keybind);
    BFButton:SetKeyBindText(S.ButtonDefaults.KeyBindText);
    BFButton:SetEnabled(S.ButtonDefaults.Enabled);
    BFButton:SetRespondToMouse(S.ButtonDefaults.RespondToMouse);
    BFButton:SetAlpha(S.ButtonDefaults.Alpha);
    BFButton:SetLocked(S.ButtonDefaults.Locked);
    BFButton:SetFullLock(S.ButtonDefaults.FullLock);
    BFButton:SetSource(S.ButtonDefaults.Source);
    BFButton:SetAlwaysShowGrid(S.ButtonDefaults.AlwaysShowGrid);
    BFButton:SetShowTooltip(S.ButtonDefaults.ShowTooltip);
    BFButton:SetShowKeyBindText(S.ButtonDefaults.ShowKeyBindText);
    BFButton:SetShowCounts(S.ButtonDefaults.ShowCounts);
    BFButton:SetShowMacroName(S.ButtonDefaults.ShowMacroName);
    BFButton:SetAllowKeybindMode(S.ButtonDefaults.AllowKeybindMode);

    return BFButton.ABW;
end



function API.GetAndReserveDeallocatedButtonName()
    return Core.GetAndReserveDeallocatedButtonName()
end



--[[------------------------------------------------
    RemoveButton
    Tests
    - BFButton is null
    - BFButton is not a Button
    - BFButton is an Active Button
    - BFButton is an Inactive Button
--------------------------------------------------]]
function API.RemoveButton(ActionButton)
    if (InCombatLockdown()) then
        return C.ERROR_IN_COMBAT_LOCKDOWN;
    end
    
    return Core.RemoveBFButton(ActionButton.BFButton);
end

--[[------------------------------------------------
    ApplySettings
--------------------------------------------------
function API.ApplySettings(BFButton, ButtonSettings)
    if (InCombatLockdown()) then
        return C.ERROR_IN_COMBAT_LOCKDOWN;
    end
    
    Methods.SetAction(BFButton, ButtonSettings.Action);
    Methods.SetKeyBindText(BFButton, ButtonSettings.KeyBindText);
end]]


--[[------------------------------------------------
    SetAction
--------------------------------------------------]]
function API.SetAction(ActionButton, Action, LinkActionTable)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_NamedParameters(Action, LinkActionTable);
end

function API.SetActionEmpty(ActionButton)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_OrderedParameters("empty");
end


--[[------------------------------------------------
    Set Spell
--------------------------------------------------]]
function API.SetActionSpell(ActionButton, SpellID)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_OrderedParameters("spell", SpellID);
end


--[[------------------------------------------------
    Set Item
--------------------------------------------------]]
function API.SetActionItem(ActionButton, ItemID)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_OrderedParameters("item", ItemID);
end


--[[------------------------------------------------
    Set Macro
--------------------------------------------------]]
function API.SetActionMacro(ActionButton, MacroIndex, MacroName, MacroBody)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_OrderedParameters("macro", MacroIndex, MacroName, MacroBody);
end


--[[------------------------------------------------
    Set Mount
--------------------------------------------------]]
function API.SetActionMount(ActionButton, MountID)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_OrderedParameters("mount", MountID);
end


--[[------------------------------------------------
    Set Mount Favorite
--------------------------------------------------]]
function API.SetActionMountFavorite(ActionButton)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_OrderedParameters("favoritemount");
end


--[[------------------------------------------------
    Set Battle Pet
--------------------------------------------------]]
function API.SetActionBattlePet(ActionButton, GUID)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_OrderedParameters("battlepet", GUID);
end


--[[------------------------------------------------
    Set Battle Pet Favorite
--------------------------------------------------]]
function API.SetActionBattlePetFavorite(ActionButton)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_OrderedParameters("favoritebattlepet");
end


--[[------------------------------------------------
    Set Flyout
--------------------------------------------------]]
function API.SetActionFlyout(ActionButton, FlyoutID)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_OrderedParameters("flyout", FlyoutID);
end


--[[------------------------------------------------
    Set EquipmentSet
--------------------------------------------------]]
function API.SetActionEquipmentSet(ActionButton, EquipmentSetName)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_OrderedParameters("equipmentset", EquipmentSetName);
end


--[[------------------------------------------------
    Set Custom Action
--------------------------------------------------]]
function API.SetActionCustom(ActionButton, CustomName, CustomData)
    return InCombatLockdown() and C.ERROR_IN_COMBAT_LOCKDOWN or ActionButton.BFButton:SetAction_OrderedParameters("custom", CustomName, CustomData);
end


--[[------------------------------------------------
    Set Flyout Direction
--------------------------------------------------]]
function API.SetFlyoutDirection(ActionButton, Direction)
    return Util.NoCombatAndValidButtonTest(ActionButton) or ActionButton.BFButton:SetFlyoutDirection(Direction);
end


--[[------------------------------------------------
    MouseOverFlyoutDirectionUI
        Enable or Disable for the Button
--------------------------------------------------]]
function API.MouseOverFlyoutDirectionUI(ActionButton, Enable)
    if (Enable) then
        return FlyoutUI.AttachFlyoutUI(ActionButton.BFButton);
    else
        return FlyoutUI.DetachFlyoutUI();
    end
end


--[[------------------------------------------------
    SetKeybind
        Set the keybinding to trigger this button
        Or clear the binding
--------------------------------------------------]]
function API.SetKeybind(ActionButton, Keybind)
    if (not ActionButton.BFButton) then
        return C.ERROR_NOT_BUTTONFORGE_ACTIONBUTTON;
    end
    return ActionButton.BFButton:SetKeybind(Keybind);
end


--[[------------------------------------------------
    SetKeyBindText
        Set the keybinding to trigger this button
        Or clear the binding
--------------------------------------------------]]
function API.SetKeyBindText(ActionButton, KeyBindText)
    if (not ActionButton.BFButton) then
        return C.ERROR_NOT_BUTTONFORGE_ACTIONBUTTON;
    end
    return ActionButton.BFButton:SetKeyBindText(KeyBindText);
end

--[[------------------------------------------------
    SetAlwaysShowGrid
        True if the button border should show even when no action
        is attached
        Border will force show in certain situations (e.g. when cursor has an action)
--------------------------------------------------]]
function API.SetAlwaysShowGrid(ActionButton, AlwaysShowGrid)
    return Util.NoCombatAndValidButtonTest(ActionButton) or ActionButton.BFButton:SetAlwaysShowGrid(AlwaysShowGrid);
end

--[[------------------------------------------------
    SetLocked
        Action can not be inadvertently dragged off the button
        - action swap can still happen
        - action can be dragged off if shift is held down
--------------------------------------------------]]
function API.SetLocked(ActionButton, Locked)
    return ActionButton.BFButton:SetLocked(Locked);
end

--[[------------------------------------------------
    SetFullLock
        Action can not be dragged off the button at all
        nor can it be swapped off
--------------------------------------------------]]
function API.SetFullLock(ActionButton, FullLock)
    return ActionButton.BFButton:SetFullLock(FullLock);
end

--[[------------------------------------------------
    SetSource
        Same as FullLock, except the cursor will set itself to the button
        action if a drag happens (the button retains the action)
--------------------------------------------------]]
function API.SetSource(ActionButton, Source)
    return ActionButton.BFButton:SetSource(Source);
end

--[[------------------------------------------------
    SetShowTooltip
--------------------------------------------------]]
function API.SetShowTooltip(ActionButton, ShowTooltip)
    return ActionButton.BFButton:SetShowTooltip(ShowTooltip);
end

--[[------------------------------------------------
    SetShowMacroName
--------------------------------------------------]]
function API.SetShowMacroName(ActionButton, ShowMacroName)
    return ActionButton.BFButton:SetShowMacroName(ShowMacroName);
end

function API.SetShowKeybindText(ActionButton, Show)
    return ActionButton.BFButton:SetShowKeyBindText(Show);
end

--[[------------------------------------------------
    SetEnabled
        If disabled, button is not visible and cannot be triggered or otherwise interacted with
--------------------------------------------------]]
function API.SetEnabled(ActionButton, Enabled)
    return Util.NoCombatAndValidButtonTest(ActionButton) or ActionButton.BFButton:SetEnabled(Enabled);
end


function API.SetAllowKeybindMode(ActionButton, Value)
    return ActionButton.BFButton:SetAllowKeybindMode(Value);
end

--[[------------------------------------------------
    SetRespondToMouse
--------------------------------------------------]]
function API.SetRespondToMouse(ActionButton, RespondToMouse)
    return ActionButton.BFButton:SetRespondToMouse(RespondToMouse);
end

--[[------------------------------------------------
    SetAlpha
--------------------------------------------------]]
function API.SetAlpha(ActionButton, Alpha)
    return ActionButton.BFButton:SetAlpha(Alpha);
end

--[[------------------------------------------------
    Trigger...
        Mainly for use with custom actions
--------------------------------------------------]]
function API.TriggerUpdateChecked()
    return Events.TriggerUpdateChecked();
end
function API.TriggerUpdateUsable()
    return Events.TriggerUpdateUsable();
end
function API.TriggerUpdateCooldown()
    return Events.TriggerUpdateCooldown();
end
function API.TriggerUpdateEquipped()
    return Events.TriggerUpdateEquipped();
end
function API.TriggerUpdateText()
    return Events.TriggerUpdateText();
end
function API.TriggerUpdateGlow()
    return Events.TriggerUpdateGlow();
end
function API.TriggerRangeRegistration()
    return Events.TriggerRangeRegistration();
end
function API.TriggerFullUpdate()
    return Events.TriggerFullUpdate();
end


function API.SetKeybindMode(Value)
    Core.SetKeybindMode(Value);
end


--[[------------------------------------------------
    RegisterForChanges
--------------------------------------------------]]
function API.RegisterForEvents(ActionButton, CallbackFunction, Object)
    Core.RegisterForEvents(ActionButton, CallbackFunction, Object);
end

function API.SetSecureListener(ActionButton, SecureListenerFrame, CallbackAttribute)
    SecureActionSwap.SetSecureListener(ActionButton, SecureListenerFrame, CallbackAttribute);
end

function API.UnsetSecureListener(ActionButton)
    SecureActionSwap.UnsetSecureListener(ActionButton);
end


--[[------------------------------------------------
    DeregisterForChanges
--------------------------------------------------]]
function API.DeregisterForEvents(ActionButton, CallbackFunction)
    Core.DeregisterForEvents(ActionButton, CallbackFunction);
end

function API.RegisterForCursorChanges(CallbackFunction)
    Core.RegisterForCursorChanges(CallbackFunction);
end

function API.GetCursorInfo()
    return Cursor.GetCursor();
end

--[[------------------------------------------------
    LookupSpellID
    * Name of the Spell, Talent, or Specialization Spell
    * [Specialization] (optional)
--------------------------------------------------]]
function API.LookupSpellID(Name, Specialization)
    -- FIXME this is somewhat redundant so maybe remove, or add back in the api call
    local SpellID = Util.LookupSpecializationSpellID(Name, Specialization) or Util.LookupTalentSpellID(Name, Specialization) --or select(7, GetSpellInfo(Name));
    return SpellID;
end


function API.RegisterCustomAction(Name, UpdateFunctions)
    CustomActionRegistry.RegisterCustomAction(Name, UpdateFunctions);
end


function API.Get_Attributes(ActionButton)
    return ActionButton.BFButton.Get_Attributes(ActionButton.BFButton.Action);
end


function API.Get_AttributesForAction(Action)
    return Core.Get_AttributesForAction(Action);
end


function API.Get_OrderedParameters(ActionButton)
    return ActionButton.BFButton.Get_OrderedParameters(ActionButton.BFButton.Action);
end


function API.Get_OrderedParametersForAction(Action)
    return Core.Get_OrderedParametersForAction(Action);
end


function API.Get_AllowKeybindMode(ActionButton)
    return ActionButton.BFButton.AllowKeybindMode;
end
