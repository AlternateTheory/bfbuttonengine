--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local PackageFlyout = Engine.PackageFlyout;
local FlyoutUI = Engine.FlyoutUI;
local Scripts = Engine.Scripts;
local Events                = Engine.Events;


local Util                          = Engine.Util;
local GetFlyoutTexture = Util.GetFlyoutTexture;


local Core              = Engine.Core;
local Core_UpdateFlyout = Core.UpdateFlyout;

--local GameTooltip                   = GameTooltip;
--local GameTooltip_SetDefaultAnchor  = GameTooltip_SetDefaultAnchor;
local GetFlyoutInfo                 = GetFlyoutInfo;
local GetMouseFocus                 = Util.GetMouseFocus;
local InCombatLockdown              = InCombatLockdown;


PackageFlyout.UpdateAction					= Scripts.EmptyF;
PackageFlyout.UpdateIcon					= Scripts.EmptyF;
PackageFlyout.UpdateChecked					= Scripts.EmptyChecked;
PackageFlyout.UpdateUsable					= Scripts.EmptyF;
PackageFlyout.UpdateCooldown				= Scripts.EmptyF;
PackageFlyout.UpdateEquipped				= Scripts.EmptyF;
PackageFlyout.UpdateText					= Scripts.EmptyF;
PackageFlyout.UpdateGlow					= Scripts.EmptyF;
PackageFlyout.UpdateShine					= Scripts.EmptyF;
PackageFlyout.UpdateFlashRegistration		= Scripts.EmptyF;
PackageFlyout.UpdateRangeCheckRegistration		= Scripts.EmptyF;
PackageFlyout.CheckRange					= Scripts.EmptyF;
PackageFlyout.SwapActionWithButtonAction	= Scripts.SwapActionWithButtonAction;


local function UpdateFlyout(self, isButtonDownOverride)

    if (self.BFButton ~= FlyoutUI.GetAttachedBFButton()) then
        FlyoutUI.DetachFlyoutUI();
    end

    -- not flyout flag
	if (self.BFButton.Type ~= "flyout") then
		self.FlyoutBorderShadow:Hide();
		self.FlyoutArrowContainer:Hide();
		return;
	end

	-- Update border
	local isMouseOverButton =  GetMouseFocus() == self;
	local isFlyoutShown = ButtonForge_SpellFlyout:IsShown() and ButtonForge_SpellFlyout:GetParent() == self;
	if (isFlyoutShown or isMouseOverButton) then
		self.FlyoutBorderShadow:Show();
	else
		self.FlyoutBorderShadow:Hide();
	end

	-- Update arrow
	local isButtonDown;
	if (isButtonDownOverride ~= nil) then
		isButtonDown = isButtonDownOverride;
	else
		isButtonDown = self:GetButtonState() == "PUSHED";
	end

	local flyoutArrowTexture = self.FlyoutArrowContainer.FlyoutArrowNormal;

	if (isButtonDown) then
		flyoutArrowTexture = self.FlyoutArrowContainer.FlyoutArrowPushed;

		self.FlyoutArrowContainer.FlyoutArrowNormal:Hide();
		self.FlyoutArrowContainer.FlyoutArrowHighlight:Hide();
	elseif (isMouseOverButton) then
		flyoutArrowTexture = self.FlyoutArrowContainer.FlyoutArrowHighlight;

		self.FlyoutArrowContainer.FlyoutArrowNormal:Hide();
		self.FlyoutArrowContainer.FlyoutArrowPushed:Hide();
	else
		self.FlyoutArrowContainer.FlyoutArrowHighlight:Hide();
		self.FlyoutArrowContainer.FlyoutArrowPushed:Hide();
	end

	self.FlyoutArrowContainer:Show();
	flyoutArrowTexture:Show();
	flyoutArrowTexture:ClearAllPoints();

	local arrowDirection = self:GetAttribute("flyoutDirection");
	local arrowDistance = isFlyoutShown and 1 or 4;

	if (arrowDirection == "LEFT") then
		SetClampedTextureRotation(flyoutArrowTexture, isFlyoutShown and 90 or 270);
		flyoutArrowTexture:SetPoint("LEFT", self, "LEFT", -arrowDistance, 0);
	elseif (arrowDirection == "RIGHT") then
		SetClampedTextureRotation(flyoutArrowTexture, isFlyoutShown and 270 or 90);
		flyoutArrowTexture:SetPoint("RIGHT", self, "RIGHT", arrowDistance, 0);
	elseif (arrowDirection == "DOWN") then
		SetClampedTextureRotation(flyoutArrowTexture, isFlyoutShown and 0 or 180);
		flyoutArrowTexture:SetPoint("BOTTOM", self, "BOTTOM", 0, -arrowDistance);
	else
		SetClampedTextureRotation(flyoutArrowTexture, isFlyoutShown and 180 or 0);
		flyoutArrowTexture:SetPoint("TOP", self, "TOP", 0, arrowDistance);
	end

end


function PackageFlyout.SetupButton(ABW)

    ABW.UpdateFlyout = UpdateFlyout;

    -- These attributes will be used if the BFButton has a flyout action type
    ABW:SetAttribute("attribute-frame", ButtonForge_SpellFlyout);
    ABW:SetAttribute("attribute-name", "flyoutbuttonname");
    ABW:SetAttribute("attribute-value", ABW:GetName());

    -- Add button to restricted environment for the ButtonForge_SpellFlyout
    PackageFlyout.AddButtonToSpellFlyout(ABW);

    -- Default flyout to UP
    ABW:SetAttribute("flyoutDirection", "UP");

end


--[[------------------------------------------------

    - Set flyout

    - While not in combat the Buttons onclick action will be set
        - values to use in the restricted environment will also be set

    - This function can be called while in combat if the action was swapped using the SecureActionSwap
        - The SecureActionSwap will handle setting the onclick and restricted environment up

--------------------------------------------------]]
function PackageFlyout.SetAction(BFButton, FlyoutID)

    local FlyoutName, FlyoutDescription = GetFlyoutInfo(FlyoutID);
    local Icon = GetFlyoutTexture(FlyoutID);

    BFButton.Type               = "flyout";
    BFButton.FlyoutID           = FlyoutID;
    BFButton.FlyoutName         = FlyoutName or Action.FlyoutName;
    BFButton.FlyoutDescription  = FlyoutDescription;

    local Action        = BFButton.Action;
    Action.Type         = "flyout";
    Action.FlyoutID     = FlyoutID;
    Action.FlyoutName   = FlyoutName or Action.FlyoutName;

    if (not InCombatLockdown()) then
        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();
    end

    if not Icon and not BFButton.RegisteredForCallback then
        Events.RegisterEventCallback("SPELL_FLYOUT_UPDATE", PackageFlyout.CallbackFlyoutAvailable, BFButton)
        BFButton.RegisteredForCallback = true
    end
    BFButton.ABWIcon:SetTexture(Icon);
    BFButton:FullUpdate();

end


function PackageFlyout.CallbackFlyoutAvailable(BFButton)
    BFButton:UpdateAction("flyout")
end


function PackageFlyout.UpdateAction(BFButton, Type)
    if Type ~= "flyout" then
        return
    end
    local FlyoutName, FlyoutDescription = GetFlyoutInfo(BFButton.FlyoutID);
    local Icon = GetFlyoutTexture(BFButton.FlyoutID);
    if Icon then
        BFButton.FlyoutName         = FlyoutName
        BFButton.FlyoutDescription  = FlyoutDescription
        BFButton.Action.FlyoutName  = FlyoutName
        BFButton.ABWIcon:SetTexture(Icon)
        BFButton:FullUpdate()
        Events.UnregisterEventCallback("SPELL_FLYOUT_UPDATE", PackageFlyout.CallbackFlyoutAvailable, BFButton)
        BFButton.RegisteredForCallback = nil
    end
end


--[[------------------------------------------------

    Set the on click action that will be triggered

--------------------------------------------------]]
function PackageFlyout.UpdateAttributes(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("type", "attribute");
    ABW:SetAttribute("typerelease", "attribute");
    ABW:SetAttribute("spell", BFButton.FlyoutID);
end


--[[------------------------------------------------

    Get the attributes that are used for the on click action
    This function can be called directly to allow the on click attributes to be
    fed into the restricted environment of BF Bars

--------------------------------------------------]]
function PackageFlyout.Get_Attributes(Action)
    -- return   TypeValue , ActionAttribute , ActionValue
    return      "attribute", "spell", Action.FlyoutID;
end


--[[------------------------------------------------

    Set parameters to describe the action in the restricted environment of the button
    This will support secure changing of the action

--------------------------------------------------]]
function PackageFlyout.UpdateSecureCursor(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("ActionType", "flyout");
    ABW:SetAttribute("ActionParameter1", BFButton.FlyoutID);
    ABW:SetAttribute("ActionParameter2", nil);
    ABW:SetAttribute("ActionParameter3", nil);
end


--[[------------------------------------------------

    Finalize will reset certain elements of the button that might not be
    affected by the new action (rather than a blanket reset, this is intended to be more performant)

--------------------------------------------------]]
function PackageFlyout.Finalize(BFButton)

    local Action = BFButton.Action;
    Action.FlyoutID = nil;
    Action.FlyoutName = nil;

    local ABW = BFButton.ABW;

    -- Checked
    ABW:SetChecked(false);

    -- Icon
    BFButton.ABWIcon:SetTexture(nil);

    -- Disbale flyout   (override, ClearFlyout)
    BFButton.Type = "";
    ABW:UpdateFlyout();

    FlyoutUI.DetachFlyoutUI()

    if BFButton.RegisteredForCallback then
        Events.UnregisterEventCallback("SPELL_FLYOUT_UPDATE", PackageFlyout.CallbackFlyoutAvailable, BFButton)
        BFButton.RegisteredForCallback = nil
    end
end


--[[------------------------------------------------
    Full Update
--------------------------------------------------]]
function PackageFlyout.FullUpdate(BFButton)
    BFButton:UpdateChecked();
    BFButton.ABW:UpdateFlyout();
    
    if (GetMouseFocus() == BFButton.ABW) then
        BFButton:UpdateTooltip();
    end
end


--[[------------------------------------------------
    Checked
--------------------------------------------------]]
function PackageFlyout.UpdateChecked(BFButton)
    BFButton.ABW:SetChecked(false);
end


--[[------------------------------------------------
    Tooltip
--------------------------------------------------]]
function PackageFlyout.UpdateTooltip(Obj)
    local ABW = Obj.ABW or Obj;
    local BFButton = ABW.BFButton;

    if ( BFButton.FlyoutName and BFButton.FlyoutName ~= "" ) then
        ButtonForgeGameTooltip:SetText(BFButton.FlyoutName, 1, 1, 1);
        ButtonForgeGameTooltip:AddLine(BFButton.FlyoutDescription, nil, nil, nil, true);
        ButtonForgeGameTooltip:Show();
    end
end


--[[------------------------------------------------
    Cursor
--------------------------------------------------]]
function PackageFlyout.GetCursor(BFButton)
    return "flyout", BFButton.FlyoutID, nil, nil;
end


--[[------------------------------------------------
    Get Ordered Parameters
--------------------------------------------------]]
function PackageFlyout.Get_OrderedParameters(Action)
    -- ActionType , ActionParameter1 , ActionParameter2 , ActionParameter3
    return "flyout", Action.FlyoutID;
end
