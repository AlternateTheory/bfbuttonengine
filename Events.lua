--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014
    
    Desc:	Monitor and respond to events that alter the visuals for Buttons
            As a general rule updating of Buttons will route through the
            Updater.OnUpdate
]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local Events = Engine.Events;

local PackageMacro = Engine.PackageMacro;
local Core = Engine.Core;
local Cursor = Engine.Cursor;
local C = Engine.Constants;
local Util = Engine.Util;
local SecureActionSwap = Engine.SecureActionSwap;


local Checked 	= CreateFrame("FRAME");
local Usable 	= CreateFrame("FRAME");
local Cooldown 	= CreateFrame("FRAME");
local Equipped 	= CreateFrame("FRAME");
local Text 		= CreateFrame("FRAME");
local Glow 		= CreateFrame("FRAME");
local Macro 	= CreateFrame("FRAME");
local Flash		= CreateFrame("FRAME");
local Range		= CreateFrame("FRAME");
local CVar      = CreateFrame("FRAME");
local Misc 		= CreateFrame("FRAME");		--, nil, nil, "SecureHandlerStateTemplate");
Macro:SetFrameStrata("BACKGROUND");
Macro:SetFrameLevel(1);
Misc:SetFrameStrata("BACKGROUND");
Misc:SetFrameLevel(2);


--[[------------------------------------------------
    Updater
    Desc:	Primarily an OnUpdate is used to coordinate
            The updating of the Buttons
--------------------------------------------------]]
Events.Updater = CreateFrame("FRAME");
local Updater = Events.Updater;
Updater:SetFrameStrata("BACKGROUND");
Updater:SetFrameLevel(3);

function Updater:OnUpdate(Elapsed)
    if (self.DoRefresh) then
        if (self.UpdateAllMacroDisplayActions) then
            PackageMacro.UpdateAllMacroDisplayActions();
            self.UpdateAllMacroDisplayActions = false;
        end
        Core.UpdateIconAll();			-- This might be heavy handed, hopefully the cost isn't really an issue though, otherwise specific events need to be determined
        if (self.UpdateChecked) then
            Core.UpdateCheckedAll();
            self.UpdateChecked = false;
        end
        if (self.UpdateUsable) then
            Core.UpdateUsableAll();
            self.UpdateUsable = false;
        end
        if (self.UpdateCooldown) then
            Core.UpdateCooldownAll();
            self.UpdateCooldown = false;
        end
        if (self.UpdateEquipped) then
            Core.UpdateEquippedAll();
            self.UpdateEquipped = false;
        end
        if (self.UpdateText) then
            Core.UpdateTextAll();
            self.UpdateText = false;
        end
        if (self.UpdateGlow) then
            Core.UpdateGlowAll();
            self.UpdateGlow = false;
        end
        if (self.UpdateFlyout) then
            Core.UpdateFlyoutAll();
            self.UpdateFlyout = false;
        end	
        if (self.UpdateFlashRegistration) then
            Core.UpdateFlashRegistrationAll();
            self.UpdateFlash = false;
        end
        if (self.UpdateRangeCheckRegistration) then
            Core.UpdateRangeCheckRegistrationAll();
            self.UpdateRange = false;
        end
        self.DoRefresh = false;
    end
end
Updater:SetScript("OnUpdate", Updater.OnUpdate);


--[[------------------------------------------------------------------------
    Checked Events
--------------------------------------------------------------------------]]
Checked:RegisterEvent("TRADE_SKILL_SHOW");
Checked:RegisterEvent("TRADE_SKILL_CLOSE");
Checked:RegisterEvent("ARCHAEOLOGY_TOGGLE");
Checked:RegisterEvent("ARCHAEOLOGY_CLOSED");
Checked:RegisterEvent("COMPANION_UPDATE");
Checked:RegisterEvent("PET_BATTLE_PET_CHANGED");
Checked:RegisterEvent("PET_ATTACK_START");
Checked:RegisterEvent("PET_ATTACK_STOP");
Checked:RegisterEvent("PET_BAR_UPDATE");
Checked:RegisterEvent("PET_BATTLE_PET_CHANGED");
Checked:RegisterEvent("CURRENT_SPELL_CAST_CHANGED");
Checked:RegisterEvent("ACTIONBAR_UPDATE_STATE");		--I am not certain how excessive this event is yet, it may not be needed and is a canidate to remove
Checked:RegisterEvent("PLAYER_ENTER_COMBAT");
Checked:RegisterEvent("PLAYER_LEAVE_COMBAT");
Checked:RegisterEvent("START_AUTOREPEAT_SPELL");
Checked:RegisterEvent("STOP_AUTOREPEAT_SPELL");
Checked:RegisterEvent("UPDATE_OVERRIDE_ACTIONBAR");
Checked:RegisterEvent("UPDATE_VEHICLE_ACTIONBAR");
Checked:RegisterEvent("ACTIONBAR_PAGE_CHANGED");

function Checked:OnEvent()
    Updater.UpdateChecked = true;
    Updater.DoRefresh = true;
end
Checked:SetScript("OnEvent", Checked.OnEvent);
function Events.TriggerUpdateChecked()
    Updater.UpdateChecked = true;
    Updater.DoRefresh = true;
end

--[[------------------------------------------------------------------------
    Usable Events
--------------------------------------------------------------------------]]
Usable:RegisterEvent("SPELL_UPDATE_USABLE");
Usable:RegisterEvent("PLAYER_CONTROL_LOST");
Usable:RegisterEvent("PLAYER_CONTROL_GAINED");
Usable:RegisterEvent("BAG_UPDATE_DELAYED");
Usable:RegisterEvent("MINIMAP_UPDATE_ZOOM");
Usable:RegisterEvent("UPDATE_OVERRIDE_ACTIONBAR");
Usable:RegisterEvent("UPDATE_VEHICLE_ACTIONBAR");
Usable:RegisterEvent("ACTIONBAR_UPDATE_USABLE");	--Use this as a backup...
Usable:RegisterEvent("VEHICLE_UPDATE");
Usable:RegisterEvent("ACTIONBAR_PAGE_CHANGED");	
--Usable:RegisterEvent("UPDATE_WORLD_STATES");
Usable:RegisterUnitEvent("UNIT_ENTERED_VEHICLE", "player");
Usable:RegisterUnitEvent("UNIT_ENTERING_VEHICLE", "player");
Usable:RegisterUnitEvent("UNIT_EXITED_VEHICLE", "player");

function Usable:OnEvent()
    Updater.UpdateUsable = true;
    Updater.DoRefresh = true;
end
Usable:SetScript("OnEvent", Usable.OnEvent);
function Events.TriggerUpdateUsable()
    Updater.UpdateUsable = true;
    Updater.DoRefresh = true;
end

--[[------------------------------------------------------------------------
    Cooldown Events
--------------------------------------------------------------------------]]
Cooldown:RegisterEvent("SPELL_UPDATE_COOLDOWN");
Cooldown:RegisterEvent("BAG_UPDATE_COOLDOWN");
Cooldown:RegisterEvent("ACTIONBAR_UPDATE_COOLDOWN");
Cooldown:RegisterEvent("UPDATE_SHAPESHIFT_COOLDOWN");
Cooldown:RegisterEvent("LOSS_OF_CONTROL_UPDATE");
Cooldown:RegisterEvent("LOSS_OF_CONTROL_ADDED");

function Cooldown:OnEvent()
    Updater.UpdateCooldown = true;
    Updater.DoRefresh = true;
end
Cooldown:SetScript("OnEvent", Cooldown.OnEvent);
function Events.TriggerUpdateCooldown()
    Updater.UpdateCooldown = true;
    Updater.DoRefresh = true;
end

--[[------------------------------------------------------------------------
    Equipped Events
--------------------------------------------------------------------------]]
Equipped:RegisterEvent("PLAYER_EQUIPMENT_CHANGED");

function Equipped:OnEvent()
    Updater.UpdateEquipped = true;
    Updater.DoRefresh = true;
end
Equipped:SetScript("OnEvent", Equipped.OnEvent);
function Events.TriggerUpdateEquipped()
    Updater.UpdateEquipped = true;
    Updater.DoRefresh = true;
end

--[[------------------------------------------------------------------------
    Text Events
--------------------------------------------------------------------------]]
Text:RegisterEvent("BAG_UPDATE_DELAYED");
Text:RegisterEvent("SPELL_UPDATE_CHARGES");
Text:RegisterEvent("UNIT_AURA");

function Text:OnEvent()
    Updater.UpdateText = true;
    Updater.DoRefresh = true;
end
Text:SetScript("OnEvent", Text.OnEvent);
function Events.TriggerUpdateText()
    Updater.UpdateText = true;
    Updater.DoRefresh = true;
end

--[[------------------------------------------------------------------------
    Glow Events
--------------------------------------------------------------------------]]
Glow:RegisterEvent("SPELL_ACTIVATION_OVERLAY_GLOW_SHOW");
Glow:RegisterEvent("SPELL_ACTIVATION_OVERLAY_GLOW_HIDE");

function Glow:OnEvent(Event, Arg1)
    Updater.UpdateGlow = true;
    Updater.DoRefresh = true;
end
Glow:SetScript("OnEvent", Glow.OnEvent);
function Events.TriggerUpdateGlow()
    Updater.UpdateGlow = true;
    Updater.DoRefresh = true;
end

--[[------------------------------------------------------------------------
    Flash Events
--------------------------------------------------------------------------]]
Flash:RegisterEvent("PLAYER_ENTER_COMBAT");
Flash:RegisterEvent("PLAYER_LEAVE_COMBAT");
Flash:RegisterEvent("START_AUTOREPEAT_SPELL");
Flash:RegisterEvent("STOP_AUTOREPEAT_SPELL");
Flash:RegisterEvent("PET_ATTACK_START");
Flash:RegisterEvent("PET_ATTACK_STOP");

function Flash:OnEvent()
    Updater.UpdateFlashRegistration = true;
    Updater.DoRefresh = true;
end
Flash:SetScript("OnEvent", Flash.OnEvent);


--[[------------------------------------------------------------------------
    Range Events
--------------------------------------------------------------------------]]
Range:RegisterEvent("PLAYER_TARGET_CHANGED");
Range:RegisterEvent("UNIT_FACTION");

function Range:OnEvent()
    Updater.UpdateRangeCheckRegistration = true;
    Updater.DoRefresh = true;
end
Range:SetScript("OnEvent", Range.OnEvent);
function Events.TriggerRangeRegistration()
    Updater.UpdateRangeCheckRegistration = true;
    Updater.DoRefresh = true;
end


--[[------------------------------------------------------------------------
    Macro Events (Events that signal a macro conditional may have changed)
--------------------------------------------------------------------------]]
Macro:RegisterEvent("MODIFIER_STATE_CHANGED");	--mod:
Macro:RegisterEvent("PLAYER_TARGET_CHANGED");		--harm, help, etc
Macro:RegisterEvent("PLAYER_FOCUS_CHANGED");		--harm, help, etc
Macro:RegisterEvent("ACTIONBAR_PAGE_CHANGED");	--actionbar
Macro:RegisterEvent("UPDATE_OVERRIDE_ACTIONBAR");
Macro:RegisterEvent("UPDATE_VEHICLE_ACTIONBAR");
Macro:RegisterEvent("PLAYER_REGEN_ENABLED");		--nocombat
Macro:RegisterEvent("PLAYER_REGEN_DISABLED");		--combat
Macro:RegisterEvent("UNIT_SPELLCAST_CHANNEL_START");	--channel:
Macro:RegisterEvent("UNIT_SPELLCAST_CHANNEL_STOP");	--channel:
Macro:RegisterEvent("PLAYER_EQUIPMENT_CHANGED");	--equipped:
Macro:RegisterEvent("ACTIVE_TALENT_GROUP_CHANGED");	--spec:
Macro:RegisterEvent("UPDATE_SHAPESHIFT_COOLDOWN");	--stance/form:
Macro:RegisterEvent("UPDATE_SHAPESHIFT_FORM");	--stance/form:
Macro:RegisterEvent("UPDATE_STEALTH");			--stealth
Macro:RegisterEvent("UNIT_ENTERED_VEHICLE");		--vehicleui
Macro:RegisterEvent("UNIT_EXITED_VEHICLE");		--vehicleui
Macro:RegisterEvent("MINIMAP_UPDATE_ZOOM");		--indoors/outdoors
Macro:RegisterEvent("ACTIONBAR_SLOT_CHANGED");	--This event is excessive, the system is designed not to need it; although at times it may provide slightly (very slightly) faster macro refreshes

--these following conditionals and targets are via the dynamic OnUpdate (yuck!)
--flyable
--flying
--mounted
--pet
--swimming
--group??part/raid
--mouseover	(help,harm etc)
function Macro:OnUpdate()
    if (UnitName("mouseover") ~= self.MOUnit or UnitIsDead("mouseover") ~= self.MOUnitDead) then
        self.MOUnit = UnitName("mouseover");
        self.MOUnitDead = UnitIsDead("mouseover");
        Updater.UpdateAllMacroDisplayActions = true;
        Updater.DoRefresh = true;
    end
    if (IsFlying() ~= self.IsFlying) then
        self.IsFlying = IsFlying();
        Updater.UpdateAllMacroDisplayActions = true;
        Updater.DoRefresh = true;
    end
    if (IsMounted() ~= self.IsMounted) then
        self.IsMounted = IsMounted();
        Updater.UpdateAllMacroDisplayActions = true;
        Updater.DoRefresh = true;
    end
    if (IsFlyableArea() ~= self.IsFlyableArea) then
        self.IsFlyableArea = IsFlyableArea();
        Updater.UpdateAllMacroDisplayActions = true;
        Updater.DoRefresh = true;
    end
end
--Macro:SetScript("OnUpdate", Macro.OnUpdate);



function Macro:OnEvent()
    Updater.UpdateAllMacroDisplayActions = true;
    Updater.DoRefresh = true;
end
Macro:SetScript("OnEvent", Macro.OnEvent);



CVar:RegisterEvent("CVAR_UPDATE");
function CVar:OnEvent(Event, Name, Value)

end
CVar:SetScript("OnEvent", CVar.OnEvent);




--[[-------------------------------------------------------------------------
    Misc
    Desc: These are left over events that we still want to respond to, however
            they're expected less frequently and considered to not warrant an individual
            frame, in this case the event handler will inspect the event and determine
            what flags need to be set for the Updater to then process.
            OR
            In some cases the Misc Frame itself may temporarily active its Own
            OnUpdate to perform some functions - the Misc OnUpdate should fire before
            the Updater one by virtue of it's strata/level
---------------------------------------------------------------------------]]
Misc:RegisterEvent("BAG_UPDATE_DELAYED");
Misc:RegisterEvent("PLAYER_ENTERING_WORLD");
Misc:RegisterEvent("PLAYER_EQUIPMENT_CHANGED");
Misc:RegisterEvent("PET_JOURNAL_LIST_UPDATE");
Misc:RegisterEvent("PET_SPECIALIZATION_CHANGED");
Misc:RegisterEvent("ACTIVE_TALENT_GROUP_CHANGED");
Misc:RegisterEvent("PLAYER_TALENT_UPDATE");
Misc:RegisterEvent("LOCALPLAYER_PET_RENAMED");
Misc:RegisterEvent("PET_BAR_UPDATE");
Misc:RegisterEvent("ADDON_ACTION_FORBIDDEN");
Misc:RegisterEvent("UNIT_PET");
Misc:RegisterEvent("UPDATE_MACROS");
Misc:RegisterEvent("EQUIPMENT_SETS_CHANGED");
Misc:RegisterEvent("PLAYER_REGEN_ENABLED");
Misc:RegisterEvent("PLAYER_REGEN_DISABLED");
Misc:RegisterEvent("CURSOR_CHANGED");
Misc:RegisterEvent("ACTIONBAR_SHOWGRID");
Misc:RegisterEvent("ACTIONBAR_HIDEGRID");
Misc:RegisterEvent("LEARNED_SPELL_IN_TAB");
Misc:RegisterEvent("UPDATE_VEHICLE_ACTIONBAR");
Misc:RegisterEvent("UPDATE_OVERRIDE_ACTIONBAR");
Misc:RegisterEvent("UPDATE_POSSESS_BAR");
Misc:RegisterEvent("SEARCH_DB_LOADED");
Misc:RegisterEvent("PLAYER_LOGIN");
Misc:RegisterEvent("COMPANION_UPDATE");
Misc:RegisterEvent("NEW_MOUNT_ADDED");
Misc:RegisterEvent("SPELLS_CHANGED");
Misc:RegisterEvent("MODIFIER_STATE_CHANGED");
Misc:RegisterEvent("CVAR_UPDATE");
Misc:RegisterEvent("TOYS_UPDATED");
--Misc:RegisterEvent("ITEM_CHANGED");

--[[-------------------------------------------------------------------------
    Old comment??
	    * In reality this event doesn't seem to do much for things
	    * The problem I was trying to solve is at first login GetItemInfo(ID) does not return info
	    * I assuming this event would help, but it does not seem to fire even when the above call fails to retrieve info
	    * That said, one of the other events that causes an ActionUpdate appears to happen after the info we need is available so in that regard the problem appears managed for now
    
    WoW v9.0.2
        * Some players have reported having issues with items, as close as I can see it is related to this, so it may be that watching this event could help
---------------------------------------------------------------------------]]
local WatchForItemID = {};
local WatchForItemIDCount = 0;
function Events.WatchForItemInfoEvent(ItemID)
    Misc:RegisterEvent("GET_ITEM_INFO_RECEIVED");
    WatchForItemIDCount = WatchForItemIDCount + (WatchForItemID[ItemID] and 0 or 1); -- if not set add 1 to the count
    WatchForItemID[ItemID] = true;
end

function Misc:OnEvent(Event, Arg1, ...)
    if (Event == "BAG_UPDATE_DELAYED") then
        Util.CacheBagItems();
        Events.TriggerUpdateUsable();
        return;

    elseif Event == "MODIFIER_STATE_CHANGED" then
        if Cursor.HasValidAction then
            Misc.UpdateRespondToMouseAll = true;
            Misc.UpdateAlphaAll = true;
        end
    elseif (Event == "CURSOR_UPDATE") then
        --[[
            Explanation:
                Generally show grid can just rely on the ACTIONBAR_SHOWGRID/HIDEGRID
                But non action based items will not trigger those events - and they are something BFButtonEngine allows (basically a convenience to track quantities collected)
                Update the show grid when we detect an item onto the cursor
                Then update the grid again when the cursor loses the item, but at any other time do not update the show grid
        ]]
        if (GetCursorInfo() == "item" and not Misc.CursorItemFlag) then
            Misc.CursorItemFlag = true;
            Misc.UpdateGrid = true;
            Misc.UpdateRespondToMouseAll = true;
            Misc.UpdateAlphaAll = true;
        elseif (GetCursorInfo() ~= "item" and Misc.CursorItemFlag) then
            Misc.CursorItemFlag = false;
            Misc.UpdateGrid = true;
            Misc.UpdateRespondToMouseAll = true;
            Misc.UpdateAlphaAll = true;
        else
            return;
        end
    elseif (Event == "ACTIONBAR_SHOWGRID"
            or Event == "ACTIONBAR_HIDEGRID") then
        Misc.UpdateGrid = true;
        Misc.UpdateRespondToMouseAll = true;
        Misc.UpdateAlphaAll = true;
    elseif (Event == "UPDATE_VEHICLE_ACTIONBAR"
            or Event == "UPDATE_OVERRIDE_ACTIONBAR"
            or Event == "UPDATE_POSSESS_BAR") then
        Misc.FullUpdate = true;
    elseif (Event == "PLAYER_EQUIPMENT_CHANGED"
            or (Event == "PLAYER_ENTERING_WORLD" and Arg1 == true)) then
        Util.CacheInventoryItems();
        Events.TriggerUpdateUsable();
        return;

    elseif (Event == "PET_JOURNAL_LIST_UPDATE") then
        Misc.UpdateActionBattlePet = true;
        Misc.FullUpdate = true;
    elseif ((Event == "UNIT_PET" and Arg1 == "player")
            or Event == "PET_SPECIALIZATION_CHANGED"
            or Event == "LOCALPLAYER_PET_RENAMED") then
        Misc.UpdateActionAll = true;
        Misc.FullUpdate = true;
    elseif (Event == "PET_BAR_UPDATE") then
        Misc.UpdateShine = true;
    elseif (Event == "UPDATE_MACROS") then
        Misc.UpdateActionMacro = true;
        Misc.FullUpdate = true;
        -- Note the updater is theoretically setup to run after the Misc OnUpdate
        Updater.UpdateAllMacroDisplayActions = true;
        Updater.DoRefresh = true;
    elseif (Event == "PLAYER_TALENT_UPDATE"
            or Event == "ACTIVE_TALENT_GROUP_CHANGED"
            or Event == "LEARNED_SPELL_IN_TAB"
            or Event == "SPELLS_CHANGED") then
        Misc.UpdateActionSpell = true;
        Misc.FullUpdate = true;
    elseif (Event == "EQUIPMENT_SETS_CHANGED") then
        Misc.UpdateActionEquipmentSet = true;
        Misc.FullUpdate = true;
    elseif (Event == "PLAYER_REGEN_DISABLED") then
        Misc.UpdateAlphaAll = true;
    elseif (Event == "PLAYER_REGEN_ENABLED") then
        Misc.OutOfCombat = true;
        Misc.UpdateAlphaAll = true;

    elseif (Event == "GET_ITEM_INFO_RECEIVED") then
        if (WatchForItemID[Arg1 or 0] and ...) then
            Misc.UpdateActionItem = true;
            Misc.FullUpdate = true;
            WatchForItemID[Arg1 or 0] = nil;
            WatchForItemIDCount = WatchForItemIDCount - 1;
            if (WatchForItemIDCount == 0) then
                Misc:UnregisterEvent("GET_ITEM_INFO_RECEIVED");
            end
        end
    elseif (Event == "ADDON_ACTION_FORBIDDEN") then
        print(Event, Arg1, ...);
    elseif (Event == "PLAYER_LOGIN") then
        Misc.UpdateActionEquipmentSet = true;
        Misc.FullUpdate = true;
    --elseif (Event == "COMPANION_UPDATE") then
    --    Misc.UpdateActionMount = true;
    --    Misc.FullUpdate = true;
    --elseif (Event == "ITEM_CHANGED") then
    --    Core.UpdateActionItem(Arg1, ...);   -- ITEM_CHANGED provides the item hyperlink for the item that got changed, we utilise this to perform a specific update
    --    Misc.FullUpdate = true;
    elseif (Event == "TOYS_UPDATED") then
        Misc.UpdateActionItem = true
        Misc.FullUpdate = true
    elseif (Event == "NEW_MOUNT_ADDED") then
        --Misc.
    elseif (Event == "CVAR_UPDATE") then
        if (Arg1 == "empowerTapControls"
            or Arg1 == "ActionButtonUseKeyDown") then
            Misc.SecureClickWrapperFrame_UpdateCVarInfo = true;
        end
    end
    Misc:SetScript("OnUpdate", Misc.OnUpdate);
end
Misc:SetScript("OnEvent", Misc.OnEvent);


function Misc:OnUpdate()
    if self.UpdateRespondToMouseAll then
        self.UpdateRespondToMouseAll = false;
        Core.UpdateRespondToMouseAll();
    end
    if (self.OutOfCombat) then
        self.OutOfCombat = false;
        Core.LeaveCombatUpdate();
    end
    if (self.UpdateBagCache) then
        self.UpdateBagCache = false;
        Util.CacheBagItems();
    end
    if (self.UpdateInventoryCache) then
        self.UpdateInventoryCache = false;
        Util.CacheInventoryItems();
    end
    if (self.UpdateActionAll) then
        self.UpdateActionAll = false;
        Core.UpdateActionAll();
    end
    if (self.UpdateActionCustom) then
        self.UpdateActionCustom = false;
        Core.UpdateActionCustom();
    end
    if (self.UpdateActionEquipmentSet) then
        self.UpdateActionEquipmentSet = false;
        Core.UpdateActionEquipmentSet();
    end
    if (self.UpdateActionItem) then
        self.UpdateActionItem = false;
        Core.UpdateActionItem();
    end
    if (self.UpdateActionMacro) then
        self.UpdateActionMacro = false;
        Core.UpdateActionMacro();
    end
    if (self.UpdateActionMount) then
        self.UpdateActionMount = false;
        Core.UpdateActionMount();
    end
    if (self.UpdateActionPetAbility) then
        self.UpdateActionPetAbility = false;
        Core.UpdateActionPetAbility();
    end
    if (self.UpdateActionPetAbilityHunter) then
        self.UpdateActionPetAbilityHunter = false;
        Core.UpdateActionPetAbilityHunter();
    end
    if (self.UpdateActionBattlePet) then
        self.UpdateActionBattlePet = false;
        Core.UpdateActionBattlePet();
    end
    if (self.UpdateActionSpell) then
        self.UpdateActionSpell = false;
        Core.UpdateActionSpell();
        SecureActionSwap.UpdateSecureSpellMap();
    end
    if (self.UpdateShine) then
        self.UpdateShine = false;
        Core.UpdateShineAll();
    end
    if (self.UpdateGrid) then
        self.UpdateGrid = false;
        Core.UpdateButtonShowHideAll();
    end
    if (self.UpdateAlphaAll) then
        self.UpdateAlphaAll = false;
        Core.UpdateAlphaAll();
    end
    if (self.SecureClickWrapperFrame_UpdateCVarInfo) then
        self.SecureClickWrapperFrame_UpdateCVarInfo = false;
        Core.SecureClickWrapperFrame_UpdateCVarInfo();
    end
    if (self.FullUpdate) then
        self.FullUpdate = false;
        Core.FullUpdateAll();
    end
    self:SetScript("OnUpdate", nil);
end
function Events.TriggerFullUpdate()
    Misc.FullUpdate = true;
    Misc:SetScript("OnUpdate", Misc.OnUpdate);
end
function Events.TriggerUpdateGrid()
    Misc.UpdateGrid = true;
    Misc:SetScript("OnUpdate", Misc.OnUpdate);
end
function Events.TriggerUpdateRespondToMouseAll()
    Misc.UpdateRespondToMouseAll = true;
    Misc:SetScript("OnUpdate", Misc.OnUpdate);
end
function Events.TriggerUpdateAlphaAll()
    Misc.UpdateAlphaAll = true;
    Misc:SetScript("OnUpdate", Misc.OnUpdate);
end


local CallbackEventsFrame = CreateFrame("FRAME")
local CallbackEvents = {}
local RemoveAfterEvent = {}
local ProcessingEvent = nil
function CallbackEventsFrame:OnEvent(event, ...)
    ProcessingEvent = event
    for _, funcCalls in ipairs(CallbackEvents[event]) do
        local func = funcCalls[1]
        local param = funcCalls[2]
        if param == nil then
            func(...)
        else
            func(param, ...)
        end
    end
    ProcessingEvent = nil

    -- Since callbacks may try to unregister when called, the removal is defered till after all callbacks are triggered to avoid distrubing processing
    -- This also provides the opportunity to remove them in reverse order "reducing" the chance we end up with a worst case array unwind continually pulling the first element
    for i = #RemoveAfterEvent, 1, -1 do
        Events.UnregisterEventCallback(unpack(RemoveAfterEvent[i]))
    end
    wipe(RemoveAfterEvent)
end
CallbackEventsFrame:SetScript("OnEvent", CallbackEventsFrame.OnEvent)


function Events.RegisterEventCallback(event, func, param)
    if not CallbackEvents[event] then
        CallbackEvents[event] = {}
        CallbackEventsFrame:RegisterEvent(event)
    end
    tinsert(CallbackEvents[event], {func, param})
end


function Events.UnregisterEventCallback(event, func, param)
    local funcCalls = CallbackEvents[event]
    if funcCalls then
        if RemoveAfterEvent == event then
            tinsert(RemoveAfterEvent, {func, param})
        else
            for i = #funcCalls, 1, -1 do
                if funcCalls[i][1] == func and funcCalls[i][2] == param then
                    tremove(funcCalls, i)
                    break
                end
            end
            if #funcCalls == 0 then
                CallbackEventsFrame:UnregisterEvent(event)
                CallbackEvents[event] = nil
            end
        end
    end
end
