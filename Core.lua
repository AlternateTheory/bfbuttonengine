--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local Core = Engine.Core;

local C = Engine.Constants;
local Cursor = Engine.Cursor;
local Util = Engine.Util;

local PackageEmpty = Engine.PackageEmpty;
local PackageSpell = Engine.PackageSpell;
local PackageItem = Engine.PackageItem;
local PackageMacro = Engine.PackageMacro;
local PackageMount = Engine.PackageMount;
local PackageMountFavorite = Engine.PackageMountFavorite;
local PackageBattlePet = Engine.PackageBattlePet;
local PackageBattlePetFavorite = Engine.PackageBattlePetFavorite;
local PackageFlyout = Engine.PackageFlyout;
local PackageEquipmentSet = Engine.PackageEquipmentSet;
local PackageCustom = Engine.PackageCustom;

local Packages = {};
Packages["empty"] = PackageEmpty;
Packages["spell"] = PackageSpell;
Packages["item"] = PackageItem;
Packages["macro"] = PackageMacro;
Packages["mount"] = PackageMount;
Packages["favoritemount"] = PackageMountFavorite;
Packages["battlepet"] = PackageBattlePet;
Packages["favoritebattlepet"] = PackageBattlePetFavorite;
Packages["flyout"] = PackageFlyout;
Packages["equipmentset"] = PackageEquipmentSet;
Packages["custom"] = PackageCustom;


local ActiveBFButtons = {};
local InactiveBFButtons = {};
local ReservedBFButtons = {}


function Core.AllocateButton(BFButton)

    table.insert(ActiveBFButtons, BFButton);

end

function Core.DeallocateButton(BFButton)

    Util.RemoveFromTable(ActiveBFButtons, BFButton);
    table.insert(InactiveBFButtons, BFButton);

end

function Core.GetDeallocatedButton(ActionButtonName)
    if ActionButtonName then
        local bfButton = Util.RemoveFromTable(ReservedBFButtons, type(_G[ActionButtonName]) == "table" and _G[ActionButtonName].BFButton)
        if bfButton then
            return bfButton
        else
            return Util.RemoveFromTable(InactiveBFButtons, type(_G[ActionButtonName]) == "table" and _G[ActionButtonName].BFButton)
        end
    else
        return table.remove(InactiveBFButtons)
    end
end

function Core.GetAndReserveDeallocatedButtonName()
    if #InactiveBFButtons > 0 then
        local bfButton = tremove(InactiveBFButtons)
        tinsert(ReservedBFButtons, bfButton)
        return bfButton.ABW:GetName()
    end
end

function Core.Get_AttributesForAction(Action)
    local Type = Action.Type;
    local Package = Packages[Type];
    if (Package) then
        return Package.Get_Attributes(Action);
    end
end


function Core.Get_OrderedParametersForAction(Action)
    local Type = Action.Type;
    local Package = Packages[Type];
    if (Package) then
        return Package.Get_OrderedParameters(Action);
    end
end


local Callbacks = {};
local CursorChangeCallbacks = {};
--[[------------------------------------------------
    * Register for events, either all events
    * or just those for a single button
--------------------------------------------------]]
function Core.RegisterForEvents(ActionButton, CallbackFunction, Object)
    assert(type(CallbackFunction) == "function", C.ERROR_REGISTERFOREVENTS_PARAMETERS);
    if (not ActionButton) then
        Callbacks[CallbackFunction] = Object or true;	
    elseif (ActionButton.BFButton) then
        ActionButton.BFButton.Callbacks[CallbackFunction] = Object or true;
    end
end


--[[------------------------------------------------
    Deregister For Button Events
    Either for a specific button or for all events
--------------------------------------------------]]
function Core.DeregisterForEvents(ActionButton, CallbackFunction)
    assert(type(CallbackFunction) == "function", C.ERROR_DEREGISTERFOREVENTS_PARAMETERS);
    if (not ActionButton) then		
        Callbacks[CallbackFunction] = nil;
    elseif (ActionButton.BFButton) then
        ActionButton.BFButton.Callbacks[CallbackFunction] = nil;
    end
end


--[[------------------------------------------------
    ReportEvent
    All changes to the buttons will get reported through this function
--------------------------------------------------]]
local function LoopThroughCallbacks(Callbacks, Event, ...)
    for Callback, Object in pairs(Callbacks) do
        local Status, Message;
        if (Object == true) then
            Callback( Event , ... );
        else
            Callback( Object , Event , ... );
        end
    end
end
function Core.ReportEvent(BFButton, Event, ...)
    if (BFButton) then
        LoopThroughCallbacks(BFButton.Callbacks, Event, BFButton.ABW, ...);
        LoopThroughCallbacks(Callbacks, Event, BFButton.ABW, ...);
    else
        LoopThroughCallbacks(Callbacks, Event, ...);
    end
end


function Core.RegisterForCursorChanges(CallbackFunction)
    assert(type(CallbackFunction) == "function", C.ERROR_REGISTERFOREVENTS_PARAMETERS);
    CursorChangeCallbacks[CallbackFunction] = true;
end


function Core.ReportCursorActionChange()
    for Callback, v in pairs(CursorChangeCallbacks) do
        Callback( Cursor.GetCursor() );
    end
end


local CombatQueuedFunctions = {};
local CombatQueuedFunctionsParams = {};
--[[------------------------------------------------
    Update Button Functions
--------------------------------------------------]]
function Core.LeaveCombatUpdate()
    for k, v in pairs(CombatQueuedFunctions) do
        k(unpack(CombatQueuedFunctionsParams[k]));
        CombatQueuedFunctions[k] = nil;
        CombatQueuedFunctionsParams[k] = nil;
    end

    -- Make sure we're otherwise all up to date
    Core.FullUpdateAll();
end

function Core.LeaveCombatQueueUpFunction(f, ...)
    CombatQueuedFunctions[f] = true;
    CombatQueuedFunctionsParams[f] = {...};
end

function Core.UpdateActionAll()
    if (InCombatLockdown()) then
        Core.LeaveCombatQueueUpFunction(Core.UpdateActionAll);
        return C.INFO_QUEUED_FOR_AFTER_COMBAT;
    end
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateAction(Type);
    end
end

function Core.UpdateActionCustom()
    if (InCombatLockdown()) then
        Core.LeaveCombatQueueUpFunction(Core.UpdateActionCustom);
        return C.INFO_QUEUED_FOR_AFTER_COMBAT;
    end
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateAction("custom");
    end
end

function Core.UpdateActionEquipmentSet()
    if (InCombatLockdown()) then
        Core.LeaveCombatQueueUpFunction(Core.UpdateActionEquipmentSet);
        return C.INFO_QUEUED_FOR_AFTER_COMBAT;
    end
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateAction("equipmentset");
    end
end

function Core.UpdateActionItem(previousHyperlink, newHyperlink)
    if (InCombatLockdown()) then
        Core.LeaveCombatQueueUpFunction(Core.UpdateActionItem, previousHyperlink, newHyperlink);
        return C.INFO_QUEUED_FOR_AFTER_COMBAT;
    end
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateAction("item", previousHyperlink, newHyperlink);
    end
end

function Core.UpdateActionMacro()
    if (InCombatLockdown()) then
        Core.LeaveCombatQueueUpFunction(Core.UpdateActionMacro);
        return C.INFO_QUEUED_FOR_AFTER_COMBAT;
    end
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateAction("macro");
    end
end

function Core.UpdateActionMount()
    if (InCombatLockdown()) then
        Core.LeaveCombatQueueUpFunction(Core.UpdateActionMount);
        return C.INFO_QUEUED_FOR_AFTER_COMBAT;
    end
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateAction("mount");
    end
end

function Core.UpdateActionPetAbility()
    if (InCombatLockdown()) then
        Core.LeaveCombatQueueUpFunction(Core.UpdateActionPetAbility);
        return C.INFO_QUEUED_FOR_AFTER_COMBAT;
    end
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateAction("petability");
    end
end

function Core.UpdateActionPetAbilityHunter()
    if (InCombatLockdown()) then
        Core.LeaveCombatQueueUpFunction(Core.UpdateActionPetAbilityHunter);
        return C.INFO_QUEUED_FOR_AFTER_COMBAT;
    end
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateAction("petabilityhunter");
    end
end

function Core.UpdateActionBattlePet()
    if (InCombatLockdown()) then
        Core.LeaveCombatQueueUpFunction(Core.UpdateActionBattlePet);
        return C.INFO_QUEUED_FOR_AFTER_COMBAT;
    end
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateAction("battlepet");
    end
end

function Core.UpdateActionSpell()
    if (InCombatLockdown()) then
        Core.LeaveCombatQueueUpFunction(Core.UpdateActionSpell);
        return C.INFO_QUEUED_FOR_AFTER_COMBAT;
    end
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateAction("spell");
    end
end

function Core.FullUpdateAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:FullUpdate();
    end
end

function Core.UpdateIconAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateIcon();
    end
end

function Core.UpdateCheckedAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateChecked();
    end
end

function Core.UpdateUsableAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateUsable();
    end
end

function Core.UpdateCooldownAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateCooldown();
    end
end

function Core.UpdateEquippedAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateEquipped();
    end
end

function Core.UpdateTextAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateText();
    end
end

function Core.UpdateGlowAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateGlow();
    end
end

function Core.UpdateShineAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateShine();
    end
end

function Core.UpdateFlashRegistrationAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateFlashRegistration();
    end
end

function Core.UpdateRangeCheckRegistrationAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateRangeCheckRegistration();
    end
end

function Core.UpdateKeybindHighlightAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateKeybindHighlight();
    end
end

function Core.UpdateRespondToMouseAll()
    if InCombatLockdown() then
        return;
    end

    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateRespondToMouse();
    end
end

function Core.UpdateAlphaAll()
    local b = ActiveBFButtons;
    for i = 1, #b do
        b[i]:UpdateAlpha();
    end
end

--[[------------------------------------------------
    * This is a central place to route errors to
    * It's not intended for basic events that are expected
    * it is for occurences not expected
    * These are cleanly handled errors (more or less)
        - Meaning the addon should still be able to operate
        - The player does not necessarily need to know (though they may see odd behaviour)
        - Mostly I intend for the primary addon to log them so that such can be reviewed
--------------------------------------------------]]
--function Core.ErrorOccurred(BFButton, Error, ...)
--    Core.ReportEvent(BFButton, Error, ...);
--    return Error;
--end


--[[------------------------------------------------
    Flash updating
--------------------------------------------------]]
local FlashButtons = {};
local FlashTimerFrame = CreateFrame("FRAME");
FlashTimerFrame.CountDown = 0;
FlashTimerFrame.FlashOn = false;
function Core.RegisterForFlash(BFButton, IsFlashing)
    if (IsFlashing) then
        FlashButtons[BFButton] = true;
        if (FlashTimerFrame.FlashOn) then
            BFButton.ABWFlash:Show();
        end
    else
        FlashButtons[BFButton] = nil;
        BFButton.ABWFlash:Hide();
    end
end

function FlashTimerFrame:OnUpdate(Elapsed)
    local CountDown = self.CountDown - Elapsed;
    if (CountDown <= 0) then
        if (-CountDown >= ATTACK_BUTTON_FLASH_TIME) then
            CountDown = ATTACK_BUTTON_FLASH_TIME;
        else
            CountDown = ATTACK_BUTTON_FLASH_TIME + CountDown;
        end
        
        if (self.FlashOn) then
            self.FlashOn = false;
            for k, v in pairs(FlashButtons) do
                k.ABWFlash:Hide();
            end
        else
            self.FlashOn = true;
            for k, v in pairs(FlashButtons) do
                k.ABWFlash:Show();
            end
        end
    end
    self.CountDown = CountDown;
end
FlashTimerFrame:SetScript("OnUpdate", FlashTimerFrame.OnUpdate);




--[[------------------------------------------------
    Register or unregister the BFButton for periodic (.2 of a second) updating of the range indicator
    
    IsInRange describe the following
    - 1: In range       Register for periodic checks
    - 0: Out of range   Register for periodic checks
    - nil: N/A          Unregister

    Note: If the button is already displaying a hotkey binding, then that will be used as the range indicator
          Otherwise the retail WoW dot will be shown and used (or hidden)
--------------------------------------------------]]
local RangeButtons = {};
local RangeTimerFrame = CreateFrame("FRAME");
RangeTimerFrame.CountDown = 0;
function Core.RegisterForRangeChecks(BFButton, IsInRange)

    local HotKey = BFButton.ABWHotKey;
    if IsInRange ~= nil then

        -- IsInRange is 1 or 0. Register for periodic check
        RangeButtons[BFButton] = true;
        if (HotKey:GetText() == RANGE_INDICATOR) then
            HotKey:Show();
        end
        -- Update RangeIndicator
        BFButton:SetInRangeIndication(IsInRange);

    else

        -- IsInRange is nil. Unregister
        RangeButtons[BFButton] = nil;
        if (HotKey:GetText() == RANGE_INDICATOR) then
            HotKey:Hide();
        end
        -- Update RangeIndicator
        BFButton:SetInRangeIndication();

    end

end

function RangeTimerFrame:OnUpdate(Elapsed)
    local CountDown = self.CountDown - Elapsed;
    if (CountDown <= 0) then
        for k, v in pairs(RangeButtons) do
            k:SetInRangeIndication(k:IsInRange());
        end
        CountDown = TOOLTIP_UPDATE_TIME;
    end
    self.CountDown = CountDown;
end
RangeTimerFrame:SetScript("OnUpdate", RangeTimerFrame.OnUpdate);


-- Note unlike the individual version below - this function will not perform other updates or resource release
-- This is because the purpose of this function is to deal with a temporary display toggle
function Core.UpdateButtonShowHideAll()
    if (InCombatLockdown()) then
        return;
    end

    local b = ActiveBFButtons;
    local CursorHasAction = Cursor.HasValidAction();

    for i = 1, #b do
        local BFButton = b[i];
        local ABW = BFButton.ABW;
        if BFButton.Enabled and ( BFButton.AlwaysShowGrid or BFButton.Type ~= "empty" or CursorHasAction ) then
            if not ABW:IsShown() then
                ABW:Show();
            end
        else
            if ABW:IsShown() then
                ABW:Hide();
            end
        end
    end

    Core.ReportEvent(nil, C.EVENT_SHOWGRID, CursorHasAction);
end


local KeybindMode = false;
function Core.SetKeybindMode(Value)

    KeybindMode = Value;
    Core.UpdateKeybindHighlightAll()
end


function Core.InKeybindMode()

    return KeybindMode;

end
