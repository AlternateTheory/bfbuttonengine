--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014 - 2022

    During combat use secure code pathways to change the button action
]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local SecureActionSwap = Engine.SecureActionSwap;
local C = Engine.Constants;
local Util = Engine.Util;
local Core = Engine.Core;
local Methods = Engine.Methods;


--[[
    OnDrag_SecureHandler

    This handler will wrap each buttons OnReceiveDrag and OnDragStart events

    During combat the handler will determine if a drag operation should change the buttons action

    To see how OnDrag secure handlers work Look at code inside Blizzard's SecureHandlers.lua
    In particular the functions:
    - Wrapped_Drag
    - PickupAny

]]
local OnDrag_SecureHandler = CreateFrame("FRAME", nil, nil, "SecureHandlerBaseTemplate");


OnDrag_SecureHandler:Execute( string.format( [[
    SpellMap = newtable();
    pressAndHoldAction = newtable();
    FavoriteMountID = %i; ]] , C.SUMMON_RANDOM_FAVORITE_MOUNT_ID ) );

--[[------------------------------------------------
    UpdateSecureSpellMap
        WoW v9.0.2
            * Provides mapping from SpellID to the correct value to use for the "spell" attribute
            * The first three tabs contain spells we can currently use

            * loop explanation
                * SPELL
                    * SPELL indicates a "known" spell
                    * FUTURESPELL is a currently unknown spell
                    * For spells that act as an override always use the base spell info as the AttrValue (the jury is out on SpellID... but SpellFullName is the majority of cases and is the reliable method)
                    * We will update the map with known spell info, but we will only add unknown spells we will not update - this is to avoid possible issues around override type spells that may not report
                      their nature correctly when unknown
                * FLYOUT
                    * We need to check IsKnown for both the flyout and it's slots

                * Professions
                    * We need to capture professions and use the SpellID's for those

    The map is created when the function is first loaded, the event manager will run this function when necessary to update
--------------------------------------------------]]
function SecureActionSwap.UpdateSecureSpellMap()
    if (InCombatLockdown()) then
        Core.LeaveCombatQueueUpFunction(SecureActionSwap.UpdateSecureSpellMap)
        return
    end

    local updateSecureSpellMapEntry =
    [[
        local spellID = owner:GetAttribute("spell_id")
        if spellID then
            SpellMap[spellID] = owner:GetAttribute("spell_value")
            pressAndHoldAction[spellID] = owner:GetAttribute("press_and_hold_action")
        end
    ]]

    for slotIndex in Util.SpellBookSlotIterator() do
        local spellBookItem = C_SpellBook.GetSpellBookItemInfo(slotIndex, Enum.SpellBookSpellBank.Player)
        if spellBookItem.itemType == Enum.SpellBookItemType.Spell or spellBookItem.itemType == Enum.SpellBookItemType.FutureSpell then
            local spellID = spellBookItem.spellID
            OnDrag_SecureHandler:SetAttribute("spell_id", spellID)
            OnDrag_SecureHandler:SetAttribute("spell_value", Util.SpellFullName(spellID))
            OnDrag_SecureHandler:SetAttribute("press_and_hold_action", C_Spell.IsPressHoldReleaseSpell(spellID))
            OnDrag_SecureHandler:Execute(updateSecureSpellMapEntry)

        elseif spellBookItem.itemType == Enum.SpellBookItemType.Flyout then
            local actionID = spellBookItem.actionID
            local _, _, numSlots, isKnown = GetFlyoutInfo(actionID)
            for k = 1, numSlots do
                local spellID, overrideSpellID, isKnown = GetFlyoutSlotInfo(actionID, k)
                OnDrag_SecureHandler:SetAttribute("spell_id" , spellID)
                OnDrag_SecureHandler:SetAttribute("spell_value", Util.SpellFullName(spellID))
                OnDrag_SecureHandler:SetAttribute("press_and_hold_action", C_Spell.IsPressHoldReleaseSpell(spellID))
                OnDrag_SecureHandler:Execute(updateSecureSpellMapEntry)
                -- It's not clear the override info is needed but it will be captured
                OnDrag_SecureHandler:SetAttribute("spell_id" , overrideSpellID)
                OnDrag_SecureHandler:Execute(updateSecureSpellMapEntry)
            end
        end
    end

    -- Note: I anticipate in future Blizz will refactor these api calls into a namespace and to return results as a table (with enum usage, the enums already exist)
    local prof1, prof2, arch, fish, cook = GetProfessions()
    local profs = {prof1, prof2, arch, fish, cook}
    for k, prof in pairs(profs) do
        local name, texture, rank, maxRank, numSpells, spellOffset, skillLine, rankModifier, specializationIndex, specializationOffset, skillLineName = GetProfessionInfo(prof)
        for i = 1, numSpells do
            local spellBookItem = C_SpellBook.GetSpellBookItemInfo(spellOffset + i, Enum.SpellBookSpellBank.Player)
            OnDrag_SecureHandler:SetAttribute("spell_id" , spellBookItem.spellID)
            OnDrag_SecureHandler:SetAttribute("spell_value", spellBookItem.spellID)
            OnDrag_SecureHandler:SetAttribute("press_and_hold_action", false)
            OnDrag_SecureHandler:Execute(updateSecureSpellMapEntry)
        end
    end
end
SecureActionSwap.UpdateSecureSpellMap()


--[[
    OnReceiveDrag_SwapAction_SecureWrapper

    -- All normally supported actions can be dragged onto the bar (except custom actions)
    -- Most supported actions can be then picked up and placed on the Cursor (Mounts/Critters/CustomActions excluded)
        -- CustomActions might be possible to implement at a later date
]]
local OnReceiveDrag_SwapAction_SecureWrapper = [[
    -- The following variables are supplied by the RestrictedEnvironment when this snippet is called
    -- button = MouseButton used
    -- kind, value, ... = GetCursorInfo()
    -- owner = ButtonActionChanger_SecureHandler
    -- self = BF Button

    -- If we are in combat then process the action swap (outside of combat the insecure OnReceiveDrag will process Action Swapping)
    if (PlayerInCombat()) then
        if (self:GetAttribute("FullLock") or self:GetAttribute("Source")) then
            return;
        end

        local TypeValue , ActionAttribute , ActionValue , ActionType , ActionParameter1 , ActionParameter2 = owner:RunAttribute( "GetActionInfo_From_CursorInfo" , kind , value , ... );
        if (ActionType) then
            -- These two parameters can be returned back to Blizz code where the PickupAny(...) will set the cursor
            local OldActionType         = self:GetAttribute("ActionType");
            local OldActionParameter1   = self:GetAttribute("ActionParameter1");

            -- This will set the Button to perform the new action , the last parameter (true) instructs to push the change to the buttons secure listener
            self:RunAttribute( "SetAction_Secure" , TypeValue , ActionAttribute , ActionValue , ActionType , ActionParameter1 , ActionParameter2 , nil , true );
            self:CallMethod( "ReportEvent_SetActionFromCursor" );

            -- Return values to set the cursor to the action the button previously had (see Blizzard's Wrapped_Drag and PickupAny)
            -- NB: If "clear" is the first parameter the cursor is cleared, and the following values are used to pickup a new action
            return "clear", OldActionType, OldActionParameter1;
        end
    end
]];


--[[
    OnDragStart_SwapAction_SecureWrapper

    -- Drag the action off the button... If the cursor has an action on it already, that action will swap onto the Button
    -- Most supported actions can be picked up and placed on the Cursor (Mounts/Critters/CustomActions excluded)
        -- CustomActions might be possible to implement
]]
local OnDragStart_SwapAction_SecureWrapper = [[
    -- Supplied variables
    -- button = MouseButton used
    -- kind, value, ... = GetCursorInfo()
    -- owner = ButtonActionChanger_SecureHandler
    -- self = BF Button

    -- If we are in combat then process the action swap (outside of combat the insecure OnDragStart will process Action Swapping)
    if (PlayerInCombat()) then
        if (self:GetAttribute("FullLock")) then
            return;
        end

        if (self:GetAttribute("Locked") and not IsModifiedClick("PICKUPACTION")) then
            return;
        end

        -- These two parameters can be returned back to Blizz code where the PickupAny(...) will set the cursor
        local OldActionType         = self:GetAttribute("ActionType");
        local OldActionParameter1   = self:GetAttribute("ActionParameter1");

        local TypeValue , ActionAttribute , ActionValue , ActionType , ActionParameter1 , ActionParameter2 = owner:RunAttribute( "GetActionInfo_From_CursorInfo" , kind , value , ... );
        if (not self:GetAttribute("Source") and ActionType) then
            self:RunAttribute( "SetAction_Secure" , TypeValue , ActionAttribute , ActionValue , ActionType , ActionParameter1 , ActionParameter2 , nil , true );
            self:CallMethod( "ReportEvent_SetActionFromCursor" );
        end
        return "clear", OldActionType, OldActionParameter1;
    end
]];


--[[
    GetActionInfo_From_CursorInfo

    Returns
        ActionType , TypeValue , ActionAttribute , ActionValue , ActionParameter1 , ActionParameter2
]]
OnDrag_SecureHandler:SetAttribute("GetActionInfo_From_CursorInfo", [[
    local Command , Data , Subvalue , SubSubvalue , SpellExtendedValue = ... ; -- SpellExtendedValue is needed for Sinister Strike; the players Spellbook reports Sinister Strike as 1752 (SpellExtendedValue) and not 193315 (SubSubvalue)
    local ActionType , TypeValue , ActionAttribute , ActionValue , ActionParameter1 , ActionParameter2 ;

    if (Command == "spell") then
        ActionType = "spell";
        TypeValue = "spell";
        ActionAttribute = "spell";
        ActionValue = SpellMap[SubSubvalue] or SpellMap[SpellExtendedValue or 0] ;
        ActionParameter1 = SubSubvalue;
        ActionParameter2 = pressAndHoldAction[SubSubvalue];
        if (not ActionValue) then
            return "empty";
        end

    elseif (Command == "item") then
        ActionType = "item";
        TypeValue = "item";
        ActionAttribute = "item";
        ActionValue = strmatch(Subvalue, "%[(.+)%]");
        ActionParameter1 = Data;
        ActionParameter2 = ActionValue;

    elseif (Command == "macro") then
        ActionType = "macro";
        TypeValue = "macro";
        ActionAttribute = "macro";
        ActionValue = Data;
        ActionParameter1 = Data;

    elseif (Command == "mount") then
        if (Data == FavoriteMountID) then
            ActionType = "favoritemount";
            ActionValue = "/script C_MountJournal.SummonByID(0)" ;
        else
            ActionType = "mount";
            ActionValue = format( "/script C_MountJournal.SummonByID(%i)" , Data );
        end
        TypeValue = "macro";
        ActionAttribute = "macrotext";
        ActionParameter1 = Data;

    elseif (Command == "battlepet") then
        ActionType = "battlepet";
        TypeValue = "macro";
        ActionAttribute = "macrotext";
        ActionValue = format( "/summonpet %s" , Data );
        ActionParameter1 = Data;

    elseif (Command == "flyout") then
        ActionType = "flyout";
        TypeValue = "attribute";
        ActionAttribute = "spell";
        ActionValue = Data;
        ActionParameter1 = Data;

    elseif (Command == "equipmentset") then
        ActionType = "equipmentset";
        TypeValue = "equipmentset";
        ActionAttribute = "equipmentset";
        ActionValue = Data;
        ActionParameter1 = Data;

    else
        return nil , nil , nil , "empty";
    end

    return TypeValue , ActionAttribute , ActionValue , ActionType , ActionParameter1 , ActionParameter2 ;

]]);


--[[
    SetAction_Secure

    Button Attribute
    Called with Action attributes, and ordered parameters to set the Button action securely

    Set FromCursor to send the changes securely to a listener
]]
local SetAction_Secure = [[

    local TypeValue , ActionAttribute , ActionValue , ActionType , ActionParameter1 , ActionParameter2 , ActionParameter3 , FromCursor = ...;

    -- Clear the few attributes that might not get set and could interfere with the action
    self:SetAttribute("macro", nil);
    self:SetAttribute("IsEmpowerSpell", ActionType == "spell" and ActionParameter2);
    self:SetID(0)

    -- Set the attributes for the action
    if ActionType ~= "empty" then
        self:SetAttribute( "type" , TypeValue );
        self:SetAttribute( "typerelease" , TypeValue );
        if ActionAttribute == "ID" then
            self:SetID(ActionValue)
        else
            self:SetAttribute( ActionAttribute , ActionValue );
        end

        if Enabled then
            -- UpdateShown
            if not self:IsShown() then
                self:Show();
            end
            -- UpdateKeybind
            if ActiveKeybind == nil and Keybind ~= nil then
                self:SetAttribute("ActiveKeybind", Keybind);
                ActiveKeybind = Keybind;
                self:SetBindingClick(false, Keybind, self:GetName(), "KeyBind");
            end
        end

    else
        self:SetAttribute( "type" , nil );
        self:SetAttribute( "typerelease" , nil );

        if Enabled and not AlwaysShowGrid then
            -- UpdateShown
            if self:IsShown() then
                self:Hide();
            end
            -- UpdateKeybind
            if ActiveKeybind ~= nil then
                self:SetAttribute("ActiveKeybind", nil);
                ActiveKeybind = nil;
                self:ClearBindings();
            end
        end
    end

    -- Capture the ActionType and ActionParameter1 as these are used to set the cursor when an attempt to pickup the action occurs.
    self:SetAttribute("ActionType"          , ActionType);
    self:SetAttribute("ActionParameter1"    , ActionParameter1);

    -- Do all the non secure state update for the Button using the Ordered ActionParameters
    self:CallMethod( "SetAction_OrderedParameters" , ActionType , ActionParameter1 , ActionParameter2 , ActionParameter3 );

    -- Check if we should try and push the change to a SecureListener
    if (FromCursor and SecureListener) then
        SecureListener:RunAttribute( SecureListener_CallbackAttribute , self:GetName() , ... );
    end

]];


local SetEnabled = [[

    local EnabledValue, BlockFullUpdate = ...;
    self:SetAttribute("Enabled", EnabledValue);
    Enabled = EnabledValue;

    -- UpdateShown and Keybind
    local IsShown = self:IsShown();
    if not Enabled then
        if IsShown then
            self:Hide();
        end
        if ActiveKeybind ~= nil then
            self:SetAttribute("ActiveKeybind", nil);
            ActiveKeybind = nil;
            self:ClearBindings();
        end
    elseif AlwaysShowGrid or self:GetAttribute("ActionType") ~= "empty" then
        if not IsShown then
            self:Show();
        end
        if ActiveKeybind == nil and Keybind ~= nil then
            self:SetAttribute("ActiveKeybind", Keybind);
            ActiveKeybind = Keybind;
            self:SetBindingClick(false, Keybind, self:GetName(), "KeyBind");
        end
    end

    self:CallMethod( "BFSetEnabled" , Enabled , BlockFullUpdate );

]]


--[[
    SetAction_OrderedParameters

    Button method
    This function can set the button action using ordered parameters (ActionType, Parameter1, Parameter2, Parameter3)
    
    During combat this function should only be called from SetAction_Secure

 ]]
local function SetAction_OrderedParameters( ABW , ... )
    ABW.BFButton:SetAction_OrderedParameters(... );
end


local function BFSetEnabled( ABW, Enabled, BlockFullUpdate)
    ABW.BFButton:SetEnabled(Enabled, BlockFullUpdate);
end


--[[
    SetSecureListener

    Attach a secure listener to call when the action is changed via SetAction_Secure
]]
function SecureActionSwap.SetSecureListener(ABW, SecureListener, CallbackAttribute)
    if (InCombatLockdown()) then
        return;
    end

    ABW:SetFrameRef("securelistener", SecureListener);
    ABW:SetAttribute("securelistener_callbackattribute", CallbackAttribute);
    ABW:Execute( [[
        SecureListener = self:GetFrameRef("securelistener");
        SecureListener_CallbackAttribute = self:GetAttribute("securelistener_callbackattribute"); ]] );
end


--[[
    Unset the SecureListener
]]
function SecureActionSwap.UnsetSecureListener(ABW)
    ABW:Execute( [[
        SecureListener = nil;
        SecureListener_CallbackAttribute = nil; ]] );
end


--[[
    ReportEvent_SetActionFromCursor
]]
local function ReportEvent_SetActionFromCursor(ABW)
    Core.ReportEvent( ABW.BFButton , C.EVENT_SETACTIONFROMCURSOR , ABW.BFButton.Action );
end


--[[
    SetupButton

    Call this with a new button to attach/wrap secure code paths to the button that can process changing an action while in combat
]]
function SecureActionSwap.SetupButton(ABW)

    ABW:SetAttribute("SetAction_Secure", SetAction_Secure);
    ABW:SetAttribute("SetEnabled", SetEnabled);
    ABW.SetAction_OrderedParameters = SetAction_OrderedParameters;
    ABW.BFSetEnabled = BFSetEnabled;
    ABW.ReportEvent_SetActionFromCursor = ReportEvent_SetActionFromCursor;

    OnDrag_SecureHandler:WrapScript(ABW, "OnReceiveDrag", OnReceiveDrag_SwapAction_SecureWrapper);
    OnDrag_SecureHandler:WrapScript(ABW, "OnDragStart", OnDragStart_SwapAction_SecureWrapper);
end
