--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine        = AddonTable.ButtonEngine;
local PackageMacro  = Engine.PackageMacro;
local Scripts       = Engine.Scripts;
local Methods       = Engine.Methods;

-- Take local copy of functions to slightly improve performance (in theory)
local Util                          = Engine.Util;
local Util_GetItemIDFromHyperlink   = Util.GetItemIDFromHyperlink;
local Util_SpellFullName            = Util.SpellFullName;

local Core                                  = Engine.Core;
local Core_ReportEvent                      = Core.ReportEvent;
local Core_RegisterForFlash                 = Core.RegisterForFlash;
local Core_RegisterForRangeChecks           = Core.RegisterForRangeChecks;

local C                                     = Engine.Constants;
local C_EVENT_UPDATEACTION                  = C.EVENT_UPDATEACTION;
local C_QUESTION_MARK                       = C.QUESTION_MARK;

local PackageItem                           = Engine.PackageItem;
local PackageItem_IsInRange                 = PackageItem.IsInRange;
local PackageItem_UpdateChecked             = PackageItem.UpdateChecked;
local PackageItem_UpdateCooldown            = PackageItem.UpdateCooldown;
local PackageItem_UpdateEquipped            = PackageItem.UpdateEquipped;
local PackageItem_UpdateRangeCheckRegistration   = PackageItem.UpdateRangeCheckRegistration;
local PackageItem_UpdateText                = PackageItem.UpdateText;
local PackageItem_UpdateTooltip             = PackageItem.UpdateTooltip;
local PackageItem_UpdateUsable              = PackageItem.UpdateUsable;

local PackageSpell                          = Engine.PackageSpell;
local PackageSpell_IsInRange                = PackageSpell.IsInRange;
local PackageSpell_UpdateChecked            = PackageSpell.UpdateChecked;
local PackageSpell_UpdateCooldown           = PackageSpell.UpdateCooldown;
local PackageSpell_UpdateFlashRegistration  = PackageSpell.UpdateFlashRegistration;
local PackageSpell_UpdateGlow               = PackageSpell.UpdateGlow;
local PackageSpell_UpdateRangeCheckRegistration  = PackageSpell.UpdateRangeCheckRegistration;
local PackageSpell_UpdateText               = PackageSpell.UpdateText;
local PackageSpell_UpdateTooltip            = PackageSpell.UpdateTooltip;
local PackageSpell_UpdateUsable             = PackageSpell.UpdateUsable;

local GetToyInfo                    = C_ToyBox.GetToyInfo;

local MAX_ACCOUNT_MACROS            = MAX_ACCOUNT_MACROS;

local ActionButton_HideOverlayGlow  = ActionButton_HideOverlayGlow;
local ActionButton_ShowOverlayGlow  = ActionButton_ShowOverlayGlow;
local CooldownFrame_Set             = CooldownFrame_Set;
--local GameTooltip                   = GameTooltip;
--local GameTooltip_SetDefaultAnchor  = GameTooltip_SetDefaultAnchor;
local GetMacroInfo                  = GetMacroInfo;
local GetMacroItem                  = GetMacroItem;
local GetMacroSpell                 = GetMacroSpell;
local GetMouseFocus                 = Util.GetMouseFocus;
local GetNumMacros                  = GetNumMacros;
local InCombatLockdown              = InCombatLockdown;
local SecureCmdOptionParse          = SecureCmdOptionParse;


--[[------------------------------------------------

    For all macro buttons check if the "display" action has changed (spell item or neither) and trigger a full update in response.

--------------------------------------------------]]
local MacroBFButtons = {};
function PackageMacro.UpdateAllMacroDisplayActions()

    for BFButton in pairs(MacroBFButtons) do
        if (BFButton:UpdateMacroDisplayAction()) then
            BFButton:FullUpdate(true);
        end
    end

end


PackageMacro.UpdateShine                = Scripts.EmptyF;
PackageMacro.SwapActionWithButtonAction = Scripts.SwapActionWithButtonAction;


--[[------------------------------------------------

    - Set Macro

    - While not in combat the Buttons onclick action will be set
        - values to use in the restricted environment will also be set

    - This function can be called while in combat if the action was swapped using the SecureActionSwap
        - The SecureActionSwap will handle setting the onclick and restricted environment up

--------------------------------------------------]]
function PackageMacro.SetAction(BFButton, MacroIndex, MacroName, MacroBody)

    local Action            = BFButton.Action;

    if (not MacroName) then
        local MacroIcon;
        MacroName, MacroIcon, MacroBody = GetMacroInfo(MacroIndex);
    end
    BFButton.Type           = "macro";
    BFButton.MacroIndex     = MacroIndex;
    BFButton.MacroName      = MacroName;
    BFButton.MacroBody      = MacroBody;

    Action.Type             = "macro";
    Action.MacroIndex       = MacroIndex;
    Action.MacroName        = MacroName;
    Action.MacroBody        = MacroBody;

    if (not InCombatLockdown()) then
        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();
    end
    MacroBFButtons[BFButton] = true;
    BFButton:FullUpdate();

end


--[[------------------------------------------------
    When an event occurs that indicates macros have been altered
--------------------------------------------------]]
function PackageMacro.UpdateAction(BFButton, Type)

    if (Type and Type ~= "macro") then
        return;
    end

    local Action = BFButton.Action;
    local Changed, UpdatedIndex = BFButton.LocateMacro(BFButton.MacroIndex, BFButton.MacroName, BFButton.MacroBody);

    if (not Changed) then
        return;
    end

    if (UpdatedIndex) then

        local MacroName, MacroTexture, MacroBody = GetMacroInfo(UpdatedIndex);
        BFButton.MacroIndex = UpdatedIndex;
        BFButton.MacroName = MacroName;
        BFButton.MacroBody = MacroBody;

        Action.MacroIndex = UpdatedIndex;
        Action.MacroName = MacroName;
        Action.MacroBody = MacroBody;

        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();

        Core_ReportEvent(BFButton, C_EVENT_UPDATEACTION, Action);

    else
        -- Macro has not been found so treat it as removed
        BFButton:SetAction_OrderedParameters("empty");

    end

end


--[[------------------------------------------------

    Set the on click action that will be triggered

--------------------------------------------------]]
function PackageMacro.UpdateAttributes(BFButton)

    local ABW = BFButton.ABW;
    ABW:SetAttribute("type", "macro");
    ABW:SetAttribute("typerelease", "macro");
    ABW:SetAttribute("macro", BFButton.MacroIndex);

end


--[[------------------------------------------------

    Get the attributes that are used for the on click action

--------------------------------------------------]]
function PackageMacro.Get_Attributes(Action)
    -- return   TypeValue , ActionAttribute , ActionValue
    return "macro", "macro", Action.MacroIndex;
end


--[[------------------------------------------------

    Set parameters to describe the action in the restricted environment of the button
    This will support secure changing of the action

--------------------------------------------------]]
function PackageMacro.UpdateSecureCursor(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("ActionType", "macro");
    ABW:SetAttribute("ActionParameter1", BFButton.MacroIndex);
    ABW:SetAttribute("ActionParameter2", BFButton.MacroName);
    ABW:SetAttribute("ActionParameter3", BFButton.MacroBody);
end


--[[------------------------------------------------

    Finalize will reset elements of the button that it changed
    Only the elements that need to be reset are

--------------------------------------------------]]
function PackageMacro.Finalize(BFButton)

    local Action = BFButton.Action;
    Action.MacroIndex = nil;
    Action.MacroName = nil;
    Action.MacroBody = nil;

    local ABW = BFButton.ABW;

    -- Checked
    ABW:SetChecked(false);

    -- Glow
    ActionButton_HideOverlayGlow(ABW);

    -- Icon
    BFButton.ABWIcon:SetTexture(nil);

    -- Usable
    BFButton:SetDisplayUsable(true);

    -- Cooldown
    BFButton.ABWCooldown:Clear();

    -- Equipped
    BFButton.ABWBorder:Hide();

    -- Text
    BFButton.ABWCount:SetText(nil);

    -- Name
    BFButton.ABWName:SetText(nil);

    -- Flash
    Core_RegisterForFlash(BFButton);

    -- Range
    Core_RegisterForRangeChecks(BFButton);

    -- Remove button from Macro specific buttons list
    MacroBFButtons[BFButton] = nil;

    if (not InCombatLockdown()) then
        ABW:SetAttribute("macro", nil);
    end
end


--[[------------------------------------------------
    LocateMacro
    Macros are a pain, this is a heuristic to try and relocate them
    Returns ChangeDetected, MacroIndex

    Note - this will report no change if the player has no macros as this may indicate the macro information has not yet
    loaded
--------------------------------------------------]]
function PackageMacro.LocateMacro(MacroIndex, MacroName, MacroBody)

    local AccMacros, CharMacros = GetNumMacros();
    local FirstMacro, LastMacro;

    if (MacroIndex > MAX_ACCOUNT_MACROS) then

        if (CharMacros == 0) then
            -- Assume the character macros have not been loaded yet
            return false, MacroIndex;
        end

        FirstMacro = MAX_ACCOUNT_MACROS + 1;
        LastMacro = MAX_ACCOUNT_MACROS + CharMacros;

    else

        if (AccMacros == 0) then
            -- To be safe assume the account wide macros have not loaded yet
            return false, MacroIndex;
        end

        FirstMacro = 1;
        LastMacro = AccMacros;

    end


    -- 1) Check: No change
    local Name, Icon, Body = GetMacroInfo(MacroIndex);
    if (Name == MacroName and Body == MacroBody) then
        return false, MacroIndex;
    end

    -- 2) Check: Down shift by 1
    --    Causes: Macro name change shifting a lower macro higher
    --            Lower Index macro deleted
    if (MacroIndex - 1 >= FirstMacro) then
        Name, Icon, Body = GetMacroInfo(MacroIndex - 1);
        if (Name == MacroName and Body == MacroBody) then
            return true, MacroIndex - 1;
        end
    end

    -- 3) Check: Up shift by 1
    --    Causes: Macro name change shifting higher macro lower
    --            New macro with lower positioning (name)
    if (MacroIndex + 1 <= LastMacro) then
        Name, Icon, Body = GetMacroInfo(MacroIndex + 1);
        if (Name == MacroName and Body == MacroBody) then
            return true, MacroIndex + 1;
        end
    end

    -- 4) Check: Macro body changed
    Name, Icon, Body = GetMacroInfo(MacroIndex);
    if (Name == MacroName) then
        return true, MacroIndex;
    end

    -- 5) Scan:
    local BodyMatch, NameMatch;
    for i = FirstMacro, LastMacro do
        Name, Icon, Body = GetMacroInfo(i);
        if (Name == MacroName and Body == MacroBody) then
            -- 5 a) Body and name match
            return true, i;

        elseif (Body == MacroBody) then
            BodyMatch = i;

        elseif (Name == MacroName) then
            NameMatch = i;
        end
    end

    -- 5 b) Body matched
    if (BodyMatch) then
        return true, BodyMatch;
    end

    -- 5 c) Name matched
    if (NameMatch) then
        return true, NameMatch;
    end

    -- 6) No match
    return true, nil;

end


--[[------------------------------------------------
    Full Update
    Note:
        NoModeCheck is used when called from UpdateMacro since UpdateMacroMode has already performed
--------------------------------------------------]]
function PackageMacro.FullUpdate(BFButton, NoModeCheck)
    if (not NoModeCheck) then
        BFButton:UpdateMacroDisplayAction();
    end
    BFButton:UpdateIcon();
    BFButton:UpdateChecked();
    BFButton:UpdateUsable();
    BFButton:UpdateCooldown();
    BFButton:UpdateEquipped();
    BFButton:UpdateText();
    BFButton:UpdateGlow();
    BFButton:UpdateFlashRegistration();
    BFButton:UpdateRangeCheckRegistration();

    if (GetMouseFocus() == BFButton.ABW) then
        BFButton:UpdateTooltip();
    end
end


--[[------------------------------------------------
    - Calculate if the macro has changed what action it performs
    - Use GetMacroItem and GetMacroSpell to determine which action the macro is intended to report
        - due to #show #showtooltip the reported action may not be the same one that is triggered by the macro
    - Use SecureCmdOptionParse to get the target... This is not a perfect approach if there is a #show or #showtooltip
        - usually it will be fine but a #show directive with no condition SecureCmdOptionParse will overshoot and pickup a target directive
            from a later condition potentially

    -- FIXME due to the way refreshing happens I may need to bring in some of the ButtonForge v1 tech for doing next OnUpdate refresh checks
--------------------------------------------------]]
function PackageMacro.UpdateMacroDisplayAction(BFButton)

    local MacroIndex = BFButton.MacroIndex;
    local Change;
    -- Test if the macro is a spell
    local SpellID = GetMacroSpell(MacroIndex);
    if (SpellID) then

        if (BFButton.MacroMode ~= "spell" or BFButton.SpellID ~= SpellID) then
            -- The macro action has changed
            BFButton.MacroMode = "spell";
            BFButton.SpellID        = SpellID;
            BFButton.SpellFullName  = Util_SpellFullName(SpellID);
            Change = true;
        end

    else

        -- Test if the macro is an item
        local ItemName, ItemLink = GetMacroItem(MacroIndex);
        if (ItemName) then

            if (BFButton.MacroMode ~= "item" or BFButton.ItemName ~= ItemName) then
                -- The macro action has changed
                local ItemID = Util_GetItemIDFromHyperlink(ItemLink);
                BFButton.MacroMode = "item";
                BFButton.ItemID = ItemID;
                BFButton.IsToy  = GetToyInfo(ItemID) ~= nil;
                Change = true;
            end

        elseif (BFButton.MacroMode ~= nil) then
            -- The macro action has changed
            BFButton.MacroMode = nil;
            Change = true;
        end

    end

    -- Test for a change of Target
    local Action, Target = SecureCmdOptionParse(BFButton.MacroBody);
    if (BFButton.Target ~= Target) then
        BFButton.Target = Target;
        Change = true;
    end

    return Change;

end


--[[------------------------------------------------
    Icon
--------------------------------------------------]]
function PackageMacro.UpdateIcon(BFButton)

    local Name, Texture = GetMacroInfo(BFButton.MacroIndex);
    BFButton.ABWIcon:SetTexture(Texture or C_QUESTION_MARK);

end


--[[------------------------------------------------
    Checked
--------------------------------------------------]]
function PackageMacro.UpdateChecked(BFButton)
    local MacroMode = BFButton.MacroMode;
    if (MacroMode == "spell") then
        PackageSpell_UpdateChecked(BFButton);
    elseif (MacroMode == "item") then
        PackageItem_UpdateChecked(BFButton);
    else
        BFButton.ABW:SetChecked(false);
    end
end


--[[------------------------------------------------
    Equipped
--------------------------------------------------]]
function PackageMacro.UpdateEquipped(BFButton)
    if (BFButton.MacroMode == "item") then
        PackageItem_UpdateEquipped(BFButton);
    else
        BFButton.ABWBorder:Hide();
    end
end


--[[------------------------------------------------
    Usable
--------------------------------------------------]]
function PackageMacro.UpdateUsable(BFButton)
    local MacroMode = BFButton.MacroMode;
    if (MacroMode == "spell") then
        PackageSpell_UpdateUsable(BFButton);
    elseif (MacroMode == "item") then
        PackageItem_UpdateUsable(BFButton);
    else
        BFButton:SetDisplayUsable(true);
    end
end


--[[------------------------------------------------
    Cooldown
--------------------------------------------------]]
function PackageMacro.UpdateCooldown(BFButton)
    local MacroMode = BFButton.MacroMode;
    if (MacroMode == "spell") then
        PackageSpell_UpdateCooldown(BFButton);
    elseif (MacroMode == "item") then
        PackageItem_UpdateCooldown(BFButton);
    else
        CooldownFrame_Set(BFButton.ABWCooldown, 0, 0, 0);
        BFButton.ABWCooldown:Hide();
    end
end


--[[------------------------------------------------
    Text    (Charges, or Counts)
--------------------------------------------------]]
function PackageMacro.UpdateText(BFButton)
    local MacroMode = BFButton.MacroMode;
    if (MacroMode == "spell") then
        PackageSpell_UpdateText(BFButton);
    elseif (MacroMode == "item") then
        PackageItem_UpdateText(BFButton);
    else
        BFButton.ABWCount:SetText(nil);
    end

    if (BFButton.ABWCount:GetText() == nil and BFButton.ShowMacroName) then
        BFButton.ABWName:SetText(BFButton.MacroName);
    else
        BFButton.ABWName:SetText(nil);
    end
end


--[[------------------------------------------------
    Glow
--------------------------------------------------]]
function PackageMacro.UpdateGlow(BFButton)
    if (BFButton.MacroMode == "spell") then
        PackageSpell_UpdateGlow(BFButton);
    else
        BFButton:HideOverlayGlow()
    end
end


--[[------------------------------------------------
    Tooltip
--------------------------------------------------]]
function PackageMacro.UpdateTooltip(Obj)
    local ABW = Obj.ABW or Obj;
    local BFButton = ABW.BFButton;
    local MacroMode = BFButton.MacroMode;
    if (MacroMode == "spell") then
        PackageSpell_UpdateTooltip(ABW);
    elseif (MacroMode == "item") then
        PackageItem_UpdateTooltip(ABW);
    else
        ButtonForgeGameTooltip:SetText(BFButton.MacroName, 1, 1, 1, 1);
    end
end


--[[------------------------------------------------
    Flash Registration
--------------------------------------------------]]
function PackageMacro.UpdateFlashRegistration(BFButton)
    if (BFButton.MacroMode == "spell") then
        PackageSpell_UpdateFlashRegistration(BFButton);
    else
        Core_RegisterForFlash(BFButton);
    end
end


--[[------------------------------------------------
    Range Registration
--------------------------------------------------]]
function PackageMacro.UpdateRangeCheckRegistration(BFButton)
    local MacroMode = BFButton.MacroMode;
    if (MacroMode == "spell") then
        PackageSpell_UpdateRangeCheckRegistration(BFButton);
    elseif (MacroMode == "item") then
        PackageItem_UpdateRangeCheckRegistration(BFButton);
    else
        Core_RegisterForRangeChecks(BFButton);
    end
end


--[[------------------------------------------------
    Range Check
--------------------------------------------------]]
function PackageMacro.IsInRange(BFButton)
    local MacroMode = BFButton.MacroMode;
    if (MacroMode == "spell") then
        PackageSpell_IsInRange(BFButton);
    elseif (MacroMode == "item") then
        PackageItem_IsInRange(BFButton);
    end
end


--[[------------------------------------------------
    Cursor
--------------------------------------------------]]
function PackageMacro.GetCursor(BFButton)
    return "macro", BFButton.MacroIndex, nil, nil;
end


--[[------------------------------------------------
    Get Ordered Parameters
--------------------------------------------------]]
function PackageMacro.Get_OrderedParameters(Action)
    -- ActionType , ActionParameter1 , ActionParameter2 , ActionParameter3
    return "macro", Action.MacroIndex, Action.MacroName, Action.MacroBody;
end
