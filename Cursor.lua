--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014
    
    Desc:	Functions for working with the cursor and its contents
            This exists to allow me to go beyond the standard actions
            that can be picked up
]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local Cursor = Engine.Cursor;

local Methods = Engine.Methods;
local CursorCustom = Engine.CursorCustom;
local Core = Engine.Core;
local Util = Engine.Util;
local C = Engine.Constants;


--[[------------------------------------------------
    GetCursor
--------------------------------------------------]]
function Cursor.GetCursor()
    local Command, Data, Subvalue, SubSubvalue = GetCursorInfo();
    if (Command) then
        return Command, Data, Subvalue, SubSubvalue;
    else
        return CursorCustom.GetCustomCursor();
    end
end


--[[------------------------------------------------
    HasValidAction
    * Currently doesn't actually check the action type
--------------------------------------------------]]
function Cursor.HasValidAction()
    local Command = GetCursorInfo() or CursorCustom.GetCustomCursor();
    if (Command) then
        return true;
    else
        return false;
    end
end


--[[------------------------------------------------
    ClearCursor
--------------------------------------------------]]
function Cursor.ClearCursor()
    ClearCursor();
    CursorCustom.ClearCustomCursor();
end


--[[------------------------------------------------
    SetCursor
--------------------------------------------------]]
function Cursor.SetCursor(Command, Data, Subvalue, SubSubvalue, Icon, TexLeft, TexRight, TexTop, TexBottom)
    if (InCombatLockdown()) then
        return C.ERROR_IN_COMBAT_LOCKDOWN;
    end
    Cursor.ClearCursor();
    if (Command == "spell") then
        C_Spell.PickupSpell(SubSubvalue);
    elseif (Command == "item") then
        C_Item.PickupItem(Data);
    elseif (Command == "macro") then
        PickupMacro(Data);
    elseif (Command == "mount") then
        C_MountJournal.Pickup(Util.LookupMountIndex(Data));
    elseif (Command == "battlepet") then
        C_PetJournal.PickupPet(Data);
    elseif (Command == "flyout") then
        C_SpellBook.PickupSpellBookItem(Util.GetFlyoutSlotIndex(Data), Enum.SpellBookSpellBank.Player);
    elseif (Command == "equipmentset") then
        C_EquipmentSet.PickupEquipmentSet(Data);
    elseif (Command == "custom") then
        CursorCustom.SetCustomCursor(Command, Data, Subvalue, SubSubvalue, Icon, TexLeft, TexRight, TexTop, TexBottom);
    end
end


--[[------------------------------------------------
    Store/Get Cursor
--------------------------------------------------]]
local StoredCommand, StoredData, StoredSubvalue, StoredSubSubvalue, Icon, TexLeft, TexRight, TexTop, TexBottom;
function Cursor.StoreCursor(...)
    StoredCommand, StoredData, StoredSubvalue, StoredSubSubvalue, Icon, TexLeft, TexRight, TexTop, TexBottom = ...;
end
function Cursor.GetStoredCursor()
    return StoredCommand, StoredData, StoredSubvalue, StoredSubSubvalue, Icon, TexLeft, TexRight, TexTop, TexBottom;
end

