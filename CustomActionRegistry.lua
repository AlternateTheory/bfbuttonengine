--[[
	Author: Alternator (Massiner of Nathrezim)
	Date:	2020
	
	Desc:	Custom Action registry - addons that use this engine need to register any custom actions that they want supported on these buttons
]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local CustomActionRegistry = Engine.CustomActionRegistry;
local Scripts = Engine.Scripts;
local ScriptsCustom = Engine.ScriptsCustom;

local CustomActions = {};


--[[------------------------------------------------
	RegisterCustomAction
	This will create a package for the custom action
	When the custom action does not have a supplied function (e.g. IsEquipped function is missing) then the associated update will be set to the empty function
--------------------------------------------------]]
function CustomActionRegistry.RegisterCustomAction(Name, UpdateFunctions)

	CustomActions[Name] = {};

	-- Copy the supplied functions into the package
	CustomActions[Name].GetAction	= UpdateFunctions.GetAction;
	CustomActions[Name].GetIcon		= UpdateFunctions.GetIcon;
	CustomActions[Name].IsChecked	= UpdateFunctions.IsChecked;
	CustomActions[Name].IsUsable		= UpdateFunctions.IsUsable;
	CustomActions[Name].GetCooldown	= UpdateFunctions.GetCooldown;
	CustomActions[Name].IsEquipped	= UpdateFunctions.IsEquipped;
	CustomActions[Name].GetText		= UpdateFunctions.GetText;
	CustomActions[Name].IsGlowing	= UpdateFunctions.IsGlowing;
	CustomActions[Name].UpdateTooltip	= UpdateFunctions.UpdateTooltip;
	CustomActions[Name].IsFlashing	= UpdateFunctions.IsFlashing;
	CustomActions[Name].IsInRange	= UpdateFunctions.IsInRange;

end


--[[------------------------------------------------

--------------------------------------------------]]
function CustomActionRegistry.GetCustomAction(Name)
	return CustomActions[Name];
end



