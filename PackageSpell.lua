--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

    Package for "spell" action type

    Notes:

        * WoW v9.0
            - Spells trigger from Spellname
                - "Hex"
                - "Hex()"
                - "Hex(Compy)" - This will cast the Compy variant (Compy is spell subtext)
                - Check: Obliterate and Obliteration (with Russian Locale)
                    --"Death Knight", "Obliteration", "Talent", 281238
                    --"Death Knight", "Obliterate", "Ability", 49020

            - Some spells can be upgraded
                - Talents is the most common way to do this
                - Revive / Mend Pet is a non talent example
                - The base Spellname and ID should be used even if it is an upgraded Spell
                - All API calls will report correct info if the base spell name is used with them
                - Button Forge will capture and use the base spell info

            - Mend/Revive Behaviour:
                - Revive is the base spell
                - Mend is the override
                - While Mend is the active spell, the base spell is correctly identified as Revive
                - While Revive is the active spell, Mend does not know that Revive is it's base...
                    - This should not be an issue except in the case of migrating Mend
                    - This will self resolve if the player revives their pet
                - Other upgraded spells seem to have this issue also

            - A few info functions only work with SpellID, GameTooltip appears to transition to the correct info so that is not an issue
                - Glow Overlay is yet to be confirmed, but fingers crossed it behaves.

            - It would appear professions do not like being triggered from SpellName... the behaviour seems to be patchy... fortunately they appear to be fine when triggered from SpellID

]]

local AddonName, AddonTable = ...;
local Engine        = AddonTable.ButtonEngine;
local PackageSpell  = Engine.PackageSpell;
local Scripts       = Engine.Scripts;

-- Take local copy of functions to slightly improve performance (in theory)
local Util                      = Engine.Util;
local IsKnownProfessionSpell    = Util.IsKnownProfessionSpell;
local SpellFullName             = Util.SpellFullName;

local Core                          = Engine.Core;
local ReportEvent                   = Core.ReportEvent;
local Core_RegisterForFlash         = Core.RegisterForFlash;
local Core_RegisterForRangeChecks   = Core.RegisterForRangeChecks;
local Events                        = Engine.Events;

local C                             = Engine.Constants;
local EVENT_UPDATEACTION            = C.EVENT_UPDATEACTION;
local QUESTION_MARK                 = C.QUESTION_MARK;

local ActionButton_HideOverlayGlow  = ActionButton_HideOverlayGlow;
local ActionButton_ShowOverlayGlow  = ActionButton_ShowOverlayGlow;
local ClearChargeCooldown           = Core.ClearChargeCooldown;
local CooldownFrame_Set             = CooldownFrame_Set;
local FindBaseSpellByID             = FindBaseSpellByID;
--local GameTooltip                   = GameTooltip;
--local GameTooltip_SetDefaultAnchor  = GameTooltip_SetDefaultAnchor;
local GetMouseFocus                 = Util.GetMouseFocus;
local GetSpellCharges               = C_Spell.GetSpellCharges;
local GetSpellCooldown              = C_Spell.GetSpellCooldown;
local GetSpellLossOfControlCooldown = C_Spell.GetSpellLossOfControlCooldown;
local GetSpellCastCount             = C_Spell.GetSpellCastCount;
local GetSpellTexture               = C_Spell.GetSpellTexture;
local InCombatLockdown              = InCombatLockdown;
local IsAutoAttackSpell             = C_Spell.IsAutoAttackSpell;
local IsAutoRepeatSpell             = C_Spell.IsAutoRepeatSpell;
local IsCurrentSpell                = C_Spell.IsCurrentSpell;
local IsSpellInRange                = C_Spell.IsSpellInRange;
local IsSpellOverlayed              = IsSpellOverlayed;
local IsSpellUsable                 = C_Spell.IsSpellUsable;
local IsPressHoldReleaseSpell       = C_Spell.IsPressHoldReleaseSpell;
local StartChargeCooldown           = Core.StartChargeCooldown;


PackageSpell.UpdateEquipped             = Scripts.EmptyF;
PackageSpell.UpdateShine                = Scripts.EmptyF;
PackageSpell.SwapActionWithButtonAction = Scripts.SwapActionWithButtonAction;

local StanceSpells = {}
local function UpdateStanceSpellCache()
    wipe(StanceSpells)
    local numForms = GetNumShapeshiftForms()
    for index = 1, numForms do
        local _, _, _, spellID = GetShapeshiftFormInfo(index)
        spellID = FindBaseSpellByID(spellID)
        StanceSpells[spellID] = index
    end
end
Events.RegisterEventCallback("UPDATE_SHAPESHIFT_FORMS", UpdateStanceSpellCache)
UpdateStanceSpellCache()


--[[------------------------------------------------

    - Set Spell
    - Capture the base spell and set the Action to that

    - While not in combat the Buttons onclick action will be set
        - values to use in the restricted environment will also be set

    - This function can be called while in combat if the action was swapped using the SecureActionSwap
        - The SecureActionSwap will handle setting the onclick and restricted environment up

--------------------------------------------------]]
function PackageSpell.SetAction(BFButton, SpellID)

    local Action        = BFButton.Action;
    local BaseSpellID   = FindBaseSpellByID(SpellID);
    local Name          = SpellFullName(BaseSpellID);

    BFButton.Type           = "spell";
    BFButton.SpellID        = BaseSpellID;
    BFButton.SpellFullName  = Name;
    BFButton.SpellPressAndHoldAction    = IsPressHoldReleaseSpell(BaseSpellID);

    Action.Type             = "spell";
    Action.SpellID          = BaseSpellID;
    Action.SpellFullName    = Name;

    if (not InCombatLockdown()) then
        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();
        BFButton.ABW:SetAttribute("IsEmpowerSpell", BFButton.SpellPressAndHoldAction);
    end

    BFButton:FullUpdate();

end


--[[------------------------------------------------

    When an event occurs that indicates the Spells might have changed
    Update the spell to the base version if it is not already

    IMPORTANT
        - This process may be unwarranted, as the spells should already by the base version
        - In Button Forge, other states and tabs and profiles will not be affected by this leaving a gap if this is needed
        - REVIEW and investigate replacing (possibly with on load processing)
        - UpdateAction is also called to refresh the OnClick attributes if the empowerTapControls changes

--------------------------------------------------]]
function PackageSpell.UpdateAction(BFButton, Type)

    if (Type and Type ~= "spell") then
        return;
    end

    local SpellID       = BFButton.SpellID;
    local BaseSpellID   = FindBaseSpellByID(SpellID);

    if (BaseSpellID ~= SpellID) then

        local Name              = SpellFullName(BaseSpellID);
        BFButton.SpellID        = BaseSpellID;
        BFButton.SpellFullName  = Name;

        local Action            = BFButton.Action;
        Action.SpellID          = BaseSpellID;
        Action.SpellFullName    = Name;

        BFButton:UpdateSecureCursor();
        ReportEvent(BFButton, EVENT_UPDATEACTION, Action);

    end
    BFButton:UpdateAttributes();

end


--[[------------------------------------------------

    Set the on click action that will be triggered

--------------------------------------------------]]
function PackageSpell.UpdateAttributes(BFButton)

    local ABW = BFButton.ABW;
    local Type, Attr, Val = BFButton.Get_Attributes(BFButton.Action);
    ABW:SetAttribute("type", Type);
    ABW:SetAttribute("typerelease", Type);
    ABW:SetAttribute(Attr, Val);

end


--[[------------------------------------------------

    Get the attributes that are used for the on click action
    This function can be called directly to allow the on click attributes to be
    fed into the restricted environment of BF Bars

--------------------------------------------------]]
function PackageSpell.Get_Attributes(Action)

    local ActionValue;
    local SpellID = Action.SpellID;
    if ( IsKnownProfessionSpell( SpellID ) ) then
        ActionValue = SpellID;
    else
        ActionValue = Action.SpellFullName;
    end
    -- return   TypeValue , ActionAttribute , ActionValue
    return      "spell"   , "spell"         , ActionValue ;
end


--[[------------------------------------------------

    Set parameters to describe the action in the restricted environment of the button
    This will support secure changing of the action

--------------------------------------------------]]
function PackageSpell.UpdateSecureCursor(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("ActionType", "spell");
    ABW:SetAttribute("ActionParameter1", BFButton.SpellID);
    ABW:SetAttribute("ActionParameter2", BFButton.SpellPressAndHoldAction);
end


--[[------------------------------------------------

    Finalize will reset certain elements of the button that might not be
    affected by the new action (rather than a blanket reset, this is intended to be more performant)

--------------------------------------------------]]
function PackageSpell.Finalize(BFButton)

    local Action = BFButton.Action;
    Action.SpellID = nil;
    Action.SpellFullName = nil;

    local ABW = BFButton.ABW;

    -- Checked
    ABW:SetChecked(false);

    -- Glow
    ActionButton_HideOverlayGlow(ABW);

    -- Icon
    BFButton.ABWIcon:SetTexture(nil);

    -- Usable
    BFButton:SetDisplayUsable(true);

    -- Cooldown
    ClearChargeCooldown(ABW);
    BFButton.ABWCooldown:Clear();
    BFButton:SetCooldownType(COOLDOWN_TYPE_NORMAL);

    -- Text
    BFButton.ABWCount:SetText(nil);

    -- Flash
    Core_RegisterForFlash(BFButton);

    -- Range (clear registration)
    Core_RegisterForRangeChecks(BFButton);

    if (not InCombatLockdown()) then
        ABW:SetAttribute("IsEmpowerSpell", nil);
    end

end


--[[------------------------------------------------
    Full Update
--------------------------------------------------]]
function PackageSpell.FullUpdate(BFButton)
    BFButton:UpdateIcon();
    BFButton:UpdateChecked();
    BFButton:UpdateUsable();
    BFButton:UpdateCooldown();
    BFButton:UpdateText();
    BFButton:UpdateGlow();
    BFButton:UpdateFlashRegistration();
    BFButton:UpdateRangeCheckRegistration();

    if GetMouseFocus() == BFButton.ABW then
        BFButton:UpdateTooltip();
    end
end


--[[------------------------------------------------
    Icon
    Notes:
        * v6 of WoW
            * SpellBookItemTexture was used to get the modified texture (some spells changed texture during play)

        * v7 of WoW
            * Switched to using just the SpellID with GetSpellTexture, this is more reliable
                It remains to be seen if spells still modify textures (I'm no longer aware of any, Warlock used to have some in v6)

        * v9
            * Using spell name
            * stance / form changes are not using the wisp texture and will require some form of adjustment (possibly a custom lookup table??)
--------------------------------------------------]]
function PackageSpell.UpdateIcon(BFButton)
    local stanceIndex = StanceSpells[BFButton.SpellID]
    local texture
    if stanceIndex then
        texture = GetShapeshiftFormInfo(stanceIndex)
    else
        texture = GetSpellTexture(BFButton.SpellFullName) or GetSpellTexture(BFButton.SpellID) or QUESTION_MARK
    end
    BFButton.ABWIcon:SetTexture(texture);
end


--[[------------------------------------------------
    Checked
    Notes:
        * WoW v7
            * Removed check for AutoRepeatSpells (I dont believe they are still a thing)
--------------------------------------------------]]
function PackageSpell.UpdateChecked(BFButton)
    local stanceIndex = StanceSpells[BFButton.SpellID]
    if stanceIndex then
        local _, selected = GetShapeshiftFormInfo(stanceIndex)
        BFButton.ABW:SetChecked(selected)
    else
        BFButton.ABW:SetChecked(IsCurrentSpell(BFButton.SpellFullName))
    end
end


--[[------------------------------------------------
    Usable
--------------------------------------------------]]
function PackageSpell.UpdateUsable(BFButton)
    BFButton:SetDisplayUsable(IsSpellUsable(BFButton.SpellFullName));
end

local function OnCooldownDone(self, requireCooldownUpdate)
    self:SetScript("OnCooldownDone", nil)
    if requireCooldownUpdate then
        self:GetParent():UpdateCooldown()
    end
end

--[[------------------------------------------------
    Cooldown
    Notes:
        * v7 of WoW
            * Does not do Loss Of Control effect (this effect seemed to annoy players)

        * v10 of WoW
            - I have reintroduced Loss Of Control effect, although I have a faint recollection of a discussion around it, I could not find the player comments regarding it

--------------------------------------------------]]
local zeroCooldown = {startTime = 0, duration = 0, isEnabled = false, modRate = 0}
local zeroCharges = {currentCharges = 0, maxCharges = 0, cooldownStartTime = 0, cooldownDuration = 0, chargeModRate = 0}
function PackageSpell.UpdateCooldown(BFButton)

--    -- This is how Auras might be implemented in ButtonForge if they are relevant to BF, though at this stage I do not believe they are
--    local passiveCooldownSpellID = C_UnitAuras.GetCooldownAuraBySpellID(BFButton.SpellID);
--    if (passiveCooldownSpellID and passiveCooldownSpellID ~= 0) then
--        local auraData = C_UnitAuras.GetPlayerAuraBySpellID(passiveCooldownSpellID);
--        if (auraData) then
--            -- Use aura cooldown (normal type)
--            local start = auraData.expirationTime - auraData.duration;
--            CooldownFrame_Set(BFButton.ABWCooldown, start, auraData.duration, 1, false, auraData.timeMod);
--            return;
--        end
--    end

    local name = BFButton.SpellFullName;
    local locStart, locDuration = GetSpellLossOfControlCooldown(name);
    local cooldownInfo = GetSpellCooldown(name) or zeroCooldown
    local start, duration, enable, modRate = cooldownInfo.startTime, cooldownInfo.duration, cooldownInfo.isEnabled, cooldownInfo.modRate

    if (locStart + locDuration) > (start + duration) then
        BFButton:SetCooldownType(COOLDOWN_TYPE_LOSS_OF_CONTROL);
        CooldownFrame_Set(BFButton.ABWCooldown, locStart, locDuration, true, true, modRate);
        BFButton.ABWCooldown:SetScript("OnCooldownDone", nil)
        ClearChargeCooldown(BFButton.ABW);

    else
        BFButton:SetCooldownType(COOLDOWN_TYPE_NORMAL);
        local chargeInfo = GetSpellCharges(name) or zeroCharges;
        local charges, maxCharges, chargeStart, chargeDuration, chargeModRate = chargeInfo.currentCharges, chargeInfo.maxCharges, chargeInfo.cooldownStartTime, chargeInfo.cooldownDuration, chargeInfo.chargeModRate
        if locStart > 0 then
            BFButton.ABWCooldown:SetScript("OnCooldownDone", OnCooldownDone)
        else
            BFButton.ABWCooldown:SetScript("OnCooldownDone", nil)
        end
        if charges and maxCharges and maxCharges > 1 and charges < maxCharges then
            StartChargeCooldown(BFButton.ABW, chargeStart, chargeDuration, chargeModRate)
        else
            ClearChargeCooldown(BFButton.ABW)
        end
        CooldownFrame_Set(BFButton.ABWCooldown, start, duration, enable, false, modRate);
    end
end


--[[------------------------------------------------
    Text	(Charges, or Counts)
    Notes:
        * WoW v7
            * Removed IsConsumableSpell check (does not appear to be a thing anymore?)
            * Example Arcane Missiles (Mage) of a Spell with SpellCount

        * WoW v10
            * GetSpellCount might not be relevant anymore?
            * Possibly just used for zone abilities?
--------------------------------------------------]]
function PackageSpell.UpdateText(BFButton)

    local SpellFullName = BFButton.SpellFullName;
    local Count         = GetSpellCastCount(SpellFullName);
    if (Count ~= 0) then
        BFButton.ABWCount:SetText(Count);
        return;
    end
    local chargeInfo = GetSpellCharges(SpellFullName) or zeroCharges;
    local Charges, MaxCharges = chargeInfo.currentCharges, chargeInfo.maxCharges
    if MaxCharges > 1 then
        BFButton.ABWCount:SetText(Charges);
        return;
    end
    BFButton.ABWCount:SetText(nil);
end


--[[------------------------------------------------
    Glow
    Notes:
        * WoW v7
            * Arcane Missiles (Mage)
--------------------------------------------------]]
function PackageSpell.UpdateGlow(BFButton)
    if (IsSpellOverlayed(BFButton.SpellID)) then
        BFButton:ShowOverlayGlow()
    else
        BFButton:HideOverlayGlow()
    end
end


--[[------------------------------------------------
    Tooltip
    Notes:
        * WoW v10
            - The parameters (probably) for SetSpellByID
              C_TooltipInfo.GetSpellByID(spellID [, isPet, showSubtext, dontOverride, difficultyID, isLink])

--------------------------------------------------]]
function PackageSpell.UpdateTooltip(Obj)

    local ABW = Obj.ABW or Obj;
    ButtonForgeGameTooltip:SetSpellByID(ABW.BFButton.SpellID, false, true);

end


--[[------------------------------------------------
    Flash     (registers and Deregisters for flash)
    Notes:
        * WoW v7
            * Removed check for AutoRepeatSpells (I dont believe they are still a thing)
        * WoW v9
            * Fixed to use SpellFullName it is the most reliable over all, the fringe edge cases just aren't worth the compromises
--------------------------------------------------]]
function PackageSpell.UpdateFlashRegistration(BFButton)
    local SpellFullName = BFButton.SpellFullName;
    local IsFlashing = (IsAutoAttackSpell(SpellFullName) and IsCurrentSpell(SpellFullName)) or IsAutoRepeatSpell(SpellFullName);
    Core_RegisterForFlash(BFButton, IsFlashing);
end


--[[------------------------------------------------
    Range Registration
    Notes:
        The Target value is not set for Items
        However if this function is triggered by a Macro action then the Target might be set

        * WoW v7
            * IsSpellInRange does not work with SpellID, so we need to use SpellFullName here
--------------------------------------------------]]
function PackageSpell.UpdateRangeCheckRegistration(BFButton)
    Core_RegisterForRangeChecks(BFButton, IsSpellInRange(BFButton.SpellFullName, BFButton.Target));
end


--[[------------------------------------------------
    Range Check
    Notes:
        The Target value is not set for Items
        However if this function is triggered by a Macro action then the Target might be set

        * WoW v7
            * IsSpellInRange does not work with SpellID, so we need to use SpellFullName here
--------------------------------------------------]]
function PackageSpell.IsInRange(BFButton)
    return IsSpellInRange(BFButton.SpellFullName, BFButton.Target);
end


--[[------------------------------------------------
    Cursor
--------------------------------------------------]]
function PackageSpell.GetCursor(BFButton)
    return "spell", nil, nil, BFButton.SpellID;
end


--[[------------------------------------------------
    Get Ordered Parameters
--------------------------------------------------]]
function PackageSpell.Get_OrderedParameters(Action)
    -- ActionType , ActionParameter1
    return "spell", Action.SpellID, IsPressHoldReleaseSpell(Action.SpellID);
end
