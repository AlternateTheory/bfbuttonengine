--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine                = AddonTable.ButtonEngine;
local PackageMount          = Engine.PackageMount;
local PackageMountFavorite  = Engine.PackageMountFavorite;
local Scripts               = Engine.Scripts;
local Events                = Engine.Events;
local Util                  = Engine.Util

-- Take local copy of functions to slightly improve performance (in theory)
local C                                 = Engine.Constants;
local C_SUMMON_RANDOM_FAVORITE_MOUNT_ID = C.SUMMON_RANDOM_FAVORITE_MOUNT_ID;

local Methods                   = Engine.Methods;
local Methods_SetAction_OrderedParameters = Methods.SetAction_OrderedParameters;

local C_MountJournal_GetMountInfoByID = C_MountJournal.GetMountInfoByID;

local format                        = format;
--local GameTooltip                   = GameTooltip;
--local GameTooltip_SetDefaultAnchor  = GameTooltip_SetDefaultAnchor;
local GetMouseFocus                 = Util.GetMouseFocus;
local GetSpellTexture               = C_Spell.GetSpellTexture;
local InCombatLockdown              = InCombatLockdown;
local IsCurrentSpell                = C_Spell.IsCurrentSpell;
local IsSpellUsable                 = C_Spell.IsSpellUsable;
local select                        = select;


PackageMount.UpdateAction               = Scripts.EmptyF;
PackageMount.UpdateIcon                 = Scripts.EmptyF;
PackageMount.UpdateCooldown             = Scripts.EmptyF;
PackageMount.UpdateEquipped             = Scripts.EmptyF;
PackageMount.UpdateText                 = Scripts.EmptyF;
PackageMount.UpdateGlow                 = Scripts.EmptyF;
PackageMount.UpdateShine                = Scripts.EmptyF;
PackageMount.UpdateFlashRegistration    = Scripts.EmptyF;
PackageMount.UpdateRangeCheckRegistration    = Scripts.EmptyF;
PackageMount.SwapActionWithButtonAction = Scripts.SwapActionWithButtonAction;


--[[------------------------------------------------

    - Set Mount

    - While not in combat the Buttons onclick action will be set
        - values to use in the restricted environment will also be set

    - This function can be called while in combat if the action was swapped using the SecureActionSwap
        - The SecureActionSwap will handle setting the onclick and restricted environment up

--------------------------------------------------]]
function PackageMount.SetAction(BFButton, MountID)
    if (MountID == C_SUMMON_RANDOM_FAVORITE_MOUNT_ID) then
        -- It's the random favorite button, use a different code path
        return BFButton:SetAction_OrderedParameters("favoritemount");
    end

    local Action                    = BFButton.Action;
    local MountName, MountSpellID   = C_MountJournal_GetMountInfoByID(MountID);

    BFButton.Type           = "mount";
    BFButton.MountID        = MountID;
    BFButton.MountName      = MountName;
    BFButton.MountSpellID   = MountSpellID;

    Action.Type             = "mount";
    Action.MountID          = MountID;
    Action.MountName        = MountName;

    if (not InCombatLockdown()) then
        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();
    end

    local texture = GetSpellTexture(MountSpellID)
    if not texture and not BFButton.RegisteredForCallback then
        Events.RegisterEventCallback("COMPANION_UPDATE", PackageMount.CallbackMountsAvailable, BFButton)
        BFButton.RegisteredForCallback = true
    end
    BFButton.ABWIcon:SetTexture(GetSpellTexture(MountSpellID));
    BFButton:FullUpdate();

end


function PackageMount.CallbackMountsAvailable(BFButton)
    BFButton:UpdateAction("mount")
end


function PackageMount.UpdateAction(BFButton, Type)
    if (Type ~= "mount") then
        return;
    end
    local MountName, MountSpellID   = C_MountJournal_GetMountInfoByID(BFButton.MountID);
    BFButton.MountName      = MountName;
    BFButton.MountSpellID   = MountSpellID;
    local texture = GetSpellTexture(BFButton.MountSpellID)
    if texture then
        Events.UnregisterEventCallback("COMPANION_UPDATE", PackageMount.CallbackMountsAvailable, BFButton)
        BFButton.RegisteredForCallback = nil
    end
    BFButton.ABWIcon:SetTexture(texture);
end


--[[------------------------------------------------

    Set the on click action that will be triggered

--------------------------------------------------]]
function PackageMount.UpdateAttributes(BFButton)
    local ABW = BFButton.ABW;
    --ABW:SetAttribute("type", "macro");
    --ABW:SetAttribute("typerelease", "macro");
    --ABW:SetAttribute("macrotext", format("/script C_MountJournal.SummonByID(%i);", BFButton.MountID));
    ABW:SetAttribute("type", "spell");
    ABW:SetAttribute("typerelease", "spell");
    ABW:SetAttribute("spell", BFButton.MountName);
    --ABW:SetAttribute("macrotext", format("/cast %s", BFButton.MountName));
end


--[[------------------------------------------------

    Get the attributes that are used for the on click action
    This function can be called directly to allow the on click attributes to be
    fed into the restricted environment of BF Bars

--------------------------------------------------]]
function PackageMount.Get_Attributes(Action)
    -- return   TypeValue , ActionAttribute , ActionValue
    --return "macro", "macrotext", format( "/script C_MountJournal.SummonByID(%i);" , Action.MountID );
    return "spell", "spell", Action.MountName
    --return "macro", "macrotext", format( "/cast %s" , Action.MountName );
end


--[[------------------------------------------------

    Set parameters to describe the action in the restricted environment of the button
    This will support secure changing of the action

--------------------------------------------------]]
function PackageMount.UpdateSecureCursor(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("ActionType", "mount");
    ABW:SetAttribute("ActionParameter1", BFButton.MountID);
    ABW:SetAttribute("ActionParameter2", nil);
    ABW:SetAttribute("ActionParameter3", nil);
end


--[[------------------------------------------------

    Finalize will reset certain elements of the button that might not be
    affected by the new action (rather than a blanket reset, this is intended to be more performant)

--------------------------------------------------]]
function PackageMount.Finalize(BFButton)

    local Action = BFButton.Action;
    Action.MountID = nil;
    Action.MountName = nil;

    local ABW = BFButton.ABW;

    -- Checked
    ABW:SetChecked(false);

    -- Icon
    BFButton.ABWIcon:SetTexture(nil);

    -- Usable
    BFButton:SetDisplayUsable(true);

    if BFButton.RegisteredForCallback then
        Events.UnregisterEventCallback("COMPANION_UPDATE", PackageMount.CallbackMountsAvailable, BFButton)
        BFButton.RegisteredForCallback = nil
    end
end


--[[------------------------------------------------
    Full Update
--------------------------------------------------]]
function PackageMount.FullUpdate(BFButton)
    BFButton:UpdateChecked();
    BFButton:UpdateUsable();

    if GetMouseFocus() == BFButton.ABW then
        BFButton:UpdateTooltip();
    end
end


--[[------------------------------------------------
    Checked
--------------------------------------------------]]
function PackageMount.UpdateChecked(BFButton)
    BFButton.ABW:SetChecked(IsCurrentSpell(BFButton.MountSpellID));
end


--[[------------------------------------------------
    Usable
--------------------------------------------------]]
function PackageMount.UpdateUsable(BFButton)
    BFButton:SetDisplayUsable(IsSpellUsable(BFButton.MountSpellID) and select(5, C_MountJournal_GetMountInfoByID(BFButton.MountID)));
end


--[[------------------------------------------------
    Tooltip
--------------------------------------------------]]
function PackageMount.UpdateTooltip(Obj)
    local ABW = Obj.ABW or Obj;
    ButtonForgeGameTooltip:SetMountBySpellID(ABW.BFButton.MountSpellID);
end


--[[------------------------------------------------
    Cursor
--------------------------------------------------]]
function PackageMount.GetCursor(BFButton)
    return "mount", BFButton.MountID, nil, nil;
end


--[[------------------------------------------------
    Get Ordered Parameters
--------------------------------------------------]]
function PackageMount.Get_OrderedParameters(Action)
    -- ActionType , ActionParameter1 , ActionParameter2 , ActionParameter3
    return "mount", Action.MountID;
end
