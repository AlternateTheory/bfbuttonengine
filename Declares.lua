--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
AddonTable.ButtonEngine = {};
AddonTable.ButtonEngine.Core = {};
AddonTable.ButtonEngine.Util = {};
AddonTable.ButtonEngine.CustomActionRegistry = {};
AddonTable.ButtonEngine.SecureManagement = {};
AddonTable.ButtonEngine.Events = {};
AddonTable.ButtonEngine.Settings = {};
AddonTable.ButtonEngine.Constants = {};
AddonTable.ButtonEngine.Cursor = {};
AddonTable.ButtonEngine.CursorCustom = {};
AddonTable.ButtonEngine.FlyoutUI = {};
AddonTable.ButtonEngine.Methods = {};
AddonTable.ButtonEngine.Scripts = {};

AddonTable.ButtonEngine.PackageEmpty = {};
AddonTable.ButtonEngine.PackageSpell = {};
AddonTable.ButtonEngine.PackageItem = {};
AddonTable.ButtonEngine.PackageMacro = {};
AddonTable.ButtonEngine.PackageMount = {};
AddonTable.ButtonEngine.PackageMountFavorite = {};
AddonTable.ButtonEngine.PackageBattlePet = {};
AddonTable.ButtonEngine.PackageBattlePetFavorite = {};
AddonTable.ButtonEngine.PackageFlyout = {};
AddonTable.ButtonEngine.PackageEquipmentSet = {};
AddonTable.ButtonEngine.PackageCustom = {};

AddonTable.ButtonEngine.ScriptsSpell = {};
AddonTable.ButtonEngine.ScriptsItem = {};
AddonTable.ButtonEngine.ScriptsMacro = {};
AddonTable.ButtonEngine.ScriptsMount = {};
AddonTable.ButtonEngine.ScriptsMountFavorite = {};
AddonTable.ButtonEngine.ScriptsBattlePet = {};
AddonTable.ButtonEngine.ScriptsBattlePetFavorite = {};
AddonTable.ButtonEngine.ScriptsFlyout = {};
AddonTable.ButtonEngine.ScriptsEquipmentSet = {};
AddonTable.ButtonEngine.ScriptsCustom = {};
AddonTable.ButtonEngine.SecureActionSwap = {};
AddonTable.ButtonEngine.Locales = {};
AddonTable.ButtonEngine.API_V2 = {};
