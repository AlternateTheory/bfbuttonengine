--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2016


]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local SecureManagement = Engine.SecureManagement;


local SecureCombatFrame = CreateFrame("FRAME", nil, nil, "SecureHandlerStateTemplate");
SecureCombatFrame:Hide();
SecureCombatFrame:Execute(
    [[
        HideOnCombatButtons = newtable();
        DisableMouseOnCombatButtons = newtable();
    ]]
);
RegisterAttributeDriver(SecureCombatFrame, "state-combat", "[combat] true; false");


--[[------------------------------------------------
    Entering combat, perform secure processing to ensure buttons are in the correct state

    This is done since the buttons may be force showed/enabled while the cursor is holding an action.
    Once in combat, cursor information largely become unavailable so the buttons and set back to their correct state at the start of combat
--------------------------------------------------]]
SecureCombatFrame:SetAttribute("_onstate-combat",
    [[
        if newstate == "true" then

            local EmptyButton = next(HideOnCombatButtons);
            if EmptyButton and EmptyButton:IsShown() then
                for Button in pairs(HideOnCombatButtons) do
                    Button:Hide();
                end
            end

            local MouseOffButton = next(DisableMouseOnCombatButtons);
            if MouseOffButton and MouseOffButton:IsMouseEnabled() then
                for Button in pairs(DisableMouseOnCombatButtons) do
                    Button:EnableMouse(false);
                end
            end


        elseif newstate == "false" then

            self:CallMethod("PostCombatProcessing");

        end
    ]]
);


local PostCombatUpdateBFButtons = {};
function SecureCombatFrame:PostCombatProcessing()
    for BFButton in pairs(PostCombatUpdateBFButtons) do
        BFButton:UpdateHideOnCombat();
        BFButton:UpdateShown();
        BFButton:UpdateRespondToMouse();
    end
end


function SecureManagement.RegisterForPostCombatUpdate(BFButton)
    PostCombatUpdateBFButtons[BFButton] = true;
end


function SecureManagement.DeregisterForPostCombatUpdate(BFButton)
    PostCombatUpdateBFButtons[BFButton] = nil;
end


--[[------------------------------------------------

--------------------------------------------------]]
function SecureManagement.RegisterForHideOnCombat(ABW)
    SecureCombatFrame:SetFrameRef("ABW", ABW);
    SecureCombatFrame:Execute(
        [[
            local ABW = owner:GetFrameRef("ABW");
            HideOnCombatButtons[ABW] = true;
        ]]
    );
end


--[[------------------------------------------------

--------------------------------------------------]]
function SecureManagement.DeregisterForHideOnCombat(ABW)
    SecureCombatFrame:SetFrameRef("ABW", ABW);
    SecureCombatFrame:Execute(
        [[
            local ABW = owner:GetFrameRef("ABW");
            HideOnCombatButtons[ABW] = nil;
        ]]
    );
end


--[[------------------------------------------------

--------------------------------------------------]]
function SecureManagement.RegisterForDisableMouseOnCombat(ABW)
    SecureCombatFrame:SetFrameRef("ABW", ABW);
    SecureCombatFrame:Execute(
        [[
            local ABW = owner:GetFrameRef("ABW");
            DisableMouseOnCombatButtons[ABW] = true;
        ]]
    );
end


--[[------------------------------------------------

--------------------------------------------------]]
function SecureManagement.DeregisterForDisableMouseOnCombat(ABW)
    SecureCombatFrame:SetFrameRef("ABW", ABW);
    SecureCombatFrame:Execute(
        [[
            local ABW = owner:GetFrameRef("ABW");
            DisableMouseOnCombatButtons[ABW] = nil;
        ]]
    );
end
