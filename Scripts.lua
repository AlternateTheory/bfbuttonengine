--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine = AddonTable.ButtonEngine;
local Scripts = Engine.Scripts;

local S = Engine.Settings;
local C = Engine.Constants;
local Core = Engine.Core;
local Cursor = Engine.Cursor;
local FlyoutUI = Engine.FlyoutUI;





--[[------------------------------------------------
    EmptyF
--------------------------------------------------]]
function Scripts.EmptyF()

end


--[[------------------------------------------------
    EmptyChecked
--------------------------------------------------]]
function Scripts.EmptyChecked(BFButton)
    BFButton.ABW:SetChecked(false);
end
