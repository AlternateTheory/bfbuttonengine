--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine                = AddonTable.ButtonEngine;
local PackageMountFavorite  = Engine.PackageMountFavorite;
local Scripts               = Engine.Scripts;
local Util                  = Engine.Util

-- Take local copy of functions to slightly improve performance (in theory)
local C                                     = Engine.Constants;
local C_SUMMON_RANDOM_FAVORITE_MOUNT_ID     = C.SUMMON_RANDOM_FAVORITE_MOUNT_ID;
local C_SUMMON_RANDOM_FAVORITE_MOUNT_SPELL  = C.SUMMON_RANDOM_FAVORITE_MOUNT_SPELL;

local C_MountJournal_GetMountFromSpell  = C_MountJournal.GetMountFromSpell;

--local GameTooltip                   = GameTooltip;
--local GameTooltip_SetDefaultAnchor  = GameTooltip_SetDefaultAnchor;
local GetMouseFocus                 = Util.GetMouseFocus;
local GetSpellTexture               = C_Spell.GetSpellTexture;
local InCombatLockdown              = InCombatLockdown;
local IsMounted                     = IsMounted;
local IsSpellUsable                 = C_Spell.IsSpellUsable;
local UnitCastingInfo               = UnitCastingInfo;


PackageMountFavorite.UpdateAction				= Scripts.EmptyF;
PackageMountFavorite.UpdateIcon 				= Scripts.EmptyF;
PackageMountFavorite.UpdateCooldown				= Scripts.EmptyF;
PackageMountFavorite.UpdateEquipped				= Scripts.EmptyF;
PackageMountFavorite.UpdateText					= Scripts.EmptyF;
PackageMountFavorite.UpdateGlow					= Scripts.EmptyF;
PackageMountFavorite.UpdateShine				= Scripts.EmptyF;
PackageMountFavorite.UpdateFlashRegistration	= Scripts.EmptyF;
PackageMountFavorite.UpdateRangeCheckRegistration	= Scripts.EmptyF;
PackageMountFavorite.SwapActionWithButtonAction	= Scripts.SwapActionWithButtonAction;


--[[------------------------------------------------

    - Set Mount Favorite

    - While not in combat the Buttons onclick action will be set
        - values to use in the restricted environment will also be set

    - This function can be called while in combat if the action was swapped using the SecureActionSwap
        - The SecureActionSwap will handle setting the onclick and restricted environment up

--------------------------------------------------]]
function PackageMountFavorite.SetAction(BFButton, MountID)

    BFButton.Type           = "favoritemount";
    BFButton.Action.Type    = "favoritemount";

    if (not InCombatLockdown()) then
        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();
    end

    BFButton.ABWIcon:SetTexture(GetSpellTexture(C_SUMMON_RANDOM_FAVORITE_MOUNT_SPELL));
    BFButton:FullUpdate();

end

local _ , Class = UnitClass("player")
if Class == "DRUID" then
    function PackageMountFavorite.UpdateAttributes(BFButton)
        local ABW = BFButton.ABW;
        ABW:SetAttribute("type", "macro");
        ABW:SetAttribute("typerelease", "macro");
        ABW:SetAttribute("macrotext", "/cancelform [nocombat, form:1][nocombat, form:2]\n/script C_MountJournal.SummonByID(0)");
    end
    function PackageMountFavorite.Get_Attributes(Action)
        -- return   TypeValue , ActionAttribute , ActionValue
        return "macro" , "macrotext" , "/cancelform [nocombat, form:1][nocombat, form:2]\n/script C_MountJournal.SummonByID(0)";
    end
else
    --[[------------------------------------------------

        Set the on click action that will be triggered

    --------------------------------------------------]]
    function PackageMountFavorite.UpdateAttributes(BFButton)
        local ABW = BFButton.ABW;
        ABW:SetAttribute("type", "macro");
        ABW:SetAttribute("typerelease", "macro");
        ABW:SetAttribute("macrotext", "/script C_MountJournal.SummonByID(0);");
    end


    --[[------------------------------------------------

        Get the attributes that are used for the on click action
        This function can be called directly to allow the on click attributes to be
        fed into the restricted environment of BF Bars

    --------------------------------------------------]]
    function PackageMountFavorite.Get_Attributes(Action)
        -- return   TypeValue , ActionAttribute , ActionValue
        return "macro" , "macrotext" , "/script C_MountJournal.SummonByID(0);" ;
    end
end

--[[------------------------------------------------

    Set parameters to describe the action in the restricted environment of the button
    This will support secure changing of the action

--------------------------------------------------]]
function PackageMountFavorite.UpdateSecureCursor(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("ActionType", "favoritemount");
    ABW:SetAttribute("ActionParameter1", nil);
    ABW:SetAttribute("ActionParameter2", nil);
    ABW:SetAttribute("ActionParameter3", nil);
end


--[[------------------------------------------------

    Finalize will reset certain elements of the button that might not be
    affected by the new action (rather than a blanket reset, this is intended to be more performant)

--------------------------------------------------]]
function PackageMountFavorite.Finalize(BFButton)

    local ABW = BFButton.ABW;

    -- Checked
    ABW:SetChecked(false);

    -- Icon
    BFButton.ABWIcon:SetTexture(nil);

    -- Usable
    BFButton:SetDisplayUsable(true);

end


--[[------------------------------------------------
    Full Update
--------------------------------------------------]]
function PackageMountFavorite.FullUpdate(BFButton)

    BFButton:UpdateChecked();
    BFButton:UpdateUsable();
    
    if GetMouseFocus() == BFButton.ABW then
        BFButton:UpdateTooltip();
    end
end


--[[------------------------------------------------
    Checked
    Notes:
        * WoW v7 (13-Aug-2016)
            * summoning any mount should check summon favorite
            * It's not possible to simply test if the player is in
            process of summoning a mount (not cheaply anyway)
--------------------------------------------------]]
function PackageMountFavorite.UpdateChecked(BFButton)
    if (IsMounted()) then
        BFButton.ABW:SetChecked(true);
    else
        local _, _, _, _, _, _, _, _, SpellID = UnitCastingInfo("player");
        SpellID = SpellID or 0;
        if (C_MountJournal_GetMountFromSpell(SpellID)) then
            BFButton.ABW:SetChecked(true);
        else
            BFButton.ABW:SetChecked(false);
        end
    end
end


--[[------------------------------------------------
    Usable
        This doesn't really work
        The correct implementation is to scan each favorite
        and detect if any are usable... since that could be expensive
        for a low value payout I'll just leave it (I doubt the spell api call
        ever correctly identifies it is unusable)
--------------------------------------------------]]
function PackageMountFavorite.UpdateUsable(BFButton)
    BFButton:SetDisplayUsable(IsSpellUsable(C_SUMMON_RANDOM_FAVORITE_MOUNT_SPELL));
end


--[[------------------------------------------------
    Tooltip
--------------------------------------------------]]
function PackageMountFavorite.UpdateTooltip(Obj)
    local ABW = Obj.ABW or Obj;
    ButtonForgeGameTooltip:SetMountBySpellID(C_SUMMON_RANDOM_FAVORITE_MOUNT_SPELL);
end


--[[------------------------------------------------
    Cursor
--------------------------------------------------]]
function PackageMountFavorite.GetCursor(BFButton)
    return "mount", C_SUMMON_RANDOM_FAVORITE_MOUNT_ID, nil, nil;
end


--[[------------------------------------------------
    Get Ordered Parameters
--------------------------------------------------]]
function PackageMountFavorite.Get_OrderedParameters(Action)
    -- ActionType , ActionParameter1 , ActionParameter2 , ActionParameter3
    return "favoritemount";
end
