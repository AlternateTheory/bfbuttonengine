--[[
    Author: Alternator (Massiner of Nathrezim)
    Date:	2014

]]

local AddonName, AddonTable = ...;
local Engine                    = AddonTable.ButtonEngine;
local PackageBattlePetFavorite  = Engine.PackageBattlePetFavorite;
local Scripts                   = Engine.Scripts;
local Util                  = Engine.Util

-- Take local copy of functions to slightly improve performance (in theory)
local C = Engine.Constants;
local C_SUMMON_RANDOM_FAVORITE_PET_SPELL    = C.SUMMON_RANDOM_FAVORITE_PET_SPELL;
local C_SUMMON_RANDOM_FAVORITE_PET_GUID     = C.SUMMON_RANDOM_FAVORITE_PET_GUID;

local C_PetBattles_IsInBattle                   = C_PetBattles.IsInBattle;
local C_PetJournal_GetNumPets                   = C_PetJournal.GetNumPets;
local C_PetJournal_GetSummonBattlePetCooldown   = C_PetJournal.GetSummonBattlePetCooldown;
local C_PetJournal_GetSummonedPetGUID           = C_PetJournal.GetSummonedPetGUID;
local C_PetJournal_PetIsSummonable              = C_PetJournal.PetIsSummonable;

local CooldownFrame_Set             = CooldownFrame_Set;
--local GameTooltip                   = GameTooltip;
--local GameTooltip_SetDefaultAnchor  = GameTooltip_SetDefaultAnchor;
local GetMouseFocus                 = Util.GetMouseFocus;
local GetSpellTexture               = C_Spell.GetSpellTexture;
local InCombatLockdown              = InCombatLockdown;


PackageBattlePetFavorite.UpdateAction				= Scripts.EmptyF;
PackageBattlePetFavorite.UpdateIcon				    = Scripts.EmptyF;
PackageBattlePetFavorite.UpdateEquipped				= Scripts.EmptyF;
PackageBattlePetFavorite.UpdateText					= Scripts.EmptyF;
PackageBattlePetFavorite.UpdateGlow					= Scripts.EmptyF;
PackageBattlePetFavorite.UpdateShine				= Scripts.EmptyF;
PackageBattlePetFavorite.UpdateFlashRegistration	= Scripts.EmptyF;
PackageBattlePetFavorite.UpdateRangeCheckRegistration	= Scripts.EmptyF;
PackageBattlePetFavorite.SwapActionWithButtonAction	= Scripts.SwapActionWithButtonAction;


--[[------------------------------------------------

    - Set BattlePet action

    - While not in combat the Buttons onclick action will be set
        - values to use in the restricted environment will also be set

    - This function can be called while in combat if the action was swapped using the SecureActionSwap
        - The SecureActionSwap will handle setting the onclick and restricted environment up

--------------------------------------------------]]
function PackageBattlePetFavorite.SetAction(BFButton)

    BFButton.Type           = "favoritebattlepet";
    BFButton.Action.Type    = "favoritebattlepet";


    if (not InCombatLockdown()) then
        BFButton:UpdateAttributes();
        BFButton:UpdateSecureCursor();
    end

    BFButton.ABWIcon:SetTexture(GetSpellTexture(C_SUMMON_RANDOM_FAVORITE_PET_SPELL));
    BFButton:FullUpdate();

end


--[[------------------------------------------------

    Set the on click action that will be triggered

--------------------------------------------------]]
function PackageBattlePetFavorite.UpdateAttributes(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("type", "macro");
    ABW:SetAttribute("typerelease", "macro");
    ABW:SetAttribute("macrotext", format("/summonpet %s", C_SUMMON_RANDOM_FAVORITE_PET_GUID));
end


--[[------------------------------------------------

    Get the attributes that are used for the on click action
    This function can be called directly to allow the on click attributes to be
    fed into the restricted environment of BF Bars

--------------------------------------------------]]
function PackageBattlePetFavorite.Get_Attributes(Action)
    -- return   TypeValue , ActionAttribute , ActionValue
    return      "macro"   , "macrotext"     , format( "/summonpet %s" , C_SUMMON_RANDOM_FAVORITE_PET_GUID );
end


--[[------------------------------------------------

    Set parameters to describe the action in the restricted environment of the button
    This will support secure changing of the action

--------------------------------------------------]]
function PackageBattlePetFavorite.UpdateSecureCursor(BFButton)
    local ABW = BFButton.ABW;
    ABW:SetAttribute("ActionType", "favoritebattlepet");
    ABW:SetAttribute("ActionParameter1", nil);
    ABW:SetAttribute("ActionParameter2", nil);
    ABW:SetAttribute("ActionParameter3", nil);
end


--[[------------------------------------------------

    Finalize will reset certain elements of the button that might not be
    affected by the new action (rather than a blanket reset, this is intended to be more performant)

--------------------------------------------------]]
function PackageBattlePetFavorite.Finalize(BFButton)

    local ABW = BFButton.ABW;

    -- Checked
    ABW:SetChecked(false);

    -- Icon
    BFButton.ABWIcon:SetTexture(nil);

    -- Usable
    BFButton:SetDisplayUsable(true);

    -- Cooldown
    BFButton.ABWCooldown:Clear();

end

--[[------------------------------------------------
    Full Update
--------------------------------------------------]]
function PackageBattlePetFavorite.FullUpdate(BFButton)
    BFButton:UpdateUsable();

    if (GetMouseFocus() == BFButton.ABW) then
        BFButton:UpdateTooltip();
    end
end


--[[------------------------------------------------
    Checked
--------------------------------------------------]]
function PackageBattlePetFavorite.UpdateChecked(BFButton)
    if (C_PetJournal_GetSummonedPetGUID()) then
        BFButton.ABW:SetChecked(true);
    else
        BFButton.ABW:SetChecked(false);
    end
end


--[[------------------------------------------------
    Usable
--------------------------------------------------]]
function PackageBattlePetFavorite.UpdateUsable(BFButton)
    BFButton:SetDisplayUsable(C_PetJournal_PetIsSummonable(C_SUMMON_RANDOM_FAVORITE_PET_GUID));
end


--[[------------------------------------------------
    Cooldown
--------------------------------------------------]]
function PackageBattlePetFavorite.UpdateCooldown(BFButton)
    CooldownFrame_Set(BFButton.ABWCooldown, C_PetJournal_GetSummonBattlePetCooldown());
end


--[[------------------------------------------------
    Tooltip
--------------------------------------------------]]
function PackageBattlePetFavorite.UpdateTooltip(Obj)
    local ABW = Obj.ABW or Obj;

    -- This next bit more or less lifted from the Pet Journal implementation
    local numPets, numOwned = C_PetJournal_GetNumPets();
    if ( numOwned > 0 and not C_PetBattles_IsInBattle()  ) then
        ButtonForgeGameTooltip:SetCompanionPet(C_SUMMON_RANDOM_FAVORITE_PET_GUID);
    else
        ButtonForgeGameTooltip:SetSpellByID(C_SUMMON_RANDOM_FAVORITE_PET_SPELL);
    end
end


--[[------------------------------------------------
    Cursor
--------------------------------------------------]]
function PackageBattlePetFavorite.GetCursor(BFButton)
    return "battlepet", C_SUMMON_RANDOM_FAVORITE_PET_GUID, nil, nil;
end


--[[------------------------------------------------
    Get Ordered Parameters
--------------------------------------------------]]
function PackageBattlePetFavorite.Get_OrderedParameters(Action)
    -- ActionType , ActionParameter1 , ActionParameter2 , ActionParameter3
    return "favoritebattlepet";
end
